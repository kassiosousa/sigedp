### Do que se trata / What is about? ###

* Sistema gerenciador de pedidos com passagens e diárias, que são gerenciados pelo Nead Ufma.
* Requests manager system with tickets and daily's, who are managed by Nead, UFMA.


### Estrutura / Structure #

* application
    * modules - Pasta HMVC com as tríades que são os módulos do sistema, cada um contendo 'controllers', 'models' e 'views' / Directory HMVC with the system modules, compounds by 'controllers', 'models' and 'views'
* assets
    * painel - Arquivos de imagem, css, js e demais adições ao design do painel. / Files of image, css, js and files to design of dashboard.
    * plugins - Plugins e extensões extras que o projeto necessite. / Plugins and extensions that project needs
    * sql - Pasta para armazenar versões do banco de dados e alterações de estrura (Querys adicionais) / Directory to keep versions of the database and structure changes

### Módulos ###

* Configurações 
* Usuários 
* Polos 
* Cursos 
* Plano Pedagógico 
* Pedidos 
* Relatórios 

### Desenvolvedores / Developers ###

* Kássio Sousa
* Júlio Filho