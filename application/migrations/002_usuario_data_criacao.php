<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Usuario_data_criacao extends CI_Migration {

    public function up()
    {
        $colunas = array(
            'usua_data_criacao' => array(
                'type' => 'datetime',
                'null' => true
            )
        );

        $this->dbforge->add_column('usuarios', $colunas);

        $this->db->query("UPDATE usuarios SET usua_data_criacao = NOW() WHERE usua_data_criacao IS NULL");
    }

    public function down()
    {
        $this->dbforge->drop_column('usuarios', 'usua_data_criacao');
    }

}
