
CREATE TABLE IF NOT EXISTS `pedidos` (
  `pmk_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `fok_usuario` int(11) NOT NULL,
  `fok_polo` int(11) NOT NULL,
  `fok_plano_disicplina` int(11) NOT NULL,
  `pedi_status` enum('Aguardando','Rejeitado','Aprovado','Liberado','Cancelado','Pendente','Conclu�do') NOT NULL DEFAULT 'Aguardando',
  `pedi_tipo` enum('Nead','Curso','Polo') NOT NULL DEFAULT 'Curso',
  `pedi_diaria_dia_inicial` date NOT NULL,
  `pedi_diaria_quant` int(11) NOT NULL,
  `pedi_viagem_ida_dia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pedi_viagem_ida_valor_previsto` decimal(8,2) NOT NULL,
  `pedi_viagem_ida_transporte` enum('Terrestre','Aereo','Carro Inst.','Carro Particular') NOT NULL DEFAULT 'Terrestre',
  `pedi_viagem_volta_dia` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pedi_viagem_volta_valor_previsto` decimal(8,2) NOT NULL,
  `pedi_viagem_volta_transporte` enum('Terrestre','Aereo','Carro Inst.','Carro Particular') NOT NULL DEFAULT 'Terrestre',
  `pedi_justificativa` text NOT NULL,
  `pedi_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_pedido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `pedidos_relatorios_viagens` (
  `pmk_pedido_relatorio` int(11) NOT NULL AUTO_INCREMENT,
  `fok_pedido` int(11) NOT NULL,
  `pedido_rel_data` date NOT NULL,
  `pedido_rel_texto` text NOT NULL,
  `pedido_rel_anexo` varchar(255) NOT NULL,
  `pedido_rel_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_pedido_relatorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `oficios` (
  `pmk_oficio` int(11) NOT NULL AUTO_INCREMENT,
  `fok_pedido` int(11) NOT NULL,
  `oficio_status` enum('Aguardando','Liberado','Rejeitado') NOT NULL DEFAULT 'Aguardando',
  `oficio_diarias_dia` date NOT NULL,
  `oficio_diarias_cod` varchar(255) NOT NULL,
  `oficio_passagens_dia` date NOT NULL,
  `oficio_passagens_cod` varchar(255) NOT NULL,
  `oficio_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_oficio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

---

ALTER TABLE  `planos_pedagogicos_disciplinas` CHANGE  `plano_disciplina_titulo`  `plano_disciplina_titulo` VARCHAR( 255 ) NOT NULL
