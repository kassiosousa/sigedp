
CREATE TABLE IF NOT EXISTS `configuracoes` (
  `pmk_config` int(11) NOT NULL AUTO_INCREMENT,
  `config_porc_hora_teorica` int(11) NOT NULL,
  `config_porc_hora_pratica` int(11) NOT NULL,
  `config_qtd_viagens_polo` int(11) NOT NULL,
  `config_qtd_diarias_polo` int(11) NOT NULL,
  `config_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_config`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `cursos` (
  `pmk_curso` int(11) NOT NULL AUTO_INCREMENT,
  `fok_usua_coordenador` int(11) NOT NULL,
  `curso_titulo` varchar(255) NOT NULL,
  `curso_descricao` varchar(255) NOT NULL,
  `curso_data_inicio` date NOT NULL,
  `curso_data_fim` date NOT NULL,
  `curso_data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `curso_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_curso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `cursos_contas` (
  `pmk_curso_conta` int(11) NOT NULL AUTO_INCREMENT,
  `fok_curso` int(11) NOT NULL,
  `curso_conta_qtd_diarias` int(11) NOT NULL,
  `curso_conta_vr_diaria` float(8,2) NOT NULL,
  `curso_conta_saldo` float(8,2) NOT NULL,
  `curso_conta_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_curso_conta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `cursos_contas_movimentacoes` (
  `pmk_curso_conta_movimentacao` int(11) NOT NULL AUTO_INCREMENT,
  `fok_curso_conta` int(11) NOT NULL,
  `movcontacurso_tipo` enum('Adicao','Retirada') NOT NULL DEFAULT 'Adicao',
  `movcontacurso_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_curso_conta_movimentacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `cursos_coordenador` (
  `pmk_curso_coordenador` int(11) NOT NULL AUTO_INCREMENT,
  `fok_curso` int(11) NOT NULL,
  `fok_usua_coordenador` int(11) NOT NULL,
  `curso_coordenador_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_curso_coordenador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `cursos_polos` (
  `pmk_curso_polo` int(11) NOT NULL AUTO_INCREMENT,
  `fok_curso` int(11) NOT NULL,
  `fok_polo` int(11) NOT NULL,
  `polo_curso_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_curso_polo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `cursos_secretarios` (
  `pmk_curso_secretario` int(11) NOT NULL AUTO_INCREMENT,
  `fok_curso` int(11) NOT NULL,
  `fok_usua_secretario` int(11) NOT NULL,
  `curso_secretario_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_curso_secretario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

---

CREATE TABLE IF NOT EXISTS `polos` (
  `pmk_polo` int(11) NOT NULL AUTO_INCREMENT,
  `fok_cidade` int(11) NOT NULL,
  `polo_titulo` int(11) NOT NULL,
  `polo_descricao` int(11) NOT NULL,
  `polo_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_polo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
