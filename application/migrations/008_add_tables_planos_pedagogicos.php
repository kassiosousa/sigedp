<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tables_planos_pedagogicos extends CI_Migration {

    public function up()
    {
        $queries = explode('---', file_get_contents(APPPATH . 'migrations/' . '008.sql'));
        foreach ($queries as $query) $this->db->query($query);
    }

    public function down()
    {
        $this->dbforge->drop_table('planos_pedagogicos');
		$this->dbforge->drop_table('planos_pedagogicos_disciplinas');
    }

}
