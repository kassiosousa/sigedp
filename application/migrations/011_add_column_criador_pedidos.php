<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_column_criador_pedidos extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column('pedidos', array(
            'fok_usuario_criador' => array('type' => 'INT', 'null' => true)
        ));
    }

    public function down()
    {
        $this->dbforge->drop_column('pedidos', 'fok_usuario_criador');
    }

}
