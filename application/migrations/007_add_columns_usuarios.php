<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_columns_usuarios extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_column('usuarios', array(
            'usua_telefone' => array('type' => 'VARCHAR', 'constraint' => '15', 'null' => true),
            'fok_cidade' => array('type' => 'INT', 'null' => true),
            'usua_end_logradouro' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => true),
            'usua_end_complemento' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => true),
            'usua_end_numero' => array('type' => 'VARCHAR', 'constraint' => '10', 'null' => true),
            'usua_end_cep' => array('type' => 'CHAR', 'constraint' => '9', 'null' => true),
            'usua_banco_nome' => array('type' => 'VARCHAR', 'constraint' => '50', 'null' => true),
            'usua_banco_agencia' => array('type' => 'VARCHAR', 'constraint' => '10', 'null' => true),
            'usua_banco_conta' => array('type' => 'VARCHAR', 'constraint' => '15', 'null' => true)
        ));
    }

    public function down()
    {
        $this->dbforge->drop_column('usuarios', 'fok_cidade');
        $this->dbforge->drop_column('usuarios', 'usua_end_logradouro');
        $this->dbforge->drop_column('usuarios', 'usua_end_complemento');
        $this->dbforge->drop_column('usuarios', 'usua_end_numero');
        $this->dbforge->drop_column('usuarios', 'usua_end_cep');
        $this->dbforge->drop_column('usuarios', 'usua_banco_nome');
        $this->dbforge->drop_column('usuarios', 'usua_banco_agencia');
        $this->dbforge->drop_column('usuarios', 'usua_banco_conta');
    }

}
