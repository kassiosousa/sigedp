ALTER TABLE  `cursos_contas_movimentacoes` ADD  `movcontacurso_valor` INT NOT NULL AFTER  `movcontacurso_tipo`

---

ALTER TABLE  `cursos_contas_movimentacoes` ADD  `movcontacurso_dt_criacao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER  `movcontacurso_valor`
	
---

ALTER TABLE  `cursos_contas_movimentacoes` ADD  `fok_usuario_criador` INT NOT NULL AFTER  `fok_curso_conta`