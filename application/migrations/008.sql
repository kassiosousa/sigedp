
CREATE TABLE IF NOT EXISTS `planos_pedagogicos` (
  `pmk_plano_pedagogico` int(11) NOT NULL AUTO_INCREMENT,
  `fok_curso` int(11) NOT NULL,
  `plano_pedagogico_data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `plano_pedagogico_qtd_semestres` int(11) NOT NULL,
  `plano_pedagogico_total_encontros` int(11) NOT NULL,
  `plano_pedagogico_total_viagens` int(11) NOT NULL,
  `plano_pedagogico_total_diarias` int(11) NOT NULL,
  `plano_pedagogico_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_plano_pedagogico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
---

CREATE TABLE IF NOT EXISTS `planos_pedagogicos_disciplinas` (
  `pmk_plano_disciplina` int(11) NOT NULL AUTO_INCREMENT,
  `fok_plano_pedagogico` int(11) NOT NULL,
  `plano_nr_semestre` int(11) NOT NULL,
  `plano_disciplina_titulo` int(11) NOT NULL,
  `plano_disciplina_horas_teoricas` int(11) NOT NULL,
  `plano_disciplina_horas_praticas` int(11) NOT NULL,
  `plano_disciplina_qtd_encontros` int(11) NOT NULL,
  `plano_disciplina_qtd_viagens` int(11) NOT NULL,
  `plano_disciplina_qtd_diarias` int(11) NOT NULL,
  `plano_disciplina_is_ativo` enum('Nao','Sim') NOT NULL DEFAULT 'Sim',
  PRIMARY KEY (`pmk_plano_disciplina`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
