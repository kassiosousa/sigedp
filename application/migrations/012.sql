ALTER TABLE  `cursos_contas` ADD  `curso_conta_saldo_dinheiro` FLOAT( 8, 2 ) NOT NULL AFTER  `curso_conta_saldo`
---
ALTER TABLE  `cursos_contas` CHANGE  `curso_conta_qtd_diarias`  `curso_conta_qtd_diarias_inicial` INT( 11 ) NOT NULL
---
ALTER TABLE  `cursos_contas` CHANGE  `curso_conta_vr_diaria`  `curso_conta_vr_diaria_inicial` FLOAT( 8, 2 ) NOT NULL
---
ALTER TABLE  `cursos_contas` ADD  `curso_conta_vr_diaria` FLOAT( 8, 2 ) NOT NULL AFTER  `curso_conta_vr_diaria_inicial`
---
ALTER TABLE  `cursos_contas` ADD  `curso_conta_saldo_inicial` FLOAT( 8, 2 ) NOT NULL AFTER  `curso_conta_vr_diaria_inicial`
---
ALTER TABLE  `cursos_contas` CHANGE  `curso_conta_saldo_inicial`  `curso_conta_saldo_dinheiro_inicial` FLOAT( 8, 2 ) NOT NULL
---
ALTER TABLE  `cursos_contas` CHANGE  `curso_conta_saldo`  `curso_conta_saldo` INT NOT NULL
---
ALTER TABLE  `cursos_contas_movimentacoes` ADD  `movcontacurso_valor_dinheiro` FLOAT( 8, 2 ) NOT NULL AFTER  `movcontacurso_valor`
---
