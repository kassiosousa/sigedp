<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Ajustes_cursos_contas extends CI_Migration {

    public function up()
    {
        $queries = explode('---', file_get_contents(APPPATH . 'migrations/' . '012.sql'));
        foreach ($queries as $query) $this->db->query($query);
    }

    public function down()
    {
        $this->dbforge->drop_column('cursos_contas', 'curso_conta_saldo_dinheiro');
    }

}
