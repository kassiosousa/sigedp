<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tipos_usuarios extends CI_Migration {

    public function up()
    {
        $queries = explode('---', file_get_contents(APPPATH . 'migrations/' . '004.sql'));
        foreach ($queries as $query) $this->db->query($query);
    }

    public function down()
    {
        //$this->dbforge->drop_table('polos');
    }

}
