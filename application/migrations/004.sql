INSERT INTO  `sigedp`.`usuarios_tipos` (
`pmk_usuariotipo` ,
`usuatipo_nivel_acesso` ,
`usuatipo_nome` ,
`usuatipo_is_ativo`
)
VALUES 
(NULL ,  '2',  'Coordenador Administrativo',  'Sim'), 
(NULL ,  '3',  'Secretaria Administrativa',  'Sim'),
(NULL ,  '4',  'Coordenador de Curso',  'Sim'),
(NULL ,  '5',  'Secretaria de Curso',  'Sim'),
(NULL ,  '6',  'Usuarios',  'Sim'), 
(NULL ,  '0',  'Admin',  'Sim');

---

ALTER TABLE  `configuracoes` ADD  `config_ver_relatorio_viagem_pendente` BOOLEAN NOT NULL DEFAULT FALSE AFTER  `config_qtd_diarias_polo` ,
ADD  `config_ver_data_viagem_finalizada` BOOLEAN NOT NULL DEFAULT FALSE AFTER  `config_ver_relatorio_viagem_pendente` ,
ADD  `config_ver_nova_diaria` BOOLEAN NOT NULL DEFAULT FALSE AFTER  `config_ver_data_viagem_finalizada` ,
ADD  `config_ver_prox_data_limite_envio_relatorio` BOOLEAN NOT NULL DEFAULT FALSE AFTER  `config_ver_nova_diaria`

