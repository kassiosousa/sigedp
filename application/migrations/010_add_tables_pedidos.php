<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Tables_Pedidos extends CI_Migration {

    public function up()
    {
		$queries = explode('---', file_get_contents(APPPATH . 'migrations/' . '010.sql'));
        foreach ($queries as $query) $this->db->query($query);
    }

    public function down()
    {
		$this->dbforge->drop_table('oficios');
        $this->dbforge->drop_table('pedidos');
		$this->dbforge->drop_table('pedidos_relatorios_viagens');
    }

}
