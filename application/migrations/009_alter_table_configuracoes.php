<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_table_configuracoes extends CI_Migration {

    public function up()
    {
        $this->dbforge->drop_column('configuracoes', 'config_porc_hora_teorica');
        $this->dbforge->drop_column('configuracoes', 'config_porc_hora_pratica');
        $this->dbforge->drop_column('configuracoes', 'config_qtd_viagens_polo');
        $this->dbforge->drop_column('configuracoes', 'config_qtd_diarias_polo');
        $this->dbforge->drop_column('configuracoes', 'config_ver_relatorio_viagem_pendente');
        $this->dbforge->drop_column('configuracoes', 'config_ver_data_viagem_finalizada');
        $this->dbforge->drop_column('configuracoes', 'config_ver_nova_diaria');
        $this->dbforge->drop_column('configuracoes', 'config_ver_prox_data_limite_envio_relatorio');

        $this->dbforge->add_column('configuracoes', array(
            'config_descricao' => array('type' => 'VARCHAR', 'constraint' => '50', 'null' => true),
            'config_valor' => array('type' => 'INT', 'null' => true),
        ));

        $configs = array(
            array(
                'pmk_config' => 1,
                'config_descricao' => '% de hora teórica a ser convertida em encontros',
                'config_valor' => 20,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 2,
                'config_descricao' => '% de hora prática a ser convertida em encontros',
                'config_valor' => 80,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 3,
                'config_descricao' => 'qtde padrão de viagens por polo',
                'config_valor' => 30,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 4,
                'config_descricao' => 'qtde padrão de diárias por polo',
                'config_valor' => 60,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 5,
                'config_descricao' => 'notificar relatório de viagem pendente',
                'config_valor' => 1,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 6,
                'config_descricao' => 'notificar data de viagem finalizada',
                'config_valor' => 1,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 7,
                'config_descricao' => 'notificar novas solicitações de diária',
                'config_valor' => 1,
                'config_is_ativo' => 'Sim'
            ),
            array(
                'pmk_config' => 8,
                'config_descricao' => 'notificar proxim. da data de envio de relatório',
                'config_valor' => 1,
                'config_is_ativo' => 'Sim'
            )
        );

        $this->db->insert_batch('configuracoes', $configs);

    }

    public function down()
    {
        $this->dbforge->drop_column('configuracoes', 'config_descricao');
        $this->dbforge->drop_column('configuracoes', 'config_valor');

        $this->dbforge->add_column('configuracoes', array(
            'config_porc_hora_teorica' => array('type' => 'INT', 'null' => true),
            'config_porc_hora_pratica' => array('type' => 'INT', 'null' => true),
            'config_qtd_viagens_polo' => array('type' => 'INT', 'null' => true),
            'config_qtd_diarias_polo' => array('type' => 'INT', 'null' => true),
            'config_ver_relatorio_viagem_pendente' => array('type' => 'INT', 'null' => true),
            'config_ver_data_viagem_finalizada' => array('type' => 'INT', 'null' => true),
            'config_ver_nova_diaria' => array('type' => 'INT', 'null' => true),
            'config_ver_prox_data_limite_envio_relatorio' => array('type' => 'INT', 'null' => true),
        ));
    }

}
