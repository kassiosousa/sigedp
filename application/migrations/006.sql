ALTER TABLE  `cidade` CHANGE  `nome`  `cidade_nome` VARCHAR( 120 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
---
ALTER TABLE  `cidade` CHANGE  `id`  `pmk_cidade` INT( 11 ) NOT NULL AUTO_INCREMENT
---
ALTER TABLE  `cidade` CHANGE  `estado`  `fok_estado` INT( 5 ) NULL DEFAULT NULL
---
ALTER TABLE  `estado` CHANGE  `nome`  `estado_nome` VARCHAR( 75 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
---
ALTER TABLE  `estado` CHANGE  `id`  `pmk_estado` INT( 11 ) NOT NULL AUTO_INCREMENT
---
ALTER TABLE  `polos` CHANGE  `polo_titulo`  `polo_titulo` VARCHAR( 255 ) NOT NULL
---
ALTER TABLE  `polos` CHANGE  `polo_descricao`  `polo_descricao` VARCHAR( 255 ) NOT NULL