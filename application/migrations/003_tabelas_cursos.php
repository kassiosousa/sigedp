<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabelas_cursos extends CI_Migration {

    public function up()
    {
        $queries = explode('---', file_get_contents(APPPATH . 'migrations/' . '003.sql'));
        foreach ($queries as $query) $this->db->query($query);
    }

    public function down()
    {
        $this->dbforge->drop_table('polos');
        $this->dbforge->drop_table('cursos_secretarios');
        $this->dbforge->drop_table('cursos_polos');
        $this->dbforge->drop_table('cursos_coordenador');
        $this->dbforge->drop_table('cursos_contas_movimentacoes');
        $this->dbforge->drop_table('cursos_contas');
        $this->dbforge->drop_table('cursos');
        $this->dbforge->drop_table('configuracoes');
        $this->dbforge->drop_table('usuarios_tipos');
    }

}
