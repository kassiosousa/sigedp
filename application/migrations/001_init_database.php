<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Init_database extends CI_Migration {

    public function up()
    {
        $queries = explode('---', file_get_contents(APPPATH . 'migrations/' . '001.sql'));
        foreach ($queries as $query) $this->db->query($query);

        $this->db->query("INSERT INTO usuarios (fok_usuariotipo, usua_nome, usua_email, usua_senha) VALUES
            (1, 'Diretor Nead', 'usuario1@email.com', '202cb962ac59075b964b07152d234b70'),
            (2, 'Coord Adm', 'usuario2@email.com', '202cb962ac59075b964b07152d234b70'),
            (3, 'Secret Adm', 'usuario3@email.com', '202cb962ac59075b964b07152d234b70'),
            (4, 'Coord Curso', 'usuario4@email.com', '202cb962ac59075b964b07152d234b70'),
            (5, 'Secret Curso', 'usuario5@email.com', '202cb962ac59075b964b07152d234b70'),
            (6, 'Usuario', 'usuario6@email.com', '202cb962ac59075b964b07152d234b70')");
    }

    public function down()
    {
        $this->dbforge->drop_table('usuarios_tipos');
        $this->dbforge->drop_table('usuarios');
        $this->dbforge->drop_table('sys_sessions');
        $this->dbforge->drop_table('pais');
        $this->dbforge->drop_table('estado');
        $this->dbforge->drop_table('cidade');
    }

}
