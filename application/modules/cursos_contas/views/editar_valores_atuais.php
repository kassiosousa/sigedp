
<!-- Modal de Editar Usu�rio -->

<form class='form-horizontal'>
	<input type='hidden' name='pmk_curso_conta' value='<?php echo $pmk_curso_conta; ?>'>
		
		<div class='form-group'>
			<label for='curso_conta_saldo_dinheiro' class='col-sm-4 control-label'>Saldo Atual (R$)</label>
			<div class='col-sm-8'>
				<input type='number' class='form-control' id='curso_conta_saldo_dinheiro' name='curso_conta_saldo_dinheiro' value='<?php echo $curso_conta_saldo_dinheiro; ?>' placeholder='1' required>
			</div>
		</div>
		<div class='form-group'>
			<label for='curso_conta_vr_diaria' class='col-sm-4 control-label'>Valor Diaria Atual</label>
			<div class='col-sm-8'>
				<input type='number' class='form-control' id='curso_conta_vr_diaria' name='curso_conta_vr_diaria' value='<?php echo $curso_conta_vr_diaria; ?>' placeholder='1' required>
			</div>
		</div>
		
		<div class='form-group'>
			<label for='curso_conta_saldo' class='col-sm-4 control-label'>Saldo Diarias</label>
			<div class='col-sm-8'>
				<input type='number' readonly class='form-control' id='curso_conta_saldo' name='curso_conta_saldo' value='<?php echo $curso_conta_saldo; ?>' placeholder='1' required>
			</div>
		</div>
</form>

<script>
	$(document).on("change", "input[id^='curso_conta_saldo_dinheiro']", function(ev) {
		var saldoDinheiro = $("#curso_conta_saldo_dinheiro").val();
		var valorDiaria = $("#curso_conta_vr_diaria").val();
		var result = Math.floor(saldoDinheiro/valorDiaria);
		$("#curso_conta_saldo").val(result);
	});
	$(document).on("change", "input[id^='curso_conta_vr_diaria']", function(ev) {
		var saldoDinheiro = $("#curso_conta_saldo_dinheiro").val();
		var valorDiaria = $("#curso_conta_vr_diaria").val();
		var result = Math.floor(saldoDinheiro/valorDiaria);
		$("#curso_conta_saldo").val(result);
	});
	
</script>
