<?php
	$cursosContas = $this->cursos_contas->listar();

// CALCULO - Diarias utilizadas por cada curso
	// Lista os cursos | Lista os pedidos | Soma a qtd de diárias 
	if ($cursosContas) {
		foreach ($cursosContas as $curso_conta) {
			// => Lista os pedidos feitos para este curso (à partir das disciplinas)
			$pediParam['fok_curso'] = $curso_conta['fok_curso'];
			$pedidosCurso = $this->pedidos->listar_por_curso($pediParam);
			if ($pedidosCurso) {
				foreach ($pedidosCurso as $pedido) {
					$diariasUsadas[$curso_conta['pmk_curso_conta']] = $pedido['pedi_diaria_quant'];
				}
			} else { $diariasUsadas[$curso_conta['pmk_curso_conta']] = 0; }
			
			// => Calcula movimentacoes do curso
			$moviParam['fok_curso'] = $curso_conta['fok_curso'];
			$movimentacoesCursosContas = $this->cursos_contas_movimentacoes->listar($moviParam);
			
			if ($movimentacoesCursosContas) {
				$movimentacoesCurso[$curso_conta['pmk_curso_conta']] = 0;
				foreach ($movimentacoesCursosContas as $movimen) {
					if ($movimen['movcontacurso_tipo'] == 'Adicao') {
						$movimentacoesCurso[$curso_conta['pmk_curso_conta']] += $movimen['movcontacurso_valor'];
					}
					if ($movimen['movcontacurso_tipo'] == 'Retirada') {
						$movimentacoesCurso[$curso_conta['pmk_curso_conta']] -= $movimen['movcontacurso_valor'];
					}
				}
			} else { $movimentacoesCurso[$curso_conta['pmk_curso_conta']] = 0; }
		}
	}
// * Limite do curso / Qtd gasto do curso - Qtd tem
// Pega o cálculo acima (Qtd gasto do curso) - 
	
	
// Calculo da conta NEAD - Sobra de diárias do saldo dos outros cursos
	// Saldo de diárias deve ser calculado de acordo com diárias já utilizadas anteriormente
	$NeadDiariasSaldo = 0;
	$utilizacoesNead = 0; // Deve ser calculada quantas diarias desse tipo foram usadas

	//CALCULO - Somatório de diárias do curso
	if ($cursosContas) {
		foreach ($cursosContas as $conta) {
			$NeadDiariasSaldo += $conta['curso_conta_saldo'];
		}
	}
	// CALCULO - Diarias utilizadas por outros cursos
	$diariasUsadasCursos = 0;
	if ($cursosContas) {
		foreach ($cursosContas as $curso_conta) {
			$diariasUsadasCursos += $diariasUsadas[$curso_conta['pmk_curso_conta']];
		}
	}
	// CALCULO - Diarias utilizadas NEAD
	if ($cursosContas) {
		// => Lista os pedidos feitos tipo NEAD
		$pediParam['fok_curso'] = $curso_conta['fok_curso'];
		$pediParam['pedi_tipo'] = 'Nead';
		$pedidosNead = $this->pedidos->listar_por_curso($pediParam);
		if ($pedidosNead) {
			foreach ($pedidosNead as $pedido) {
				$utilizacoesNead = $pedido['pedi_diaria_quant'];
			}
		}
	}
	
	// Somatória de diárias iniciais de todos os cursos - diárias utilizadas pelos cursos - diárias usadas pelo tipo NEAD
	$NeadDiariasSaldo -= $diariasUsadasCursos;
	$NeadDiariasSaldo -= $utilizacoesNead;
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Cursos Contas<small class='hidden-xs'><br>Lista de Contas dos cursos cadastrados</small></h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>cursos_contas'><i class='fa fa-usd'></i> Contas de Cursos</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>

<div class='row'>
	<div class='col-xs-12'>
	
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th colspan="2" ></th>
							<th colspan="3" style="text-align:center"><i class='icon_profile'></i>Valores Iniciais</th>
							<th colspan="2" style="text-align:center"><i class='icon_profile'></i> Utilizações</th>
							<th colspan="3" style="text-align:center"><i class='icon_profile'></i> Atuais</th>
							<td></td>
						</tr>
						<tr>
							<th style="width: 40px"><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Curso</th>
							<th style="width: 100px"><i class='icon_profile'></i> Saldo (R$)</th>
							<th style="width: 100px"><i class='icon_profile'></i> Valor Diaria</th>
							<th style="width: 100px"><i class='icon_profile'></i> Qtd Diarias</th>
							<th style="width: 180px"><i class='icon_profile'></i> Qtd Diarias Utilizadas</th>
							<th style="width: 200px"><i class='icon_profile'></i> Movimentações (Diárias)</th>
							<th style="width: 100px"><i class='icon_profile'></i> Saldo (R$)</th>
							<th style="width: 100px"><i class='icon_profile'></i> Valor Diaria</th>
							<th style="width: 120px"><i class='icon_profile'></i> Saldo (Diárias)</th>
							<td style="width: 40px"></td>
						</tr>
					</thead>
					<tbody>
						<tr class="info">
							<td></td>
							<td>Conta NEAD</td>
							<td></td>
							<td></td>
							<td></td>
							<td><?php echo $utilizacoesNead; /*$diariasUsadasCursos;*/ ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td><?php echo $NeadDiariasSaldo; ?></td>
							<td class='text-center acoes'></td>
						</tr>
						<?php
							if ($cursosContas) {
								foreach ($cursosContas as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_curso_conta']; ?></td>
									<td><?php echo $lista['curso_titulo']; ?></td>
									<!-- Iniciais -->
									<td><?php echo $lista['curso_conta_saldo_dinheiro_inicial']; ?></td>
									<td><?php echo $lista['curso_conta_vr_diaria_inicial']; ?></td>
									<td><?php echo $lista['curso_conta_qtd_diarias_inicial']; ?></td>
									<!-- Usos -->
									<td><?php echo $diariasUsadas[$lista['pmk_curso_conta']]; ?></td>
									<td><?php echo $movimentacoesCurso[$lista['pmk_curso_conta']]; ?></td>
									<!-- Saldos Atuais -->
									<td><?php echo $lista['curso_conta_saldo_dinheiro']; ?></td>
									<td><?php echo $lista['curso_conta_vr_diaria']; ?></td>
									<td class="success"><?php echo $lista['curso_conta_saldo']; ?></td>

									<td class='text-center acoes'>
										<div class='btn-group'>
											<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>cursos_contas/editar/?id=<?php echo $lista['pmk_curso_conta'];?>' data-header='Editar Valores Iniciais'><i class='fa fa-edit'></i></button>
											<button class='btn btn-info btn-sm modal-form' data-url='<?php echo base_url();?>cursos_contas/editar_valores_atuais/?id=<?php echo $lista['pmk_curso_conta'];?>' data-header='Editar Valores Atuais'><i class='fa fa-edit'></i></button>
											<a class='btn btn-success btn-sm modal-form' href='<?php echo base_url();?>cursos_contas_movimentacoes/<?php echo $lista['pmk_curso_conta'];?>' ><i class="fa fa-arrows-h"></i></a>
											<!--<button class='btn btn-warning btn-sm modal-form' data-url='<?php echo base_url();?>cursos_contas_transferencias/<?php echo $lista['pmk_curso_conta'];?>' data-header='Transferência entre contas'><i class="fa fa-retweet"></i></a>-->
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Curso</th>
							<th><i class='icon_profile'></i> Saldo (R$)</th>
							<th><i class='icon_profile'></i> Valor Diaria</th>
							<th><i class='icon_profile'></i> Qtd Diarias</th>
							<th><i class='icon_profile'></i> Qtd Diarias Utilizadas</th>
							<th><i class='icon_profile'></i> Movimentações (Diárias)</th>
							<th><i class='icon_profile'></i> Saldo (R$)</th>
							<th><i class='icon_profile'></i> Valor Diaria</th>
							<th><i class='icon_profile'></i> Saldo (Diárias)</th>
							<td></td>
						</tr>
					</tfoot>
				</table>
				<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button> = Editar Valores Iniciais |
				<button class="btn btn-info btn-sm"><i class="fa fa-edit"></i></button> = Editar Valor da Diária |
				<button class="btn btn-success btn-sm"><i class="fa fa-arrows-h"></i></button> = Movimentações
				<br>
				* O saldo NEAD consiste em: Saldo de diárias dos cursos - Qtd diárias utilizadas pelos cursos - Qtd diárias utilizadas pelo NEAD <br>
			
			</div>
		</div>
		
	</div>
</div>

