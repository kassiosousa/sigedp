<!-- Modal de Editar Usuário -->

<form class='form-horizontal'>
	<input type='hidden' name='pmk_curso_conta' value='<?php echo $pmk_curso_conta; ?>'>
		
		<div class='form-group'>
			<label for='curso_conta_saldo_dinheiro_inicial' class='col-sm-4 control-label'>Saldo (R$)</label>
			<div class='col-sm-8'>
				<input type='number' class='form-control' id='curso_conta_saldo_dinheiro_inicial' name='curso_conta_saldo_dinheiro_inicial' value='<?php echo $curso_conta_saldo_dinheiro_inicial; ?>' placeholder='1' required>
			</div>
		</div>
		<div class='form-group'>
			<label for='curso_conta_vr_diaria_inicial' class='col-sm-4 control-label'>Valor Diaria</label>
			<div class='col-sm-8'>
				<input type='number' class='form-control' id='curso_conta_vr_diaria_inicial' name='curso_conta_vr_diaria_inicial' value='<?php echo $curso_conta_vr_diaria_inicial; ?>' placeholder='1' required>
			</div>
		</div>
		
		<div class='form-group'>
			<label for='curso_conta_qtd_diarias_inicial' class='col-sm-4 control-label'>Qtd. Diarias</label>
			<div class='col-sm-8'>
				<input type='number' readonly class='form-control' id='curso_conta_qtd_diarias_inicial' name='curso_conta_qtd_diarias_inicial' value='<?php echo $curso_conta_qtd_diarias_inicial; ?>' placeholder='1' required>
			</div>
		</div>
</form>

<script>
	$(document).on("change", "input[id^='curso_conta_saldo_dinheiro_inicial']", function(ev) {
		var saldoDinheiro = $("#curso_conta_saldo_dinheiro_inicial").val();
		var valorDiaria = $("#curso_conta_vr_diaria_inicial").val();
		var result = Math.floor(saldoDinheiro/valorDiaria);
		$("#curso_conta_qtd_diarias_inicial").val(result);
	});
	$(document).on("change", "input[id^='curso_conta_vr_diaria_inicial']", function(ev) {
		var saldoDinheiro = $("#curso_conta_saldo_dinheiro_inicial").val();
		var valorDiaria = $("#curso_conta_vr_diaria_inicial").val();
		var result = Math.floor(saldoDinheiro/valorDiaria);
		$("#curso_conta_qtd_diarias_inicial").val(result);
	});
	
</script>
