<?php
/* CRUD Controller for Cursos_contas */

class Cursos_contas extends CI_Controller {
	
	function _remap($method){ 
		//Carrega os modelos a serem utilizados
		$this->load->model('Cursos_contas/modelcursos_contas', 'cursos_contas');
        $this->load->model('Pedidos/modelpedidos', 'pedidos');
		$this->load->model('Cursos_contas_movimentacoes/modelcursos_contas_movimentacoes', 'cursos_contas_movimentacoes');
		
		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }
		
		// chama a função de acordo com o método (method)
		switch( $method ) {
		
			default:
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'index', $templateExtras);
			break;
		
			case 'editar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras
							$this->form_validation->set_rules('curso_conta_qtd_diarias_inicial', 'Qtd. Diarias', 'required');
							$this->form_validation->set_rules('curso_conta_vr_diaria_inicial', 'Vr. Diarias', 'required');
							
							// testa o formulário com a validação
							$sucesso = $this->form_validation->run();
							if ($sucesso) {
							
								// Carrega a conta_curso | Edita o saldo | Atualiza a conta_curso
								$cursoConta = $this->cursos_contas->carregar($post['pmk_curso_conta']);
								
								// Atualiza o saldo atual de diarias e dinheiro
								$post['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo']-$cursoConta[0]['curso_conta_qtd_diarias_inicial']+$post['curso_conta_qtd_diarias_inicial'];
								$post['curso_conta_saldo_dinheiro'] = $cursoConta[0]['curso_conta_saldo_dinheiro']-$cursoConta[0]['curso_conta_saldo_dinheiro_inicial']+$post['curso_conta_saldo_dinheiro_inicial'];
								
								// Se não tem saldo e vr diaria atuais, atualiza
								if ($cursoConta[0]['curso_conta_vr_diaria']==0){$post['curso_conta_vr_diaria']=$post['curso_conta_vr_diaria_inicial'];}
								if ($cursoConta[0]['curso_conta_saldo_dinheiro']==0){$post['curso_conta_saldo_dinheiro']=$post['curso_conta_saldo_dinheiro_inicial'];}
								
								
								$this->cursos_contas->editar($post);
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
								
							} else {
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else {
							$table = $this->cursos_contas->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('editar', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;
			
			case 'editar_valores_atuais':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras
							$this->form_validation->set_rules('curso_conta_saldo_dinheiro', 'Saldo em Dinheiro', 'required');
							$this->form_validation->set_rules('curso_conta_vr_diaria', 'Vr. Diarias', 'required');
							$this->form_validation->set_rules('curso_conta_saldo', 'Qtd. Diarias', 'required');
							
							// testa o formulário com a validação
							$sucesso = $this->form_validation->run();
							if ($sucesso) {
							
								// Carrega a conta_curso | Edita o saldo | Atualiza a conta_curso
								//$cursoConta = $this->cursos_contas->carregar($post['pmk_curso_conta']);
								//$post['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo']-$cursoConta[0]['curso_conta_qtd_diarias_inicial']+$post['curso_conta_qtd_diarias_inicial'];
								$this->cursos_contas->editar($post);
								
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
								
							} else {
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else {
							$table = $this->cursos_contas->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('editar_valores_atuais', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;
			
		}
	}
}

