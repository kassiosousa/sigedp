<?php
/* Model that represents Cursos_polos at database */

class ModelCursos_polos extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('cursos_polos tabela');
		$this->db->where('tabela.pmk_curso_polo', $idTable);
		$this->db->where('tabela.curso_polo_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar($tableParam = ''){

		$where = array('tabela.curso_polo_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_curso_polo'])){ $where += array('tabela.pmk_curso_polo' => $tableParam['pmk_curso_polo']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('cursos_polos tabela');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('cursos_polos',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_curso_polo'];
			
			$this->db->where('pmk_curso_polo', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('cursos_polos');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['curso_polo_is_ativo'] = 'Nao';
		$tableParam['pmk_curso_polo'] = $idTable;
		
		$this->db->where('pmk_curso_polo', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('cursos_polos');
	}
}
