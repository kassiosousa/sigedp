
<?php
	$tableList = $this->cursos_polos->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Cursos_polos<small class='hidden-xs'><br>Lista de Cursos_polos cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>cursos_polos/criar' data-header='Cadastrar Cursos_polos'>
				<i class='fa fa-plus'></i> &nbsp;Criar Cursos_polos
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>cursos_polos'><i class='fa fa-users'></i> Cursos_polos</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th><i class='icon_profile'></i> Pmk_curso_polo</th>
<th><i class='icon_profile'></i> Fok_curso</th>
<th><i class='icon_profile'></i> Fok_polo</th>
<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_curso_polo']; ?></td>
<td><?php echo $lista['fok_curso']; ?></td>
<td><?php echo $lista['fok_polo']; ?></td>
<td class='text-center acoes'>
					<div class='btn-group'>
						<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>cursos_polos/editar/?id=<?php echo $lista['pmk_curso_polo'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
						<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>cursos_polos/deletar/?id=<?php echo $lista['pmk_curso_polo'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
					</div>
				</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Pmk_curso_polo</th>
<th><i class='icon_profile'></i> Fok_curso</th>
<th><i class='icon_profile'></i> Fok_polo</th>
<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

