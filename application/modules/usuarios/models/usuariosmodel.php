<?php
/* Model that represents Sys_usuarios at database */

class Usuariosmodel extends CI_Model {

	public function carregar ($idTable){

		$this->db->select('*');
		$this->db->from('usuarios tabela');
		$this->db->where('tabela.pmk_usuario', $idTable);
		$this->db->where('tabela.usua_is_ativo', 'Sim');

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function carregar_desativado ($idUsuario, $tpUsuario){
		$this->db->select('*');
		$this->db->from('usuarios user');
		$this->db->where('user.pmk_usuario', $idUsuario);

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function carregar_por_email ($mailUsuario){
		$this->db->select('*');
		$this->db->from('usuarios user');
		$this->db->where('user.usua_email', $mailUsuario);

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function listar($tableParam = ''){

		$where = array('tabela.usua_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_usuario'])){ $where += array('tabela.pmk_usuario' => $tableParam['pmk_usuario']); }
		if(isset($tableParam['fok_usuariotipo'])){ $where += array('tabela.fok_usuariotipo' => $tableParam['fok_usuariotipo']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('usuarios tabela');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
		}

	public function criar($tableParam) {

		if (isset($tableParam)){
			$this->db->trans_start();
			$tableParam['usua_senha'] = MD5($tableParam['usua_senha']);
			$this->db->insert('usuarios',	$tableParam);
			$insert_id = $this->db->insert_id(); // << é necessário transação p/ usar isto corretamente
			$this->db->trans_complete();
			return $insert_id;
		} else {
			return false;
		}
	}

	public function editar ($tableParam) {

		if (isset($tableParam)){
			$idTable = $tableParam['pmk_usuario'];
			if (!empty($tableParam['usua_senha']))
			{
				$tableParam['usua_senha'] = MD5($tableParam['usua_senha']);
			}

			$this->db->where('pmk_usuario', $idTable);
			$this->db->set($tableParam);

			if ($this->db->update('usuarios'))
			{
				// se eu estiver alterando a mim mesmo
				if ($tableParam['pmk_usuario'] == $this->session->userdata('pmk_usuario'))
				{
					// atualiza sessão
					if (!empty($tableParam['fok_usuariotipo']))
					{
						$this->session->set_userdata('fok_usuariotipo', $tableParam['fok_usuariotipo']);
						$query = $this->db->get_where('usuarios_tipos',array('pmk_usuariotipo' => $tableParam['fok_usuariotipo']));
						$_result = $query->result();
						$this->session->set_userdata('usuatipo_nivel_acesso', $_result[0]->usuatipo_nivel_acesso);
					}
					if (!empty($tableParam['usua_email'])) $this->session->set_userdata('usua_email', $tableParam['usua_email']);
					if (!empty($tableParam['usua_nome'])) $this->session->set_userdata('usua_nome', $tableParam['usua_nome']);
				}
				return true;
			}
			else return false;
		} else {
			return false;
		}
	}

	public function deletar ($idTable) {
		$tableParam['usua_is_ativo'] = 'Nao';
		$tableParam['pmk_usuario'] = $idTable;
		$tableParam['usua_email'] = ''; // << limpamos o e-mail para que o mesmo e-mail possa ser usado

		$this->db->where('pmk_usuario', $idTable);
		$this->db->set($tableParam);

		return $this->db->update('usuarios');
	}

	public function tipos() {
		// retorna os tipos de usuarios num array associativo
		$query = $this->db->get_where('usuarios_tipos', array(
			'usuatipo_nivel_acesso >' => '0',
			'usuatipo_is_ativo' => 'Sim'
		));
		$_results = $query->result();
		$results = array();
		foreach ($_results as $result) {
			$results[ $result->pmk_usuariotipo ] = $result->usuatipo_nome;
		}
		return $results;
	}

	public function coordenadores_de_curso() {
		// retorna os coordenadores de curso num array associativo
		$query = $this->db->get_where('usuarios', array(
			'fok_usuariotipo' => '4',
			'usua_is_ativo' => 'Sim'
		));
		$_results = $query->result();
		$results = array();
		foreach ($_results as $result) {
			$results[ $result->pmk_usuario ] = $result->usua_nome;
		}
		return $results;
	}

    public function todos() {
        // retorna todos num array associativo
        $query = $this->db->get('usuarios');
        $_results = $query->result();
        $results = array();
        foreach ($_results as $result) {
            $results[ $result->pmk_usuario ] = $result->usua_nome;
        }
        return $results;
    }
}
