<?php
/* CRUD Controller for usuarios */

class Usuarios extends CI_Controller{

	function _remap($method){
		$this->load->model('usuarios/usuariosmodel', 'usuarios');

		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists("usuatipo_nivel_acesso", $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }

		switch( $method ) {
			case 'criar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata["usuatipo_nivel_acesso"] > 2) {
						echo json_encode("nivel de acesso insuficiente");
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras
							$this->form_validation->set_rules('usua_nome', 'Nome', 'required');
							$this->form_validation->set_rules('usua_email', 'E-mail', 'required|valid_email|is_unique[usuarios.usua_email]');
							$this->form_validation->set_rules('fok_usuariotipo', 'Nível', 'required|is_natural_no_zero');
							$this->form_validation->set_rules('usua_senha', 'Senha', 'required|min_length[3]');
							$this->form_validation->set_rules('usua_cpf', 'CPF', 'exact_length[14]');
							$this->form_validation->set_rules('usua_telefone', 'Telefone', 'exact_length[15]');
							$this->form_validation->set_rules('usua_end_cep', 'CEP', 'exact_length[9]');

							// a validação de data é mais coisada
							$this->load->helper('data');

							if ($data_nascimento_fixed = valid_date_str($post['usua_data_nascimento']))
							{
								// valida data e conserta para IE antigo e Firefox
								$post['usua_data_nascimento'] = $data_nascimento_fixed;
							}
							else
							{
								$post['usua_data_nascimento'] = null;
							}

							// testa o formulário com a validação
							$sucesso = $this->form_validation->run();
							if ($sucesso)
							{
								$this->usuarios->criar($post);
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							}
							else
							{
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else
						{
							// renderiza o modal
							$data = array();
							$data['estados'] = $this->db->order_by('estado_nome')->get('estado')->result_array();
							$this->load->view('criar',$data);
						}
					}
				}
				else
				{
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'editar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata["usuatipo_nivel_acesso"] > 2)
					{
						echo json_encode("nivel de acesso insuficiente");
					}
					else
					{
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras
							$this->form_validation->set_rules('usua_nome', 'Nome', 'required');
							$this->form_validation->set_rules('usua_email', 'E-mail', 'required|valid_email');
							$this->form_validation->set_rules('fok_usuariotipo', 'Nível', 'required|is_natural_no_zero');
							$this->form_validation->set_rules('usua_senha', 'Senha', 'min_length[3]');
							$this->form_validation->set_rules('usua_cpf', 'CPF', 'exact_length[14]');
							$this->form_validation->set_rules('usua_telefone', 'Telefone', 'exact_length[15]');
							$this->form_validation->set_rules('usua_end_cep', 'CEP', 'exact_length[9]');

							// a troca de senha é opcional
							if ($post['usua_senha'] == '') unset($post['usua_senha']);
							// a validação de data é mais coisada
							$this->load->helper('data');

							if ($data_nascimento_fixed = valid_date_str($post['usua_data_nascimento']))
							{
								// valida data e conserta para IE antigo e Firefox
								$post['usua_data_nascimento'] = $data_nascimento_fixed;
							}
							else
							{
								$post['usua_data_nascimento'] = null;
							}

							// testa o formulário com a validação
							$sucesso = $this->form_validation->run();
							if ($sucesso)
							{
								// necessário ainda testar o unique o e-mail
								$_unique = $this->usuarios->carregar_por_email($post['usua_email']);
								if ($_unique && $_unique[0]['pmk_usuario'] != $post['pmk_usuario'])
								{
									// tem que ser assim, pois é permitido o mesmo e-mail se o usuário for ele mesmo
									echo json_encode(array('usua_email' => 'Este registro para E-mail já existe. Por favor, escolha outro.'));
								}
								else
								{
									$this->usuarios->editar($post);
									echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
								}
							}
							else
							{
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else
						{
							$usuario = $this->usuarios->carregar($this->input->get('id'));
							$data = $usuario[0];
							$data['estados'] = $this->db->order_by('estado_nome')->get('estado')->result_array();
							if (!empty($data['fok_cidade'])) $data['cidade'] = $this->db->get_where('cidade',array('pmk_cidade' => $data['fok_cidade']))->row()->cidade_nome;
							// renderiza o modal
							$this->load->view('editar',$data);
						}
					}
				}
				else
				{
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'banco';
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata["usuatipo_nivel_acesso"] > 2)
					{
						echo json_encode("nivel de acesso insuficiente");
					}
					else
					{
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// não necessita validação
							$this->usuarios->editar($post);
							echo json_encode(false);
						}
						// se foi um GET
						else
						{
							$usuario = $this->usuarios->carregar($this->input->get('id'));
							$data = $usuario[0];
							// renderiza o modal
							$this->load->view('banco',$data);
						}
					}
				}
				else
				{
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'deletar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata["usuatipo_nivel_acesso"] > 2)
					{
						echo json_encode("nivel de acesso insuficiente");
					}
					else
					{
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							$this->usuarios->deletar($post['pmk_usuario']);
							echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
						}
						// se foi um GET
						else
						{
							$usuario = $this->usuarios->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('deletar',$usuario[0]);
						}
					}
				}
				else
				{
					echo 'essa requisição precisa vir por ajax';
				}

			break;

			case 'cidades':
				// serve para o ajax do select2
				$_result = $this->db->get_where('cidade',array('fok_estado' => $this->input->get('estado')))->result();
				$result = array();
				foreach ($_result as $cidade)
				{
					$c = new stdClass;
					$c->id = $cidade->pmk_cidade;
					$c->text = $cidade->cidade_nome;
					$result[] = $c;
				}
				echo json_encode($result);
			break;

			default:
				// SEGURANÇA - Somente Diretor e Coord.Admin
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('select2.min.css', 'crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'index', $templateExtras);
			break;
		}
	}
}

