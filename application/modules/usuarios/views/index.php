<?php
	$tableList = $this->usuarios->listar();
	$niveisUsuarios = $this->usuarios->tipos();
?>

<div class="row">
    <div class="col-xs-12">
        <h1>Usuários<small class="hidden-xs"><br>Lista de usuários cadastrados</small>
			<button class="btn btn-success pull-right modal-form" data-url="<?php echo base_url();?>usuarios/criar" data-header="Cadastrar Usuário">
				<i class="fa fa-plus"></i> &nbsp;Criar Usuário
			</button>
		</h1>
        <ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>home"><i class="fa fa-dashboard"></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>usuarios'><i class="fa fa-users"></i> Usuários</a></li>
			<li class="active">Listar</li>
		</ol>
    </div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-body">
				<table id="listTable" class="table table-bordered table-hover">
					<thead>
						<tr>
							<th class="visible-lg">Cód.</th>
							<th>Nome</th>
							<th>E-mail</th>
							<th>Nível</th>
							<th class="text-center" style="width:60px;"><i class="fa fa-money"></i> <i class="fa fa-dollar"></i></th>
							<th class="text-center acoes">Ações</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td class="visible-lg"><?php echo $lista['pmk_usuario']; ?></td>
									<td><?php echo $lista['usua_nome']; ?></td>
									<td><?php echo $lista['usua_email']; ?></td>
									<td><?php echo $niveisUsuarios[$lista['fok_usuariotipo']]; ?></td>
									<td class="text-center"><button class="btn btn-success btn-sm modal-form" title="Informações Bancárias" data-url="<?php echo base_url();?>usuarios/banco/?id=<?php echo $lista['pmk_usuario'];?>" data-header="Informações Bancárias"><i class="fa fa-bank"></button></td>
									<td class="text-center acoes">
										<div class='btn-group'>
											<button class='btn btn-primary btn-sm modal-form' data-url="<?php echo base_url();?>usuarios/editar/?id=<?php echo $lista['pmk_usuario'];?>" data-header="Editar Cadastro"><i class='fa fa-edit'></i> <span class="hidden-xs">Editar</span></button>
											<button class='btn btn-danger btn-sm modal-form' data-url="<?php echo base_url();?>usuarios/deletar/?id=<?php echo $lista['pmk_usuario'];?>" data-header="Confirmar Exclusão"><i class='fa fa-trash'></i> <span class="hidden-xs">Excluir</span></button>
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th class="visible-lg">Cód.</th>
							<th>Nome</th>
							<th>E-mail</th>
							<th>Nível</th>
							<th class="text-center"><i class="fa fa-money"></i> <i class="fa fa-dollar"></i></th>
							<th class="text-center acoes">Ações</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>
