<form>
    <input type="hidden" name="pmk_usuario" value="<?php echo $pmk_usuario; ?>">
    <div class="form-group">
        <label for="usua_banco_nome" class="control-label">Banco</label>
        <input type="text" class="form-control" id="usua_banco_nome" name="usua_banco_nome" value="<?php echo $usua_banco_nome; ?>" placeholder="Banco do Brasil, Caixa Econômica, etc" required>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group" style="margin-left: 0; margin-right: 0;">
                <label for="usua_banco_agencia" class="control-label">Agência</label>
                <input type="text" class="form-control" id="usua_banco_agencia" name="usua_banco_agencia" value="<?php echo $usua_banco_agencia; ?>" required>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group" style="margin-left: 0; margin-right: 0;">
                <label for="usua_banco_conta" class="control-label">Conta:</label>
                <input type="text" class="form-control" id="usua_banco_conta" name="usua_banco_conta" value="<?php echo $usua_banco_conta; ?>" required>
            </div>
        </div>
    </div>
</form>
