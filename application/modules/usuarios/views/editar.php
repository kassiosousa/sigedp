<!-- Modal de Editar Usuário -->
<?php
	$niveisUsuarios = $this->usuarios->tipos();
?>
<form class="form-horizontal">
	<input type="hidden" name="pmk_usuario" value="<?php echo $pmk_usuario; ?>">
	<div class="form-group">
		<label for="usua_nome" class="col-sm-2 control-label">Nome</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="usua_nome" name="usua_nome" value="<?php echo $usua_nome; ?>" placeholder="Nome e Sobrenome" required>
		</div>
	</div>
	<div class="form-group">
		<label for="usua_email" class="col-sm-2 control-label">E-mail</label>
		<div class="col-sm-10">
			<input type="email" class="form-control" id="usua_email" name="usua_email" value="<?php echo $usua_email; ?>" placeholder="E-mail" required>
		</div>
	</div>
	<div class="form-group">
		<label for="fok_usuariotipo" class="col-sm-2 control-label">Nível</label>
		<div class="col-sm-10">
			<select name="fok_usuariotipo" id="fok_usuariotipo" class="form-control" required>
				<?php foreach ($niveisUsuarios as $key => $nivel): ?>
				<option value="<?php echo $key; ?>" <?php echo ($key == $fok_usuariotipo ? 'selected':''); ?>><?php echo $nivel; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="usua_senha" class="col-sm-2 control-label">Senha</label>
		<div class="col-sm-10">
			<input type="password" class="form-control" id="usua_senha" name="usua_senha" placeholder="Deixe em branco para não alterar">
		</div>
	</div>
	<fieldset>
		<legend>
			<small class="text-muted">Informações Pessoais</small>
		</legend>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_data_criacao" class="control-label">Cadastrado em</label>
					<input type="datetime" class="form-control input-date" id="usua_data_criacao" value="<?php echo $usua_data_criacao; ?>" readonly>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_data_nascimento" class="control-label">Data de Nascimento</label>
					<input type="date" class="form-control input-date" id="usua_data_nascimento" name="usua_data_nascimento" value="<?php echo $usua_data_nascimento; ?>">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_cpf" class="control-label">CPF</label>
					<input type="text" class="form-control" id="usua_cpf" name="usua_cpf" value="<?php echo $usua_cpf; ?>" placeholder="000.000.000-00" data-mask="999.999.999-99">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_telefone" class="control-label">Telefone</label>
					<input type="text" class="form-control" id="usua_telefone" name="usua_telefone" value="<?php echo $usua_telefone; ?>" placeholder="(XX) 99999-9999" data-mask="(99) 99999-9999">
				</div>
			</div>
			<div class="col-sm-8">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_end_logradouro" class="control-label">Logradouro:</label>
					<input type="text" class="form-control" id="usua_end_logradouro" name="usua_end_logradouro" value="<?php echo $usua_end_logradouro; ?>" placeholder="Rua, Avenida, Travessa, etc">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_end_numero" class="control-label">Número</label>
					<input type="text" class="form-control" id="usua_end_numero" name="usua_end_numero" value="<?php echo $usua_end_numero; ?>" placeholder="Casa, Apt.">
				</div>
			</div>
			<div class="col-sm-8">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_end_complemento" class="control-label">Complemento, Bairro:</label>
					<input type="text" class="form-control" id="usua_end_complemento" name="usua_end_complemento" value="<?php echo $usua_end_complemento; ?>" placeholder="Qd., Conj., Bairro">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="uf" class="control-label">UF</label>
					<select id="uf" class="form-control">
						<option>- Selecione -</option>
						<?php foreach ($estados as $estado): ?>
						<option value="<?php echo $estado['pmk_estado']; ?>"><?php echo $estado['estado_nome']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="fok_cidade" class="control-label">Cidade</label>
					<select id="fok_cidade" name="fok_cidade" class="form-control select-cidade" data-url="<?php echo base_url();?>usuarios/cidades">
						<?php if(empty($fok_cidade)): ?>
						<option>- Selecione UF -</option>
						<?php else: ?>
						<option value="<?php echo $fok_cidade; ?>"><?php echo $cidade; ?></option>
						<?php endif;?>
					</select>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group" style="margin-left: 0; margin-right: 0;">
					<label for="usua_end_cep" class="control-label">CEP</label>
					<input type="text" class="form-control" id="usua_end_cep" name="usua_end_cep" value="<?php echo $usua_end_cep; ?>" placeholder="99999-999" data-mask="99999-999">
				</div>
			</div>
		</div>
	</fieldset>
</form>
