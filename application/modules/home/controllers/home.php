<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function _remap($method){
		//Carrega os modelos a serem utilizados
		$this->load->model('login/loginmodel', 'login');
		$this->load->model('usuarios/usuariosmodel', 'usuarios');
		$this->load->model('configuracoes/modelconfiguracoes', 'configuracoes');
		$this->load->model('pedidos/modelpedidos', 'pedidos');
		$this->load->model('Pedidos_relatorios_viagens/modelpedidos_relatorios_viagens', 'pedidos_relatorios_viagens');
		$this->load->model('planos_pedagogicos_disciplinas/modelplanos_pedagogicos_disciplinas', 'planos_pedagogicos_disciplinas');
		
		switch( $method ) {

			case 'perfil':
				// SEGURANÇA - Impedir acesso a área restrita
				if (isset($this->session->userdata))
				{
					if (
						(!(array_key_exists("usuatipo_nivel_acesso", $this->session->userdata)))||
						($this->session->userdata('base_url') != base_url())
						)
					{
						redirect('login/logout', 'refresh');
					}
				}
				else
				{
					redirect('login/logout', 'refresh');
				}

				// pega todo o post já limpo de html/css/js (anti trolls)
				$post = $this->input->post(NULL, TRUE);

				// tratamento do POST
				if($post)
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA
					$post['pmk_usuario'] = $this->session->userdata('pmk_usuario');
					unset($post['usua_nome']);
					unset($post['fok_usuariotipo']);

					// usaremos o codeigniter para validar nosso input
					$this->load->library('form_validation');
					$this->lang->load('form_validation', 'portuguese'); // << traduções

					// regras
					$this->form_validation->set_rules('usua_email', 'E-mail', 'required|valid_email');
					$this->form_validation->set_rules('usua_senha', 'Senha', 'min_length[3]|matches[usua_senha_confirma]');
					$this->form_validation->set_rules('usua_cpf', 'CPF', 'exact_length[14]');

					// a troca de senha é opcional
					if ($post['usua_senha'] == '') unset($post['usua_senha']);

					// a validação de data é mais coisada
					$this->load->helper('data');

					if ($data_nascimento_fixed = valid_date_str($post['usua_data_nascimento']))
					{
						// valida data e conserta para IE antigo e Firefox
						$post['usua_data_nascimento'] = $data_nascimento_fixed;
					}
					else
					{
						$post['usua_data_nascimento'] = null;
					}

					// testa o formulário com a validação
					$sucesso = $this->form_validation->run();
					if ($sucesso)
					{
						$outros_erros = array();

						// necessário ainda testar o unique o e-mail
						$_unique = $this->usuarios->carregar_por_email($post['usua_email']);
						if ($_unique && $_unique[0]['pmk_usuario'] != $post['pmk_usuario'])
						{
							// tem que ser assim, pois é permitido o mesmo e-mail se o usuário for ele mesmo
							$outros_erros['usua_email'] = 'Este registro para E-mail já existe. Por favor, escolha outro.';
						}
						if (!empty($post['usua_senha']))
						{
							$eu = $this->usuarios->carregar($post['pmk_usuario']);
							// necessário informar se a senha anterior é válida
							if ($eu[0]['usua_senha'] != MD5($post['usua_senha_atual']))
							{
								$outros_erros['usua_senha_atual'] = 'A Senha Atual informada está incorreta.';
							}
						}

						// se não houveram outros erros
						if(empty($outros_erros))
						{
							unset($post['usua_senha_atual']);
							unset($post['usua_senha_confirma']);
							$this->usuarios->editar($post);
							echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
						}
						else
						{
							echo json_encode($outros_erros);
						}
					}
					else
					{
						echo json_encode($this->form_validation->get_all_errors_array());
					}
				}
				// tratamento do GET
				else
				{
					// css e js exclusivos para essa página
					$templateExtras = array(
						'eu' => $this->usuarios->carregar($this->session->userdata('pmk_usuario'))[0],
						'estados' => $this->db->order_by('estado_nome')->get('estado')->result_array(),
						'styles' => array('select2.min.css'),
						'scripts' => array('bootbox.min.js','jquery.maskedinput.js', 'select2.min.js', 'perfil.js')
					);

					if (!empty($templateExtras['eu']['fok_cidade']))
					{
						$templateExtras['eu']['cidade'] = $this->db->get_where('cidade',array('pmk_cidade' => $templateExtras['eu']['fok_cidade']))->row()->cidade_nome;
					}

					//========================
					// Método antigo -> $this->load->view("perfil");
					$this->template->load('templates/template_painel', 'perfil', $templateExtras);
					//========================
				}
			break;

			default:
				// SEGURANÇA - Impedir acesso a área restrita
				if (isset($this->session->userdata))
				{
					if ( (!(array_key_exists("usuatipo_nivel_acesso", $this->session->userdata)))||($this->session->userdata('base_url') != base_url()) )
					{
						redirect('login/logout', 'refresh');
					}
				}
				else
				{
					redirect('login/logout', 'refresh');
				}

				// Teste de login/sessão
				$idUsuario = $this->session->userdata('pmk_usuario');

				if ($idUsuario > 0) { // Carregou o perfil do usuário
					$this->template->load('templates/template_painel', 'index');
				} else { // Nao carregou perfil, direcionar para novo login
					//Devolve ao início
					redirect('home/login', 'refresh');
				}
			break;
		}
	}

}
