<?php
	// Lista de configurações usadas no sistema
	$tableList = $this->configuracoes->listar();
	$usuario['pmk_usuario'] = $this->session->userdata('pmk_usuario');
	
	// Contador de pedidos criados
	$pedidosGerados = $this->pedidos->listar();
	
	// Contador de discplinas criados
	$disciplinasGeradas = $this->planos_pedagogicos_disciplinas->listar();
?>
<div class="row">
    <div class="col-xs-12">
        <h1 class="page-header">
            Painel Administrativo <small>Dados gerais do sistema</small>
		</h1>
		
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> DADOS MOSTRADOS ABAIXO, AINDA NÃO CONSULTAM O SISTEMA
			</li>
		</ol>
	</div>
</div>

<div class="row">
    <div class="col-xs-12">
		<div class="row">
		
			<div class="col-lg-6 col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-comments fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge"><?php echo count($pedidosGerados); ?></div>
								<div>Pedidos Gerados no sistema</div>
							</div>
						</div>
					</div>
					<a href="pedidos">
						<div class="panel-footer">
							<span class="pull-left">Detalhes</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-tasks fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge"><?php echo count($disciplinasGeradas); ?></div>
								<div>Disciplinas Criadas</div>
							</div>
						</div>
					</div>
					<a href="planos_pedagogicos">
						<div class="panel-footer">
							<span class="pull-left">Detalhes</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-bell fa-fw"></i> Configurações Utilizadas</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="list-group">
					<a href="#" class="list-group-item">
						<i class="fa fa-comment fa-fw"></i> Porcentagem de hora teórica que deve ser convertido em encontros 
						<span class="pull-right text-muted small"><?php echo $tableList[0]['config_valor']; ?>%</span>
					</a>
					<a href="#" class="list-group-item">
						<i class="fa fa-comment fa-fw"></i> Porcentagem de hora prática que deve ser convertido em encontros
						<span class="pull-right text-muted small"><?php echo $tableList[1]['config_valor']; ?>%</span>
					</a>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
</div>

<!-- ÁREA DE MODALS -->
<?php
	$qtdModalsAlerta = 0;
	$arrayModalsAlerta = array();
	$boolAlertRelViagemPend = $tableList[4]['config_valor'];
	$boolAlertDataViagemFinalizada = $tableList[5]['config_valor'];
	$boolAlertNovasSolicDiaria = $tableList[6]['config_valor'];
	$boolAlertProximoDataEnvioRel = $tableList[7]['config_valor'];	
	
	// Se tem alguma configuração de alerta permitido
	if ($boolAlertRelViagemPend==1||$boolAlertDataViagemFinalizada==1||$boolAlertNovasSolicDiaria==1||$boolAlertProximoDataEnvioRel){
		
		// Verifica alerta: Possui um relatório de viagem pendente
		// Verifica alerta: Está próximo do limite da data de envio do relatório
		// Verifica alerta: Possui uma viagem que a data já finalizou
		$today = date("Y-m-d H:i:s");
		
			// Lista todos os pedidos deste usuario
			$pediParam['fok_usuario'] = $usuario['pmk_usuario'];
			$pedidosArr = $this->pedidos->listar($pediParam);
			if ($pedidosArr) {
				foreach ($pedidosArr as $pedido) {
					$dataRelPendente = date('Y-m-d H:i:s', strtotime($pedido['pedi_viagem_volta_dia'] . ' +7 day'));
					$dataRelPendenteProx = date('Y-m-d H:i:s', strtotime($pedido['pedi_viagem_volta_dia'] . ' +5 day'));
				
					// 5 -> Se esta liberado e Hoje > data_da_volta+7dias, deve ter um relatório
					// 		Se não tiver o relatório, manda o alerta
					if (
						($pedido['pedi_status'] == 'Liberado') &&
						($today > $dataRelPendente)
					) {
						$relParam['fok_pedido'] = $pedido['pmk_pedido'];
						$relatorioViagem = $this->pedidos_relatorios_viagens->listar($relParam);
						if (!$relatorioViagem) { // Se não tem relatório (Deveria ter)
							$arrayModalsAlerta[$qtdModalsAlerta]['tituloAlerta'] = "Você possui um relatório de viagem pendente";
							$arrayModalsAlerta[$qtdModalsAlerta]['mensagemAlerta'] = 
								"O Pedido de código '".$pedido['pmk_pedido']."' está pendente. O prazo de 7 dias para enviar o relatório
								de viagem esgotou. Para ter direito a novos pedidos, regularize sua situação enviando o 
								relatório.";
							$qtdModalsAlerta++;
						}
					}
					// 8 -> Se esta liberado e Hoje > data_da_volta+5dias, esta próximo de ter um relatório
					// 		Se não tiver o relatório, manda o alerta
					if (
						($pedido['pedi_status'] == 'Liberado') &&
						( $today > $dataRelPendenteProx)
					) {
						$relParam['fok_pedido'] = $pedido['pmk_pedido'];
						$relatorioViagem = $this->pedidos_relatorios_viagens->listar($relParam);
						if (!$relatorioViagem) { // Se não tem relatório (Deveria ter)
							$arrayModalsAlerta[$qtdModalsAlerta]['tituloAlerta'] = "Você possui um relatório de viagem próximo de ficar pendente";
							$arrayModalsAlerta[$qtdModalsAlerta]['mensagemAlerta'] = 
								"O Pedido de código '".$pedido['pmk_pedido']."' está próximo de ficar pendente. O prazo de 7 dias
								para enviar o relatório	de viagem está próximo de esgotar. Para ter direito a 
								novos pedidos, lembre de manter regularizada a sua situação enviando o relatório.";
							$qtdModalsAlerta++;
						}
					}
					// 6 -> Se esta liberado e Hoje > data_da_volta viagem finalizada
					if (
						($pedido['pedi_status'] == 'Liberado') &&
						( $today > $pedido['pedi_viagem_volta_dia'])
					) {
						$arrayModalsAlerta[$qtdModalsAlerta]['tituloAlerta'] = "Você possui uma viagem finalizada";
						$arrayModalsAlerta[$qtdModalsAlerta]['mensagemAlerta'] = 
							"O Pedido de código '".$pedido['pmk_pedido']."' está próximo de ficar pendente. O prazo de 7 dias
							para enviar o relatório	de viagem está contando. Para ter direito a 
							novos pedidos, lembre de manter regularizada a sua situação enviando o relatório.";
						$qtdModalsAlerta++;
					}
					// 5 -> Se esta pendente, possui novos pedidos
					if (
						($pedido['pedi_status'] == 'Pendente') &&
						( $today > $pedido['pedi_viagem_volta_dia'])
					) {
						$arrayModalsAlerta[$qtdModalsAlerta]['tituloAlerta'] = "Você possui novos pedidos";
						$arrayModalsAlerta[$qtdModalsAlerta]['mensagemAlerta'] = 
							"O Pedido de código '".$pedido['pmk_pedido']."' foi criado para você. 
							Acompanhe o seu andamento e situação.";
						$qtdModalsAlerta++;
					}
					
					
				}
			}
		
		
		// Verifica alerta: Uma nova diária foi solicitada pra você
		
		
		$boolMostrarModal = 1;
	} else { $boolMostrarModal = 0; }

	
	$tituloAlerta = "";
	$textoAlerta = "";
	
?>
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script>
	<?php 
	if ($arrayModalsAlerta) { 
		foreach ($arrayModalsAlerta as $keyModal => $arrayModal) {
	?>
	$( document ).ready(function() {
		$('#myModal<?php echo $keyModal;?>').modal('show');
	});
	<?php 
		}
	}
	?>
</script>
<?php 
if ($arrayModalsAlerta) { 
	foreach ($arrayModalsAlerta as $keyModal => $arrayModal) {
?>
<div class="modal fade" id="myModal<?php echo $keyModal;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel"><?php echo $arrayModal['tituloAlerta'];?></h4>
			</div>
			<div class="modal-body"><?php echo $arrayModal['mensagemAlerta'];?></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				<!--<button type="button" class="btn btn-primary">Save changes</button>-->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php 
	}
}
?>