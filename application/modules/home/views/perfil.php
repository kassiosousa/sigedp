<?php
    $niveisUsuarios = $this->usuarios->tipos();
?>
<div class="row">
    <div class="col-xs-12">
        <h1>Minha Conta<small class='hidden-xs'><br>Informações cadastrais</small></h1>
        <ol class='breadcrumb'>
            <li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
            <li><i class='fa fa-user'></i> Minha Conta</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <form method="post" class="form-horizontal">
            <div class="form-group">
                <label for="usua_nome" class="col-sm-2 control-label">Nome</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="usua_nome" value="<?php echo $eu['usua_nome']; ?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="fok_usuariotipo" class="col-sm-2 control-label">Nível</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="fok_usuariotipo" value="<?php echo $niveisUsuarios[$eu['fok_usuariotipo']]; ?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="usua_email" class="col-sm-2 control-label">E-mail</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="usua_email" name="usua_email" value="<?php echo $eu['usua_email']; ?>" placeholder="E-mail" required>
                </div>
            </div>
            <div class="form-group">
                <label for="usua_cpf" class="col-sm-2 control-label">CPF</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="usua_cpf" name="usua_cpf" value="<?php echo $eu['usua_cpf']; ?>" placeholder="000.000.000-00" data-mask="999.999.999-99">
                </div>
            </div>
            <div class="form-group">
                <label for="usua_telefone" class="col-sm-2 control-label">Telefone</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="usua_telefone" name="usua_telefone" value="<?php echo $eu['usua_telefone']; ?>" placeholder="(XX) 99999-9999" data-mask="(99) 99999-9999">
                </div>
            </div>
            <div class="form-group">
                <label for="usua_data_nascimento" class="col-sm-2 control-label">Data de Nascimento</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control input-date" id="usua_data_nascimento" name="usua_data_nascimento" value="<?php echo $eu['usua_data_nascimento']; ?>">
                </div>
            </div>
            <fieldset>
                <legend>
                    <small class="text-muted">Endereço</small>
                </legend>
                <div class="row">
                    <div class="col-xs-12">
                        <label for="usua_end_logradouro" class="control-label">Logradouro</label>
                        <input type="text" class="form-control" id="usua_end_logradouro" name="usua_end_logradouro" value="<?php echo $eu['usua_end_logradouro']; ?>" placeholder="Rua, Avenida, Travessa, etc">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="usua_end_numero" class="control-label">Número</label>
                            <input type="text" class="form-control" id="usua_end_numero" name="usua_end_numero" value="<?php echo $eu['usua_end_numero']; ?>" placeholder="Casa, Apt.">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="usua_end_complemento" class="control-label">Complemento, Bairro:</label>
                            <input type="text" class="form-control" id="usua_end_complemento" name="usua_end_complemento" value="<?php echo $eu['usua_end_complemento']; ?>" placeholder="Qd., Conj., Bairro">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="uf" class="control-label">UF</label>
                            <select id="uf" class="form-control">
                                <option>- Selecione -</option>
                                <?php foreach ($estados as $estado): ?>
                                <option value="<?php echo $estado['pmk_estado']; ?>"><?php echo $estado['estado_nome']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="fok_cidade" class="control-label">Cidade</label>
                            <select id="fok_cidade" name="fok_cidade" class="form-control select-cidade" data-url="<?php echo base_url();?>usuarios/cidades">
                                <?php if(empty($eu['fok_cidade'])): ?>
                                <option>- Selecione UF -</option>
                                <?php else: ?>
                                <option value="<?php echo $eu['fok_cidade']; ?>"><?php echo $eu['cidade']; ?></option>
                                <?php endif;?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="usua_end_cep" class="control-label">CEP</label>
                            <input type="text" class="form-control" id="usua_end_cep" name="usua_end_cep" value="<?php echo $eu['usua_end_cep']; ?>" placeholder="99999-999" data-mask="99999-999">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>
                    <small class="text-muted">Dados Bancários</small>
                </legend>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="usua_banco_nome" class="control-label">Banco</label>
                            <input type="text" class="form-control" id="usua_banco_nome" name="usua_banco_nome" value="<?php echo $eu['usua_banco_nome']; ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="usua_banco_agencia" class="control-label">Agência</label>
                            <input type="text" class="form-control" id="usua_banco_agencia" name="usua_banco_agencia" value="<?php echo $eu['usua_banco_agencia']; ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group" style="margin-left: 0; margin-right: 0;">
                            <label for="usua_banco_conta" class="control-label">Conta:</label>
                            <input type="text" class="form-control" id="usua_banco_conta" name="usua_banco_conta" value="<?php echo $eu['usua_banco_conta']; ?>">
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="form-group bg-warning" style="padding: 20px 0;">
                <label for="btn-alterar-senha" class="col-sm-2 control-label">Senha</label>
                <div class="col-sm-10">
                    <button type="button" id="btn-alterar-senha" class="btn btn-warning">Alterar Senha</button>
                </div>
            </div>
            <div id="div-alterar-senha" style="display:none;padding:20px;margin-bottom:20px;" class="bg-warning">
                <div class="form-group">
                    <label for="usua_senha_atual" class="col-sm-2 control-label">Senha Atual</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="usua_senha_atual" name="usua_senha_atual">
                    </div>
                </div>
                <div class="form-group">
                    <label for="usua_senha" class="col-sm-2 control-label">Nova Senha</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="usua_senha" name="usua_senha">
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:0;">
                    <label for="usua_senha_confirma" class="col-sm-2 control-label">Confirma Nova Senha</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="usua_senha_confirma" name="usua_senha_confirma">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="row col-lg-3 col-md-4 col-sm-5 col-xs-6">
                        <button id="btn-submit" type="button" class="btn btn-primary btn-block">Atualizar Dados</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
