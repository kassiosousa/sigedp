<?php
/* CRUD Controller for Polos */

class Polos extends CI_Controller {

	function _remap($method){
		//Carrega os modelos a serem utilizados
		$this->load->model('Polos/modelpolos', 'polos');
		$this->load->model('Configuracoes/modelconfiguracoes', 'configuracoes');

		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }

		// chama a função de acordo com o método (method)
		switch( $method ) {

			default:
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('select2.min.css', 'crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'index', $templateExtras);
			break;

			case 'criar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('Nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post) {

							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras do formulário (Deve ser adaptado para cada módulo)
							$this->form_validation->set_rules('polo_titulo', 'Título', 'required');
							// $this->form_validation->set_rules('usua_email', 'E-mail', 'required|valid_email|is_unique[usuarios.usua_email]');
							// $this->form_validation->set_rules('fok_usuariotipo', 'Nível', 'required|is_natural_no_zero');
							// $this->form_validation->set_rules('usua_senha', 'Senha', 'required|min_length[3]');
							// $this->form_validation->set_rules('usua_cpf', 'CPF', 'exact_length[14]');
							// a validação de data é mais coisada
							// $this->load->helper('data');

							//if ($data_nascimento_fixed = valid_date_str($post['usua_data_nascimento'])){
								// valida data e conserta para IE antigo e Firefox
								//$post['usua_data_nascimento'] = $data_nascimento_fixed;
							//} else {
								//$post['usua_data_nascimento'] = null;
							//}

							// testa o formulário com a validação informada
							$sucesso = $this->form_validation->run();

							if ($sucesso) {
								$this->polos->criar($post);
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							}
							else
							{
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else
						{
                            // renderiza o modal
                            $data = array();
                            $data['estados'] = $this->db->order_by('estado_nome')->get('estado')->result_array();
                            $this->load->view('criar',$data);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'editar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras
							$this->form_validation->set_rules('polo_titulo', 'Título', 'required');
							$this->form_validation->set_rules('polo_descricao', 'Descrição', 'required');
							// $this->form_validation->set_rules('usua_email', 'E-mail', 'required|valid_email|is_unique[usuarios.usua_email]');
							// $this->form_validation->set_rules('fok_usuariotipo', 'Nível', 'required|is_natural_no_zero');
							// $this->form_validation->set_rules('usua_senha', 'Senha', 'min_length[3]');
							// $this->form_validation->set_rules('usua_cpf', 'CPF', 'exact_length[14]');
							// a troca de senha é opcional
							//if ($post['usua_senha'] == '') unset($post['usua_senha']);
							// a validação de data é mais coisada
							// $this->load->helper('data');

							//if ($data_nascimento_fixed = valid_date_str($post['usua_data_nascimento'])) {
								// valida data e conserta para IE antigo e Firefox
								//$post['usua_data_nascimento'] = $data_nascimento_fixed;
							//} else {
								//$post['usua_data_nascimento'] = null;
							//}

							// testa o formulário com a validação
							$sucesso = $this->form_validation->run();
							if ($sucesso) {
								$this->polos->editar($post);
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							} else {
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else {
							$table = $this->polos->carregar($this->input->get('id'));
                            $data = $table[0];
                            $data['estados'] = $this->db->order_by('estado_nome')->get('estado')->result_array();
                            if (!empty($data['fok_cidade'])) $data['cidade'] = $this->db->get_where('cidade',array('pmk_cidade' => $data['fok_cidade']))->row()->cidade_nome;
							// renderiza o modal
							$this->load->view('editar', $data);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'deletar':
				// ajax
				if ($this->input->is_ajax_request()) {
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2){
						echo json_encode('nivel de acesso insuficiente');
					} else {

						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post) {
							$this->polos->deletar($post['pmk_polo']);
							echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
						}
						// se foi um GET
						else
						{
							$table = $this->polos->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('deletar', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;


		}
	}
}

