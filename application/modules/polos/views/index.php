
<?php
	$tableList = $this->polos->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Polos<small class='hidden-xs'><br>Lista de Polos cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>polos/criar' data-header='Cadastrar Polo'>
				<i class='fa fa-plus'></i> &nbsp;Criar Polo
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>polos'><i class='fa fa-users'></i> Polos</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th style="width: 50px"><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Polo</th>
							<th style="width: 220px"><i class='icon_profile'></i> Cidade</th>
							<td style="width: 160px"></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_polo']; ?></td>
									<td>
										<?php echo $lista['polo_titulo']; ?>
										<small class="text-muted"><br><?php echo $lista['polo_descricao']; ?></small>
									</td>
									<td><?php echo $lista['cidade_nome']; ?></td>
									<td class='text-center acoes'>
										<div class='btn-group'>
											<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>polos/editar/?id=<?php echo $lista['pmk_polo'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
											<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>polos/deletar/?id=<?php echo $lista['pmk_polo'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Polo</th>
							<th><i class='icon_profile'></i> Cidade</th>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

