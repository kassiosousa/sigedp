<!-- Modal de Editar Usu�rio -->

<form class='form-horizontal'>
	<input type='hidden' name='pmk_polo' value='<?php echo $pmk_polo; ?>'>

    <div class="form-group">
        <label for="uf" class="col-sm-4 control-label">UF</label>
        <div class="col-sm-8">
            <select id="uf" class="form-control">
                <option>- Selecione -</option>
                <?php foreach ($estados as $estado): ?>
                <option value="<?php echo $estado['pmk_estado']; ?>"><?php echo $estado['estado_nome']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

	<div class="form-group">
		<label for="fok_cidade" class="col-sm-4 control-label">Cidade</label>
		<div class="col-sm-8">
			<select name="fok_cidade" id="fok_cidade" class="form-control select-cidade" style="width:100%;" required data-url="<?php echo base_url();?>usuarios/cidades">
				<?php if(empty($fok_cidade)): ?>
                <option>- Selecione UF -</option>
                <?php else: ?>
                <option value="<?php echo $fok_cidade; ?>"><?php echo $cidade; ?></option>
                <?php endif;?>
			</select>
		</div>
	</div>


	<div class='form-group'>
		<label for='Polo_titulo' class='col-sm-4 control-label'>Titulo</label>
		<div class='col-sm-8'>
			<input type='text' class='form-control' id='polo_titulo' name='polo_titulo' value='<?php echo $polo_titulo; ?>' placeholder='Titulo' required>
		</div>
	</div>
	<div class='form-group'>
		<label for='Polo_descricao' class='col-sm-4 control-label'>Descrição:</label>
		<div class='col-sm-8'>
			<input type='text' class='form-control' id='polo_descricao' name='polo_descricao' value='<?php echo $polo_descricao; ?>' placeholder='Descrição' required>
		</div>
	</div>
</form>

