<?php
/* Model that represents Polos at database */

class ModelPolos extends CI_Model {

	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('polos tabela');
		$this->db->join('cidade cidade', 'tabela.fok_cidade = cidade.pmk_cidade');
		$this->db->where('tabela.pmk_polo', $idTable);
		$this->db->where('tabela.polo_is_ativo', 'Sim');

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function listar($tableParam = ''){

		$where = array('tabela.polo_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_polo'])){ $where += array('tabela.pmk_polo' => $tableParam['pmk_polo']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('polos tabela');
		$this->db->join('cidade cidade', 'tabela.fok_cidade = cidade.pmk_cidade');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function criar($tableParam) {
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('polos',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();
			return $insert_id;
		} else {
			return false;
		}
	}

	public function editar ($tableParam) {

		if (isset($tableParam)){
			$idTable = $tableParam['pmk_polo'];

			$this->db->where('pmk_polo', $idTable);
			$this->db->set($tableParam);

			return $this->db->update('polos');
		} else {
			return false;
		}
	}

	public function deletar ($idTable) {
		$tableParam['polo_is_ativo'] = 'Nao';
		$tableParam['pmk_polo'] = $idTable;

		$this->db->where('pmk_polo', $idTable);
		$this->db->set($tableParam);

		return $this->db->update('polos');
	}

    public function todos() {
        // retorna todos num array associativo
        $query = $this->db->get('polos');
        $_results = $query->result();
        $results = array();
        foreach ($_results as $result) {
            $results[ $result->pmk_polo ] = $result->polo_titulo;
        }
        return $results;
    }

    public function por_curso($pmk_curso) {
        // retorna todos num array associativo
        $query = $this->db->get_where('cursos_polos',array('fok_curso' => $pmk_curso));
        $_results = $query->result();
        $results = array();
        foreach ($_results as $result) {
            $results[] = $result->fok_polo;
        }
        return $results;
    }
}
