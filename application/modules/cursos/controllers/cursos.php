<?php
	/* CRUD Controller for Cursos */

	class Cursos extends CI_Controller {

		function _remap($method){
			//Carrega os modelos a serem utilizados
			$this->load->model('cursos/modelcursos', 'cursos');
            $this->load->model('Polos/modelpolos', 'polos');
			$this->load->model('usuarios/usuariosmodel', 'usuarios');
			$this->load->model('cursos_secretarios/modelcursos_secretarios', 'cursos_secretarios');
			$this->load->model('cursos_contas/modelcursos_contas', 'cursos_contas');
			$this->load->model('Planos_pedagogicos/modelplanos_pedagogicos', 'planos_pedagogicos');

			// Teste de sessão ativa para acessar este módulo
			if (isset($this->session->userdata)) {
				if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
					redirect('login', 'refresh');
				}
			} else { redirect('login', 'refresh'); }

			// chama a função de acordo com o método (method)
			switch( $method ) {

				default:
					// SEGURANÇA - Level de acesso
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

					// css e js exclusivos para essa página
					$templateExtras = array(
						'styles' => array('select2.min.css', 'crud.css'),
						'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
					);

					$this->template->load('templates/template_painel', 'index', $templateExtras);
				break;

				case 'criar':
					// ajax
					if ($this->input->is_ajax_request()) {

						// tipo de retorno: JSON
						header('Content-Type: application/json');

						// SEGURANÇA - Nível requerido
						if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
							echo json_encode('Nivel de acesso insuficiente');
						} else {

							// pega todo o post já limpo de html/css/js (anti trolls)
							$post = $this->input->post(NULL, TRUE);

							// se foi um POST
							if ($post) {

								// usaremos o codeigniter para validar nosso input
								$this->load->library('form_validation');
								$this->lang->load('form_validation', 'portuguese'); // << traduções

								// regras do formulário
								$this->form_validation->set_rules('curso_titulo', 'Título', 'required');
								$this->form_validation->set_rules('fok_usua_coordenador', 'Coordenador', 'required|is_natural_no_zero');
								$this->form_validation->set_rules('curso_data_inicio', 'Data de Início', 'required');
								$this->form_validation->set_rules('curso_data_fim', 'Data de Fim', 'required');

								// a validação de data é mais coisada
								$this->load->helper('data');

								if ($data_inicio_fixed = valid_date_str($post['curso_data_inicio'])){
									// valida data e conserta para IE antigo e Firefox
									$post['curso_data_inicio'] = $data_inicio_fixed;
								} else {
									$post['curso_data_inicio'] = null;
									$_POST['curso_data_inicio'] = null; // << por causa do required
								}

								if ($data_fim_fixed = valid_date_str($post['curso_data_fim'])){
									// valida data e conserta para IE antigo e Firefox
									$post['curso_data_fim'] = $data_fim_fixed;
								} else {
									$post['curso_data_fim'] = null;
									$_POST['curso_data_fim'] = null; // << por causa do required
								}

								// testa o formulário com a validação informada
								$sucesso = $this->form_validation->run();

								if ($sucesso) {
									// Cria um curso
									$pmk_curso = $this->cursos->criar($post);
									// Cria o plano pedagogico dele (1-1)
									$planoParam['fok_curso'] = $pmk_curso;
									$this->planos_pedagogicos->criar($planoParam);

									echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
								}
								else
								{
									echo json_encode($this->form_validation->get_all_errors_array());
								}
							}
							// se foi um GET
							else
							{
								// renderiza o modal
								$this->load->view('criar');
							}
						}
					} else {
						echo 'essa requisição precisa vir por ajax';
					}
				break;

				case 'editar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('nivel de acesso insuficiente');
						} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras
							$this->form_validation->set_rules('curso_titulo', 'Título', 'required');
							$this->form_validation->set_rules('fok_usua_coordenador', 'Coordenador', 'required|is_natural_no_zero');
							$this->form_validation->set_rules('curso_data_inicio', 'Data de Início', 'required');
							$this->form_validation->set_rules('curso_data_fim', 'Data de Fim', 'required');

							// a validação de data é mais coisada
							$this->load->helper('data');

							if ($data_inicio_fixed = valid_date_str($post['curso_data_inicio'])){
								// valida data e conserta para IE antigo e Firefox
								$post['curso_data_inicio'] = $data_inicio_fixed;
								} else {
								$post['curso_data_inicio'] = null;
								$_POST['curso_data_inicio'] = null; // << por causa do required
							}

							if ($data_fim_fixed = valid_date_str($post['curso_data_fim'])){
								// valida data e conserta para IE antigo e Firefox
								$post['curso_data_fim'] = $data_fim_fixed;
								} else {
								$post['curso_data_fim'] = null;
								$_POST['curso_data_fim'] = null; // << por causa do required
							}

							// testa o formulário com a validação informada
							$sucesso = $this->form_validation->run();

							if ($sucesso) {
								$this->cursos->editar($post);
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
								} else {
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else {
							$table = $this->cursos->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('editar', $table[0]);
						}
					}
					} else {
					echo 'essa requisição precisa vir por ajax';
				}
				break;

				case 'deletar':
					// ajax
					if ($this->input->is_ajax_request()) {
						// tipo de retorno: JSON
						header('Content-Type: application/json');

						// SEGURANÇA - Nível requerido
						if($this->session->userdata['usuatipo_nivel_acesso'] > 2){
							echo json_encode('nivel de acesso insuficiente');
						} else {

							// pega todo o post já limpo de html/css/js (anti trolls)
							$post = $this->input->post(NULL, TRUE);

							// se foi um POST
							if ($post) {
								// Deleta o curso
								$this->cursos->deletar($post['pmk_curso']);
								// Deleta a ligação com os secretarios deste curso
								$this->cursos_secretarios->deletar_multiplos($post['pmk_curso']);
								// Deleta a conta daquele curso
								$this->cursos_contas->deletar_por_curso($post['pmk_curso']);
								// Deleta o plano pedagogico daquele curso
								$this->planos_pedagogicos->deletar_por_curso($post['pmk_curso']);

								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							}
							// se foi um GET
							else
							{
								$table = $this->cursos->carregar($this->input->get('id'));
								// renderiza o modal
								$this->load->view('deletar', $table[0]);
							}
						}
					} else {
						echo 'essa requisição precisa vir por ajax';
					}
				break;

				case 'secretarios':

					if (isset($_POST['fields']['fok_curso'])) {

						$pmk_curso = $_POST['fields']['fok_curso'];

						if (isset($_POST['secretario'])) {
							$tableParams = $_POST['secretario'];

							if ($tableParams) {
								// Remove todos os secretarios prévios deste curso para então,
								// criar novos | Se deletar os anteriores, cria o novo

								$this->cursos_secretarios->deletar_multiplos($pmk_curso); // Pode ser que nao tenha nada para excluir
								foreach ($tableParams as $idSecretario => $secretario) {
									$cursoSecretario['fok_curso'] = $pmk_curso;
									$cursoSecretario['fok_usua_secretario'] = $secretario['pmk_usuario'];

									// Cria o curso_secretario de fato
									$idSecretario = $this->cursos_secretarios->criar($cursoSecretario);
								}

								if ($idSecretario) {
									redirect('cursos/secretarios/'.$pmk_curso, 'refresh');
									} else {
									redirect('cursos/secretarios/'.$pmk_curso, 'refresh');
								}
							}
						} else {
							// Veio vazio, apaga todos
							$this->cursos_secretarios->deletar_multiplos($pmk_curso); // Pode ser que nao tenha nada para excluir
							redirect('cursos/secretarios/'.$pmk_curso, 'refresh');
						}
					}
					//========================
					// css e js exclusivos para essa página
					$templateExtras = array(
					'styles' => array('select2.min.css', 'crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
					);

					$this->template->load('templates/template_painel', 'secretarios', $templateExtras);
					//========================

				break;


			}
		}
	}

