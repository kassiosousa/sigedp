<?php
/* Model that represents Cursos at database */

class ModelCursos extends CI_Model {

	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('cursos tabela');
		$this->db->where('tabela.pmk_curso', $idTable);
		$this->db->where('tabela.curso_is_ativo', 'Sim');

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function listar($tableParam = ''){

		$where = array('tabela.curso_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_curso'])){ $where += array('tabela.pmk_curso' => $tableParam['pmk_curso']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('cursos tabela');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function criar($tableParam) {
		if (isset($tableParam)){

            if (!empty($tableParam['polos'])) $polos = $tableParam['polos'];
            else $polos = array();

            unset($tableParam['polos']);

			$this->db->trans_start();
			$this->db->insert('cursos', $tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();

			$this->db->insert('cursos_contas', array(
				'fok_curso' => $insert_id,
				//'curso_conta_qtd_diarias' => 0,
                'curso_conta_qtd_diarias_inicial' => 0,
                'curso_conta_vr_diaria_inicial' => 0,
                'curso_conta_saldo_dinheiro_inicial' => 0,
                'curso_conta_vr_diaria' => 0,
				'curso_conta_saldo' => 0,
                'curso_conta_saldo_dinheiro' => 0
			));

            foreach ($polos as $polo) {
                $this->db->insert('cursos_polos', array(
                    'fok_curso' => $insert_id,
                    'fok_polo' => $polo
                ));
            }

			return $insert_id;
		} else {
			return false;
		}
	}

	public function editar ($tableParam) {

		if (isset($tableParam)){
			$idTable = $tableParam['pmk_curso'];

            if (!empty($tableParam['polos'])) $polos = $tableParam['polos'];
            else $polos = array();

            unset($tableParam['polos']);

			$this->db->where('pmk_curso', $idTable);
			$this->db->set($tableParam);
			$this->db->update('cursos');

            $this->db->where('fok_curso', $idTable);
            $this->db->delete('cursos_polos');

            foreach ($polos as $polo) {
                $this->db->insert('cursos_polos', array(
                    'fok_curso' => $idTable,
                    'fok_polo' => $polo
                ));
            }

            return true;
		} else {
			return false;
		}
	}

	public function deletar ($idTable) {
		$tableParam['curso_is_ativo'] = 'Nao';
		$tableParam['pmk_curso'] = $idTable;

		$this->db->where('pmk_curso', $idTable);
		$this->db->set($tableParam);
		$this->db->update('cursos');

        $this->db->where('fok_curso', $idTable);
        $this->db->set(array('curso_conta_is_ativo' => 'Nao'));
        $this->db->update('cursos_contas');

        $this->db->where('fok_curso', $idTable);
        $this->db->set(array('polo_curso_is_ativo' => 'Nao'));
        $this->db->update('cursos_polos');

        return true;
	}

    public function buscar_por_polo($fok_polo)
    {
        $this->db->select('*');
        $this->db->from('cursos');
        $this->db->join('cursos_polos', 'fok_curso = pmk_curso');
        $this->db->where('fok_polo',$fok_polo);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }
}
