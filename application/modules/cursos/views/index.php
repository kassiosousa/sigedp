<?php
	$tableList = $this->cursos->listar();
	$coordenadores = $this->usuarios->coordenadores_de_curso();

?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Cursos<small class='hidden-xs'><br>Lista de cursos cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>cursos/criar' data-header='Cadastrar Curso'>
				<i class='fa fa-plus'></i> &nbsp;Cadastrar Curso
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>cursos'><i class='fa fa-book'></i> Cursos</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th>Curso</th>
							<th>Coordenador / Secretarios</th>
							<th>Data de Início</th>
							<th>Data de Fim</th>
							<th class="text-center acoes">Ações</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) {

								// Carrega os secretarios deste curso
								$funcParam['pmk_curso'] = $lista['pmk_curso'];
								$secretariosCurso = $this->cursos_secretarios->listar($funcParam);
							?>
								<tr>
									<td>
										<?php echo $lista['curso_titulo']; ?>
										<small class="text-muted"><br><?php echo $lista['curso_descricao']; ?></small>
									</td>
									<td>
										<?php
										echo @$coordenadores[$lista['fok_usua_coordenador']];
										if ($secretariosCurso) {
											echo "<small class='text-muted'><br>";
											$contS = 0;
											foreach ($secretariosCurso as $secretario){
												if ($contS > 0){echo ", ";}
												echo $secretario['usua_nome'];
												$contS++;
											}
											echo '</small>';
										}
										?>
									</td>
									<td><?php echo date('d/m/Y',strtotime($lista['curso_data_inicio'])); ?></td>
									<td><?php echo date('d/m/Y',strtotime($lista['curso_data_fim'])); ?></td>
									<td class='text-center acoes'>
										<div class='btn-group'>
											<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>cursos/editar/?id=<?php echo $lista['pmk_curso'];?>' data-header='Editar Curso'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
											<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>cursos/deletar/?id=<?php echo $lista['pmk_curso'];?>' data-header='Confirmar Exclusão'><i class='fa fa-trash'></i> <span class='hidden-xs'>Excluir</span></button>
											<a class='btn btn-warning btn-sm modal-form' href='<?php echo base_url();?>cursos/secretarios/<?php echo $lista['pmk_curso'];?>' ><i class='fa fa-user'></i> <span class='hidden-xs'>Secretarios</span></a>
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th>Curso</th>
							<th>Coordenador</th>
							<th>Data de Início</th>
							<th>Data de Fim</th>
							<th class="text-center acoes">Ações</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

