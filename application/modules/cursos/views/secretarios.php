<?php 
	//Carrega o curso
	$idTable = $this->uri->segment(3);
	$cursoFields = $this->cursos->carregar($idTable);
	
	//Lista todos os secretarios existentes
	$funcParam['fok_usuariotipo'] = 5;
	$secretariosArray = $this->usuarios->listar($funcParam);
	
	// Carrega os secretarios deste curso
	$funcParam['fok_curso'] = $idTable;
	$secretariosCurso = $this->cursos_secretarios->listar($funcParam);
	
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Cursos<small class='hidden-xs'><br>Secretarios</small></h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>cursos'><i class='fa fa-book'></i> Cursos</a></li>
		</ol>
	</div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
					
				<h4>Secretarios do curso: <?php echo $cursoFields[0]['curso_titulo'];?></h4>
				
				<form method='post' action='<?php echo base_url(); ?>cursos/secretarios'>
					<input type='hidden' name='fields[fok_curso]' value='<?php echo $idTable; ?>' />
							
						<div class="row">
							
							<!-- Cabeçalho -->
							<div class="col-md-12" style="text-align:center;">
								<div class="col-xs-1">Secretarios</div>
								<div class="col-xs-1" style="text-align:center;">
									<button class="add_field_button btn btn-success">+</button>
								</div>
							</div>
							<div class="col-md-12">
								<!-- Área de receber novos campos via javascript -->
								<div class="contacts"><div class="input_fields_wrap"></div></div>
							</div>
						</div>
						<br>
						<div class='box-footer' style="float: left">
							<button type='submit' class='btn btn-primary pull-right'>Atualizar secretarios</button>
						</div>
				</form>

			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jQuery-2.2.0.min.js"></script>

<script>
	$(document).ready(function() {
		var max_fields      = 30; //Máximo de secretarios
		var wrapper         = $(".input_fields_wrap"); //Fields wrapper
		var add_button      = $(".add_field_button"); //Add button ID
		
		var x = 1; // initlal text box count
		$(add_button).click(function(e){ //on add input button click
			e.preventDefault();
			if(x < max_fields){ //max input box allowed
				x++; //text box increment
				var campos = '<span>';
				campos += '	<div class="col-xs-11">';
				campos += '		<select name="secretario['+x+'][pmk_usuario]" class="form-control" >';
				<?php 
					if ($secretariosArray) {
						foreach ($secretariosArray as $secretario) {
				?>
				campos += '<option value="<?php echo $secretario['pmk_usuario'];?>"><?php echo $secretario['usua_nome'];?></option>';
				<?php
						}
					}
				?>
				campos += '		</select>';
				campos += '	</div>';
				campos += '	<div class="col-xs-1" style="text-align:center;"><a href="#"><button class="add_field_button btn btn-danger remove_field">-</button></a></div></span>';
				$(wrapper).append(campos); //add input box
			}
		});
		
		// Carrega os secretarios já cadastrados
		<?php
			if ($secretariosCurso) {
				foreach ($secretariosCurso as $secretarioCurso) {
				?>
				if (x < max_fields){ //max input box allowed
				x++; //text box increment
				var campos = '<span>';
				campos += '	<div class="col-xs-11">';
				campos += '		<select name="secretario['+x+'][pmk_usuario]" class="form-control" >';
				
				<?php 
					if ($secretariosArray) {
						foreach ($secretariosArray as $secretario) {
				?>
				campos += '<option <?php if ($secretario['pmk_usuario'] == $secretarioCurso['fok_usua_secretario']) { echo "selected"; } ?> value="<?php echo $secretario['pmk_usuario'];?>"><?php echo $secretario['usua_nome'];?></option>';
				<?php
						}
					}
				?>
				campos += '		</select>';
				campos += '	</div>';
				campos += '	<div class="col-xs-1" style="text-align:center;"><a href="#"><button class="add_field_button btn btn-danger remove_field">-</button></a></span>';
				$(wrapper).append(campos); //add input box
			}
			<?php
				}
			}
		?>
	
		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			e.preventDefault(); 
			$(this).closest('span').remove(); x--;
		})
	
	});
</script>
