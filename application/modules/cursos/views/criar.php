<!-- Modal de Cadastrar Curso -->
<?php
	$coordenadores = $this->usuarios->coordenadores_de_curso();
    $polos = $this->polos->todos();
?>
<form class='form-horizontal'>
	<div class='form-group'>
		<label for='curso_titulo' class='col-sm-4 control-label'>Nome:</label>
		<div class='col-sm-8'>
			<input type='text' class='form-control' id='curso_titulo' name='curso_titulo' placeholder='Nome do Curso' required>
		</div>
	</div>
	<div class='form-group'>
		<label for='curso_descricao' class='col-sm-4 control-label'>Descrição:</label>
		<div class='col-sm-8'>
			<input type='text' class='form-control' id='curso_descricao' name='curso_descricao' placeholder='Uma breve descrição desse curso'>
		</div>
	</div>
	<div class="form-group">
		<label for="fok_usua_coordenador" class="col-sm-4 control-label">Coordenador:</label>
		<div class="col-sm-8">
			<select name="fok_usua_coordenador" id="fok_usua_coordenador" class="form-control select-search" style="width:100%;" required>
				<?php foreach ($coordenadores as $key => $coordenador): ?>
				<option value="<?php echo $key; ?>"><?php echo $coordenador; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>
    <div class="form-group">
        <label for="polos" class="col-sm-4 control-label">Polos onde esse curso é oferecido:</label>
        <div class="col-sm-8">
            <select name="polos[]" id="polos" class="form-control select-search" style="width:100%;" multiple required>
                <?php foreach ($polos as $key => $polo): ?>
                <option value="<?php echo $key; ?>"><?php echo $polo; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
	<div class='form-group'>
		<label for='curso_data_inicio' class='col-sm-4 control-label'>Data de Início:</label>
		<div class='col-sm-8'>
			<input type='date' class='form-control input-date' id='curso_data_inicio' name='curso_data_inicio' required>
		</div>
	</div>
	<div class='form-group'>
		<label for='curso_data_fim' class='col-sm-4 control-label'>Data de Fim:</label>
		<div class='col-sm-8'>
			<input type='date' class='form-control input-date' id='curso_data_fim' name='curso_data_fim' required>
		</div>
	</div>
</form>

