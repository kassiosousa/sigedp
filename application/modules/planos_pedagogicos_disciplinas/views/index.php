
<?php
	$tableList = $this->planos_pedagogicos_disciplinas->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Planos_pedagogicos_disciplinas<small class='hidden-xs'><br>Lista de Planos_pedagogicos_disciplinas cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>planos_pedagogicos_disciplinas/criar' data-header='Cadastrar Planos_pedagogicos_disciplinas'>
				<i class='fa fa-plus'></i> &nbsp;Criar Planos_pedagogicos_disciplinas
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>planos_pedagogicos_disciplinas'><i class='fa fa-users'></i> Planos_pedagogicos_disciplinas</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th><i class='icon_profile'></i> Pmk_plano_disciplina</th>
							<th><i class='icon_profile'></i> Fok_plano_semestre</th>
							<th><i class='icon_profile'></i> Plano_disciplina_titulo</th>
							<th><i class='icon_profile'></i> Plano_disciplina_horas_teoricas</th>
							<th><i class='icon_profile'></i> Plano_disciplina_horas_praticas</th>
							<th><i class='icon_profile'></i> Plano_disciplina_qtd_diarias</th>
							<th><i class='icon_profile'></i> Plano_disciplina_is_ativo</th>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_plano_disciplina']; ?></td>
									<td><?php echo $lista['fok_plano_semestre']; ?></td>
									<td><?php echo $lista['plano_disciplina_titulo']; ?></td>
									<td><?php echo $lista['plano_disciplina_horas_teoricas']; ?></td>
									<td><?php echo $lista['plano_disciplina_horas_praticas']; ?></td>
									<td><?php echo $lista['plano_disciplina_qtd_diarias']; ?></td>
									<td><?php echo $lista['plano_disciplina_is_ativo']; ?></td>
									<td class='text-center acoes'>
										<div class='btn-group'>
											<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>planos_pedagogicos_disciplinas/editar/?id=<?php echo $lista['pmk_plano_disciplina'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
											<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>planos_pedagogicos_disciplinas/deletar/?id=<?php echo $lista['pmk_plano_disciplina'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Pmk_plano_disciplina</th>
							<th><i class='icon_profile'></i> Fok_plano_semestre</th>
							<th><i class='icon_profile'></i> Plano_disciplina_titulo</th>
							<th><i class='icon_profile'></i> Plano_disciplina_horas_teoricas</th>
							<th><i class='icon_profile'></i> Plano_disciplina_horas_praticas</th>
							<th><i class='icon_profile'></i> Plano_disciplina_qtd_diarias</th>
							<th><i class='icon_profile'></i> Plano_disciplina_is_ativo</th>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

