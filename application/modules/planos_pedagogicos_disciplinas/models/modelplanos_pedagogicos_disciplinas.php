<?php
/* Model that represents Planos_pedagogicos_disciplinas at database */

class ModelPlanos_pedagogicos_disciplinas extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('planos_pedagogicos_disciplinas tabela');
		$this->db->where('tabela.pmk_plano_disciplina', $idTable);
		$this->db->where('tabela.plano_pedagogico_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function carregar_por_plano_pedagogico ($idTable){
		$this->db->select('*');
		$this->db->from('planos_pedagogicos_disciplinas tabela');
		$this->db->join('planos_pedagogicos plano', 'plano.pmk_plano_pedagogico = tabela.fok_plano_pedagogico');
		$this->db->where('tabela.pmk_plano_disciplina', $idTable);
		$this->db->where('tabela.plano_pedagogico_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	
	public function listar($tableParam = ''){

		$where = array('tabela.plano_disciplina_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_plano_disciplina'])){ $where += array('tabela.pmk_plano_disciplina' => $tableParam['pmk_plano_disciplina']); }
		if(isset($tableParam['fok_plano_pedagogico'])){ $where += array('tabela.fok_plano_pedagogico' => $tableParam['fok_plano_pedagogico']); }
		if(isset($tableParam['fok_curso'])){ $where += array('plano.fok_curso' => $tableParam['fok_curso']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('planos_pedagogicos_disciplinas tabela');
		$this->db->join('planos_pedagogicos plano', 'plano.pmk_plano_pedagogico = tabela.fok_plano_pedagogico');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$this->db->order_by("plano_nr_semestre", "asc");
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('planos_pedagogicos_disciplinas',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_plano_disciplina'];
			
			$this->db->where('pmk_plano_disciplina', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('planos_pedagogicos_disciplinas');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['plano_pedagogico_is_ativo'] = 'Nao';
		$tableParam['pmk_plano_disciplina'] = $idTable;
		
		$this->db->where('pmk_plano_disciplina', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('planos_pedagogicos_disciplinas');
	}
	
	public function deletar_por_plano_pedagogico ($idTable) {
		$tableParam['plano_disciplina_is_ativo'] = 'Nao';
		$tableParam['fok_plano_pedagogico'] = $idTable;
		
		$this->db->where('fok_plano_pedagogico', $idTable);
		$this->db->delete('planos_pedagogicos_disciplinas');
		if ($this->db->affected_rows()){
			return true;
		} else { return false; }
	}
	
}
