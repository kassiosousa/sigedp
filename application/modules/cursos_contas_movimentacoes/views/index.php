﻿<?php
	$pmk_curso_conta = $this->uri->segment(2);
	$tableParam['fok_curso_conta'] = $pmk_curso_conta;
	
	// Carrega o Curso
	$cursoArray = $this->cursos->carregar($pmk_curso_conta);
	
	$tableList = $this->cursos_contas_movimentacoes->listar($tableParam);
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Curso Conta Movimentacoes<small class='hidden-xs'><br>Curso: <?php echo $cursoArray[0]['curso_titulo']; ?></small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>cursos_contas_movimentacoes/criar/?fok_curso_conta=<?php echo $pmk_curso_conta;?>' data-header='Criar Movimentação'>
				<i class='fa fa-plus'></i> &nbsp;Criar Movimentacão
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>cursos_contas_movimentacoes'><i class='fa fa-users'></i> Movimentacões</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th style="width: 40px"><i class='icon_profile'></i> Cod.</th>
							<th><i class='icon_profile'></i> Curso</th>
							<th><i class='icon_profile'></i> Criador</th>
							<th style="width: 150px"><i class='icon_profile'></i> Data Criação</th>
							<th style="width: 120px"><i class='icon_profile'></i> Qtd. Diárias</th>
							<th style="width: 80px"><i class='icon_profile'></i> Tipo</th>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_curso_conta_movimentacao']; ?></td>
									<td><?php echo $lista['curso_titulo']; ?></td>
									<td><?php echo $lista['usua_nome']; ?></td>
									<td><?php echo date_format(new DateTime($lista['movcontacurso_dt_criacao']), 'd/m/Y H:i:s'); ?></td>
									<td <?php if ($lista['movcontacurso_tipo']=="Adicao") { echo "class='success'"; } else { echo "class='danger'"; } ?> >
										<?php echo $lista['movcontacurso_valor']; ?>
									</td>
									<td><?php echo $lista['movcontacurso_tipo']; ?></td>
									<td class='text-center acoes' >
										<div class='btn-group'>
											<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>cursos_contas_movimentacoes/editar/?id=<?php echo $lista['pmk_curso_conta_movimentacao'];?>&fok_curso_conta=<?php echo $pmk_curso_conta;?>' data-header='Editar Movimentacão'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
											<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>cursos_contas_movimentacoes/deletar/?id=<?php echo $lista['pmk_curso_conta_movimentacao'];?>&fok_curso_conta=<?php echo $pmk_curso_conta;?>' data-header='Confirmar Exclusão'><i class='fa fa-trash'></i> <span class='hidden-xs'>Excluir</span></button>
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Cod.</th>
							<th><i class='icon_profile'></i> Curso</th>
							<th><i class='icon_profile'></i> Criador</th>
							<th><i class='icon_profile'></i> Criação</th>
							<th><i class='icon_profile'></i> Valor</th>
							<th><i class='icon_profile'></i> Tipo</th>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

