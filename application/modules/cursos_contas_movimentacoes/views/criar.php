﻿<!-- Modal de Criar -->

<form class='form-horizontal'>
	<input type='hidden' name='fok_curso_conta' value='<?php echo $pmk_curso_conta; ?>'>
	
	<div class='form-group'>
		<label for='movcontacurso_tipo' class='col-sm-4 control-label'>Tipo:</label>
		<div class='col-sm-8'>
			<select name="movcontacurso_tipo" class="form-control">
				<option value="Adicao">Adição</option>
				<option value="Retirada">Retirada</option>
			</select>
		</div>
	</div>
	<div class='form-group'>
		<label for='movcontacurso_valor_dinheiro' class='col-sm-4 control-label'>Valor:</label>
		<div class='col-sm-8'>
			<input type='number' class='form-control' id='movcontacurso_valor_dinheiro' name='movcontacurso_valor_dinheiro' placeholder='1' value="1" required>
		</div>
	</div>
	<div class='form-group'>
		<label for='curso_conta_vr_diaria' class='col-sm-4 control-label'>Valor Diaria</label>
		<div class='col-sm-8'>
			<input type='number' readonly class='form-control' id='curso_conta_vr_diaria'  value='<?php echo $curso_conta_vr_diaria; ?>' placeholder='1' required>
		</div>
	</div>
	<div class='form-group'>
		<label for='movcontacurso_valor' class='col-sm-4 control-label'>Diárias:</label>
		<div class='col-sm-8'>
			<input type='number' readonly class='form-control' id='movcontacurso_valor' name='movcontacurso_valor' placeholder='1' required>
		</div>
	</div>
	
</form>

<script>
	$(document).on("change", "input[id^='movcontacurso_valor_dinheiro']", function(ev) {
		var saldoDinheiro = $("#movcontacurso_valor_dinheiro").val();
		var valorDiaria = $("#curso_conta_vr_diaria").val();
		var result = Math.floor(saldoDinheiro/valorDiaria);
		$("#movcontacurso_valor").val(result);
	});
	$(document).on("change", "input[id^='curso_conta_vr_diaria']", function(ev) {
		var saldoDinheiro = $("#movcontacurso_valor_dinheiro").val();
		var valorDiaria = $("#curso_conta_vr_diaria").val();
		var result = Math.floor(saldoDinheiro/valorDiaria);
		$("#movcontacurso_valor").val(result);
	});
	
</script>

