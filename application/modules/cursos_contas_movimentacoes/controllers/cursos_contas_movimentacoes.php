<?php
/* CRUD Controller for Cursos_contas_movimentacoes */

class Cursos_contas_movimentacoes extends CI_Controller {
	
	function _remap($method){ 
		//Carrega os modelos a serem utilizados
		$this->load->model('Cursos_contas_movimentacoes/modelcursos_contas_movimentacoes', 'cursos_contas_movimentacoes');
		$this->load->model('cursos_contas/modelcursos_contas', 'cursos_contas');
		$this->load->model('cursos/modelcursos', 'cursos');
		
		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }
		
		// chama a função de acordo com o método (method)
		switch( $method ) {
		
			default:
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'crud.js')
				);
				$this->template->load('templates/template_painel', 'index', $templateExtras);
			break;
		
			case 'criar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('Nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post) {
						
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras do formulário (Deve ser adaptado para cada módulo)
							$this->form_validation->set_rules('movcontacurso_tipo', 'Tipo', 'required');
							$this->form_validation->set_rules('movcontacurso_valor', 'Valor', 'required|is_natural_no_zero');
							
							// testa o formulário com a validação informada
							$sucesso = $this->form_validation->run();
							
							if ($sucesso) {
							
								// Cria a movimentação
								$post['fok_usuario_criador'] = $this->session->userdata['pmk_usuario'];
								$this->cursos_contas_movimentacoes->criar($post);
								
								// Calcula se é negativa a movimentação
								// Carrega a conta_curso | Edita o saldo | Atualiza a conta_curso
								if ($post['movcontacurso_tipo'] == "Retirada"){
									$post['movcontacurso_valor'] = $post['movcontacurso_valor']*(-1);
								}
								$EditcursoConta = array();
								$cursoConta = $this->cursos_contas->carregar($post['fok_curso_conta']);
								
								$EditcursoConta['pmk_curso_conta'] = $cursoConta[0]['pmk_curso_conta'];
								$EditcursoConta['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo'] + $post['movcontacurso_valor'];
								$EditcursoConta['curso_conta_saldo_dinheiro'] = $cursoConta[0]['curso_conta_saldo_dinheiro'] + $post['movcontacurso_valor_dinheiro'];

								$sucesso = $this->cursos_contas->editar($EditcursoConta);
								
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							} else {
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else
						{
							$table = $this->cursos_contas->carregar($this->input->get('fok_curso_conta'));
							// renderiza o modal se achar o conta_curso
							if ($table) {
								$this->load->view('criar', $table[0]);
							} else {
							
							}
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;
			
			case 'editar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2) {
						echo json_encode('nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post)
						{
							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções

							// regras do formulário (Deve ser adaptado para cada módulo)
							$this->form_validation->set_rules('movcontacurso_tipo', 'Tipo', 'required');
							$this->form_validation->set_rules('movcontacurso_valor', 'Valor', 'required|is_natural_no_zero');
							
							// testa o formulário com a validação
							$sucesso = $this->form_validation->run();
							if ($sucesso) {
								// Verifica se o valor ou o tipo foi alterado para atualizar o saldo
								$boolMudaTipo = 0;
								$boolMudaValor = 0;
								$EditcursoConta = array();
								
								// Carrega a conta_curso | Edita o saldo | Atualiza a conta_curso
								$cursoConta = $this->cursos_contas->carregar($this->input->get('fok_curso_conta'));
								$EditcursoConta['pmk_curso_conta'] = $cursoConta[0]['pmk_curso_conta'];
								// Carrega a movimentação
								$movimentacao = $this->cursos_contas_movimentacoes->carregar($this->input->get('id'));
								
								if ($movimentacao[0]['movcontacurso_tipo'] != $post['movcontacurso_tipo']){$boolMudaTipo=1;}
								if ($movimentacao[0]['movcontacurso_valor'] != $post['movcontacurso_valor']){$boolMudaValor=1;}
								
								// Se modou o tipo, testa para qual mudou
								if ($boolMudaTipo){
									if ($post['movcontacurso_tipo'] == "Adicao"){
										// o saldo deve ser adicionado 2 vezes (O que tinha e o novo valor)
										$EditcursoConta['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo'] + 2*($post['movcontacurso_valor']);
									}
									if ($post['movcontacurso_tipo'] == "Retirada"){
										// o saldo deve ser retirado 2 vezes (O que tinha e o novo valor)
										$EditcursoConta['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo'] + -2*($post['movcontacurso_valor']);
									}
								}
								// Se modou o valor, retira ou adiciona a diferença
								if ($boolMudaValor){
									$EditcursoConta['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo'] + ($movimentacao[0]['movcontacurso_valor']-$post['movcontacurso_valor']);
								}
								// Edita a movimentação
								$this->cursos_contas_movimentacoes->editar($post);
								
								
								// Atualiza o saldo de dinheiro
								$EditcursoConta['curso_conta_saldo_dinheiro'] = $cursoConta[0]['curso_conta_saldo_dinheiro'] + $post['movcontacurso_valor_dinheiro'];

								$sucesso = $this->cursos_contas->editar($EditcursoConta);
							
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							} else {
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else {
							$table = $this->cursos_contas_movimentacoes->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('editar', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;
			
			case 'deletar':
				// ajax
				if ($this->input->is_ajax_request()) {
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 2){
						echo json_encode('nivel de acesso insuficiente');
					} else {
					
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post) {
							// Verifica se o valor ou o tipo foi alterado para atualizar o saldo
							$EditcursoConta = array();
							
							// Carrega a conta_curso | Atualiza o saldo |
							$cursoConta = $this->cursos_contas->carregar($this->input->get('fok_curso_conta'));
							$EditcursoConta['pmk_curso_conta'] = $cursoConta[0]['pmk_curso_conta'];
							// Carrega a movimentação
							$movimentacao = $this->cursos_contas_movimentacoes->carregar($post['pmk_curso_conta_movimentacao']);
							
							// Se deleta, testa qual era o tipo
							if ($movimentacao){
								if ($movimentacao[0]['movcontacurso_tipo'] == "Adicao"){
									// o saldo deve ser retirado, o valor da movimentacao
									$EditcursoConta['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo'] - $movimentacao[0]['movcontacurso_valor'];
								}
								if ($movimentacao[0]['movcontacurso_tipo'] == "Retirada"){
									// o saldo deve ser adicionado, o valor da movimentacao
									$EditcursoConta['curso_conta_saldo'] = $cursoConta[0]['curso_conta_saldo'] + $movimentacao[0]['movcontacurso_valor'];
								}
							}
							// Atualiza o saldo
							$EditcursoConta['curso_conta_saldo_dinheiro'] = $cursoConta[0]['curso_conta_saldo_dinheiro'] - $movimentacao[0]['movcontacurso_valor_dinheiro'];
							$this->cursos_contas->editar($EditcursoConta);
						
							// Deleta a movimentação
							$this->cursos_contas_movimentacoes->deletar($post['pmk_curso_conta_movimentacao']);
							echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
						}
						// se foi um GET
						else
						{
							$table = $this->cursos_contas_movimentacoes->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('deletar', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;
		
			
		}
	}
}

