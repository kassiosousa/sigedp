<?php
/* Model that represents Cursos_contas_movimentacoes at database */

class ModelCursos_contas_movimentacoes extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('cursos_contas_movimentacoes tabela');
		$this->db->join('cursos_contas conta', 'tabela.fok_curso_conta = conta.pmk_curso_conta');
		$this->db->join('cursos curso', 'conta.fok_curso = curso.pmk_curso');
		$this->db->join('usuarios usua', 'usua.pmk_usuario = tabela.fok_usuario_criador');
		$this->db->where('tabela.pmk_curso_conta_movimentacao', $idTable);
		$this->db->where('tabela.movcontacurso_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar($tableParam = ''){

		$where = array('tabela.movcontacurso_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_curso_conta_movimentacao'])){ $where += array('tabela.pmk_curso_conta_movimentacao' => $tableParam['pmk_curso_conta_movimentacao']); }
		if(isset($tableParam['fok_curso_conta'])){ $where += array('tabela.fok_curso_conta' => $tableParam['fok_curso_conta']); }
		if(isset($tableParam['fok_curso'])){ $where += array('conta.fok_curso' => $tableParam['fok_curso']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('cursos_contas_movimentacoes tabela');
		$this->db->join('cursos_contas conta', 'tabela.fok_curso_conta = conta.pmk_curso_conta');
		$this->db->join('cursos curso', 'conta.fok_curso = curso.pmk_curso');
		$this->db->join('usuarios usua', 'usua.pmk_usuario = tabela.fok_usuario_criador');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('cursos_contas_movimentacoes',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_curso_conta_movimentacao'];
			
			$this->db->where('pmk_curso_conta_movimentacao', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('cursos_contas_movimentacoes');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['movcontacurso_is_ativo'] = 'Nao';
		$tableParam['pmk_curso_conta_movimentacao'] = $idTable;
		
		$this->db->where('pmk_curso_conta_movimentacao', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('cursos_contas_movimentacoes');
	}
}
