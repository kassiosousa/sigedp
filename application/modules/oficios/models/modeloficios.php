<?php
/* Model that represents Oficios at database */

class ModelOficios extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('oficios tabela');
		$this->db->where('tabela.pmk_oficio', $idTable);
		$this->db->where('tabela.oficio_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar($tableParam = ''){

		$where = array('tabela.oficio_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_oficio'])){ $where += array('tabela.pmk_oficio' => $tableParam['pmk_oficio']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('oficios tabela');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('oficios',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_oficio'];
			
			$this->db->where('pmk_oficio', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('oficios');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['oficio_is_ativo'] = 'Nao';
		$tableParam['pmk_oficio'] = $idTable;
		
		$this->db->where('pmk_oficio', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('oficios');
	}
}
