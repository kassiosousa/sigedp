
<?php
	$tableList = $this->oficios->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Oficios<small class='hidden-xs'><br>Lista de Oficios cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>oficios/criar' data-header='Cadastrar Oficios'>
				<i class='fa fa-plus'></i> &nbsp;Criar Oficios
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>oficios'><i class='fa fa-users'></i> Oficios</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th><i class='icon_profile'></i> Pmk_oficio</th>
<th><i class='icon_profile'></i> Fok_pedido</th>
<th><i class='icon_profile'></i> Oficio_status</th>
<th><i class='icon_profile'></i> Oficio_diarias_dia</th>
<th><i class='icon_profile'></i> Oficio_diarias_cod</th>
<th><i class='icon_profile'></i> Oficio_passagens_dia</th>
<th><i class='icon_profile'></i> Oficio_passagens_cod</th>
<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_oficio']; ?></td>
<td><?php echo $lista['fok_pedido']; ?></td>
<td><?php echo $lista['oficio_status']; ?></td>
<td><?php echo $lista['oficio_diarias_dia']; ?></td>
<td><?php echo $lista['oficio_diarias_cod']; ?></td>
<td><?php echo $lista['oficio_passagens_dia']; ?></td>
<td><?php echo $lista['oficio_passagens_cod']; ?></td>
<td class='text-center acoes'>
					<div class='btn-group'>
						<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>oficios/editar/?id=<?php echo $lista['pmk_oficio'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
						<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>oficios/deletar/?id=<?php echo $lista['pmk_oficio'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
					</div>
				</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Pmk_oficio</th>
<th><i class='icon_profile'></i> Fok_pedido</th>
<th><i class='icon_profile'></i> Oficio_status</th>
<th><i class='icon_profile'></i> Oficio_diarias_dia</th>
<th><i class='icon_profile'></i> Oficio_diarias_cod</th>
<th><i class='icon_profile'></i> Oficio_passagens_dia</th>
<th><i class='icon_profile'></i> Oficio_passagens_cod</th>
<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

