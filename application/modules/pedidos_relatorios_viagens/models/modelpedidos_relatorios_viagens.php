<?php
/* Model that represents Pedidos_relatorios_viagens at database */

class ModelPedidos_relatorios_viagens extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('pedidos_relatorios_viagens tabela');
		$this->db->where('tabela.pmk_pedido_relatorio', $idTable);
		$this->db->where('tabela.pedi_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar($tableParam = ''){

		$where = array('tabela.pedido_rel_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_pedido_relatorio'])){ $where += array('tabela.pmk_pedido_relatorio' => $tableParam['pmk_pedido_relatorio']); }
		if(isset($tableParam['fok_pedido'])){ $where += array('tabela.fok_pedido' => $tableParam['fok_pedido']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('pedidos_relatorios_viagens tabela');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('pedidos_relatorios_viagens',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_pedido_relatorio'];
			
			$this->db->where('pmk_pedido_relatorio', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('pedidos_relatorios_viagens');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['pedi_is_ativo'] = 'Nao';
		$tableParam['pmk_pedido_relatorio'] = $idTable;
		
		$this->db->where('pmk_pedido_relatorio', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('pedidos_relatorios_viagens');
	}
}
