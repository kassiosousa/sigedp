
<?php
	$tableList = $this->pedidos_relatorios_viagens->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Pedidos_relatorios_viagens<small class='hidden-xs'><br>Lista de Pedidos_relatorios_viagens cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>pedidos_relatorios_viagens/criar' data-header='Cadastrar Pedidos_relatorios_viagens'>
				<i class='fa fa-plus'></i> &nbsp;Criar Pedidos_relatorios_viagens
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>pedidos_relatorios_viagens'><i class='fa fa-users'></i> Pedidos_relatorios_viagens</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th><i class='icon_profile'></i> Pmk_pedido_relatorio</th>
<th><i class='icon_profile'></i> Pedido_rel_data</th>
<th><i class='icon_profile'></i> Pedido_rel_texto</th>
<th><i class='icon_profile'></i> Pedido_rel_anexo</th>
<th><i class='icon_profile'></i> Pedido_rel_is_ativo</th>
<th><i class='icon_profile'></i> Pedi_viagem_ida_dia</th>
<th><i class='icon_profile'></i> Pedi_justificativa</th>
<th><i class='icon_profile'></i> Pedi_viagem_volta_dia</th>
<th><i class='icon_profile'></i> Pedi_viagem_volta_valor_previsto</th>
<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_pedido_relatorio']; ?></td>
<td><?php echo $lista['pedido_rel_data']; ?></td>
<td><?php echo $lista['pedido_rel_texto']; ?></td>
<td><?php echo $lista['pedido_rel_anexo']; ?></td>
<td><?php echo $lista['pedido_rel_is_ativo']; ?></td>
<td><?php echo $lista['pedi_viagem_ida_dia']; ?></td>
<td><?php echo $lista['pedi_justificativa']; ?></td>
<td><?php echo $lista['pedi_viagem_volta_dia']; ?></td>
<td><?php echo $lista['pedi_viagem_volta_valor_previsto']; ?></td>
<td class='text-center acoes'>
					<div class='btn-group'>
						<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>pedidos_relatorios_viagens/editar/?id=<?php echo $lista['pmk_pedido_relatorio'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
						<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>pedidos_relatorios_viagens/deletar/?id=<?php echo $lista['pmk_pedido_relatorio'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
					</div>
				</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Pmk_pedido_relatorio</th>
<th><i class='icon_profile'></i> Pedido_rel_data</th>
<th><i class='icon_profile'></i> Pedido_rel_texto</th>
<th><i class='icon_profile'></i> Pedido_rel_anexo</th>
<th><i class='icon_profile'></i> Pedido_rel_is_ativo</th>
<th><i class='icon_profile'></i> Pedi_viagem_ida_dia</th>
<th><i class='icon_profile'></i> Pedi_justificativa</th>
<th><i class='icon_profile'></i> Pedi_viagem_volta_dia</th>
<th><i class='icon_profile'></i> Pedi_viagem_volta_valor_previsto</th>
<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

