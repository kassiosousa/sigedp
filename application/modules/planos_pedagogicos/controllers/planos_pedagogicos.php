<?php
/* CRUD Controller for Planos_pedagogicos */

class Planos_pedagogicos extends CI_Controller {
	
	function _remap($method){ 
		//Carrega os modelos a serem utilizados
		$this->load->model('Planos_pedagogicos/modelplanos_pedagogicos', 'planos_pedagogicos');
		$this->load->model('Configuracoes/modelconfiguracoes', 'configuracoes');
		$this->load->model('Planos_pedagogicos_disciplinas/modelplanos_pedagogicos_disciplinas', 'planos_pedagogicos_disciplinas');
		
		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }
		
		// chama a função de acordo com o método (method)
		switch( $method ) {
		
			default:
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'index', $templateExtras);
			break;
		
			case 'disciplinas':
				
				if (isset($_POST['fields']['pmk_plano_pedagogico'])) {
					
					$pmk_plano_pedagogico = $_POST['fields']['pmk_plano_pedagogico'];
					
					if (isset($_POST['disciplina'])) {
						$tableParams = $_POST['disciplina'];
						
						if ($tableParams) {
							// Remove todas os disciplinas prévias deste plano_pedagogico para então, 
							// criar novos | Se deletar os anteriores, cria o novo
							
							// Pode ser que nao tenha nada para excluir
							$this->planos_pedagogicos_disciplinas->deletar_por_plano_pedagogico($pmk_plano_pedagogico); 
							// Atualiza: Plano Pedagógico - Disciplinas
							
							$maiorNrSemestre = 0;
							foreach ($tableParams as $idRow => $disciplina) {
								$planoDisciplina['fok_plano_pedagogico'] = $pmk_plano_pedagogico;
								$planoDisciplina['plano_nr_semestre'] = $disciplina['plano_nr_semestre'];
								$planoDisciplina['plano_disciplina_titulo'] = $disciplina['plano_disciplina_titulo'];
								$planoDisciplina['plano_disciplina_horas_teoricas'] = $disciplina['plano_disciplina_horas_teoricas'];
								$planoDisciplina['plano_disciplina_horas_praticas'] = $disciplina['plano_disciplina_horas_praticas'];
								
								$planoDisciplina['plano_disciplina_qtd_encontros'] = $disciplina['encontros'];
								$planoDisciplina['plano_disciplina_qtd_viagens'] = $disciplina['viagens'];
								$planoDisciplina['plano_disciplina_qtd_diarias'] = $disciplina['diarias'];
								
								// Calcula o maior nr de semestre
								if ($maiorNrSemestre < $disciplina['plano_nr_semestre']) {
									$maiorNrSemestre = $disciplina['plano_nr_semestre'];
								}
								
								// Cria o curso_semestre de fato
								$idPlanoDisciplina = $this->planos_pedagogicos_disciplinas->criar($planoDisciplina);
							}
							
							// Atualiza o plano pedagógico em si
							$planoParam['pmk_plano_pedagogico'] = $pmk_plano_pedagogico;
							$planoParam['plano_pedagogico_total_encontros'] = $_POST['plano_pedagogico']['qtdEncontros'];
							$planoParam['plano_pedagogico_total_viagens'] = $_POST['plano_pedagogico']['qtdViagens'];
							$planoParam['plano_pedagogico_total_diarias'] = $_POST['plano_pedagogico']['qtdDiarias'];
							$planoParam['plano_pedagogico_qtd_semestres'] = $maiorNrSemestre;
							$this->planos_pedagogicos->editar($planoParam);
							
							if ($idPlanoDisciplina) {
								redirect('planos_pedagogicos/disciplinas/'.$pmk_plano_pedagogico, 'refresh');
								} else {
								redirect('planos_pedagogicos/disciplinas/'.$pmk_plano_pedagogico, 'refresh');
							}
						}
					} else { 
						// Veio vazio, apaga todos
						$this->planos_pedagogicos_disciplinas->deletar_por_plano_pedagogico($pmk_plano_pedagogico); // Pode ser que nao tenha nada para excluir
						redirect('planos_pedagogicos/disciplinas/'.$pmk_plano_pedagogico, 'refresh'); 
					}
				}
				//========================
				// css e js exclusivos para essa página
				$templateExtras = array(
				'styles' => array('select2.min.css', 'crud.css'),
				'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
				);
				
				$this->template->load('templates/template_painel', 'disciplinas', $templateExtras);
				//========================
				
			break;
			
		}
	}
}

