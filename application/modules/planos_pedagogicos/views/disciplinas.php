<?php
	//Carrega o curso
	$idTable = $this->uri->segment(3);
	$planoFields = $this->planos_pedagogicos->carregar($idTable);

	//Carrega as configurações
	$configHorasTeoricas = $this->configuracoes->carregar(1);
	$configHorasPraticas = $this->configuracoes->carregar(2);
	
	// Carrega os semestres deste curso
	$discParam['fok_plano_pedagogico'] = $idTable;
	$disciplinasPlano = $this->planos_pedagogicos_disciplinas->listar($discParam);

?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Plano Pedagogico<small class='hidden-xs'><br>Semestres</small></h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>planos_pedagogicos'><i class='fa fa-users'></i> Planos_Pedagogicos</a></li>
			<li class='active'>Semestres / Disciplinas</li>
		</ol>
	</div>
</div>

<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				
				<h4>Semestres do plano pedagógico: <?php echo $planoFields[0]['curso_titulo'];?></h4>
				
				<form method='post' action='<?php echo base_url(); ?>planos_pedagogicos/disciplinas'>
					<input type='hidden' name='fields[pmk_plano_pedagogico]' value='<?php echo $idTable; ?>' />
					
					<div class="row">
						
						<!-- Cabeçalho -->
						<div class="col-md-12" style="text-align:center;">
							<div class="col-xs-11">Semestres / Disciplinas</div>
							<div class="col-xs-1" style="text-align:center;">
								<button class="add_field_button btn btn-success">+</button>
							</div>
						</div>
						<div class="col-md-12" style="text-align:center;">
							<div class="col-xs-1">Semestre</div>
							<div class="col-xs-5">Disciplina</div>
							<div class="col-xs-1">Horas Teóricas</div>
							<div class="col-xs-1">Horas Práticas</div>
							<div class="col-xs-1">Encontros</div>
							<div class="col-xs-1">Viagens</div>
							<div class="col-xs-1">Diárias</div>
						</div>
						<div class="col-md-12">
							<!-- área de receber novos campos via javascript -->
							<div class="contacts"><div class="input_fields_wrap"></div></div>
						</div>
						<div class="col-md-12">
							<div class="col-xs-8">
								Totais:
							</div>
							<div class="col-xs-1">
								<div class="form-group input-group">
									<input type="text" name="plano_pedagogico[qtdEncontros]" id="qtdEncontros" value="1" readonly class="form-control">
								</div>
							</div>
							<div class="col-xs-1">
								<div class="form-group input-group">
									<input type="text" name="plano_pedagogico[qtdViagens]" id="qtdViagens" value="1" readonly class="form-control">
								</div>
							</div>
							<div class="col-xs-1">
								<div class="form-group input-group">
									<input type="text" name="plano_pedagogico[qtdDiarias]" id="qtdDiarias" value="1" readonly class="form-control">
								</div>
							</div>
						</div>
					</div>
					<br>
					<div class='box-footer' style="float: left">
						<button type='submit' class='btn btn-primary pull-right'>Atualizar Semestres</button>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jQuery-2.2.0.min.js"></script>

<script>
	
	$(document).on("change", "input[id^='Hora']", function(ev) {
		var idInput = $(this).attr('id'); // ID Completo da linha
		var res = idInput.split("-"); // Código da linha
		var cod = res[1];
		var valTeoricas = $('#HoraTeorica-'+cod).val();
		var valPraticas = $('#HoraPratica-'+cod).val();
		
		if (valTeoricas >= 1 && valPraticas >= 1) {
			
			// Formula para calcular qtd de encontros
			var horasTeoricas = (<?php echo $configHorasTeoricas[0]["config_valor"]; ?>/100)*valTeoricas;
			var horasPraticas = (<?php echo $configHorasPraticas[0]["config_valor"]; ?>/100)*valPraticas;
			horasTeoricas = Math.round(horasTeoricas);
			horasPraticas = Math.round(horasPraticas);
			var qtdHorasGeral = horasTeoricas+horasPraticas;
			var qtdEncontrosParcial = Math.round(qtdHorasGeral/4);
			// Quantidade de viagens e diárias
			var qtdViagensParcial = 0;
			var qtdDiariasParcial = 0;
			if (qtdEncontrosParcial > 0 && qtdEncontrosParcial <= 4){qtdViagensParcial = 1; qtdDiariasParcial= 4;}
			if (qtdEncontrosParcial > 4 && qtdEncontrosParcial <= 8){qtdViagensParcial = 2;qtdDiariasParcial= 8;}
			if (qtdEncontrosParcial > 8 && qtdEncontrosParcial <= 12){qtdViagensParcial = 3;qtdDiariasParcial= 12;}
			if (qtdEncontrosParcial > 12 && qtdEncontrosParcial <= 16){qtdViagensParcial = 4;qtdDiariasParcial= 16;}
			if (qtdEncontrosParcial > 16){qtdEncontrosParcial = 16; qtdViagensParcial = 4; qtdDiariasParcial= 16;} // Máximo é 4 viagens ou 16 encontros
			// Atualiza os campos parciais
			$("#Encontro-"+cod).val(qtdEncontrosParcial);
			$("#Viagem-"+cod).val(qtdViagensParcial);
			$("#Diaria-"+cod).val(qtdDiariasParcial);
			// Atualiza campos gerais
				var sumEcontros = 0;
				var sumViagens = 0;
				var sumDiarias = 0;
			// Atualiza encontros
			$(".encontros").each(function(){ sumEcontros += +$(this).val();});
			// Atualiza viagens
			$(".viagens").each(function(){ sumViagens += +$(this).val(); });
			// Atualiza diarias
			$(".diarias").each(function(){ sumDiarias += +$(this).val(); });
			
			// Atualiza os valores
			$("#qtdEncontros").val(sumEcontros);
			$("#qtdViagens").val(sumViagens);
			$("#qtdDiarias").val(sumDiarias);
		} else {
		
			if (valTeoricas < 1){ $("#HoraTeorica-"+cod).val(1);}
			if (valPraticas < 1){ $("#HoraPratica-"+cod).val(1);}
			
		}
	});
			
	$(document).on("change", "input[id^='Encontro']", function(ev) {
		var sum = 0;
		$(".encontros").each(function(){
			sum += +$(this).val();
		});
		$("#qtdEncontros").val(sum);
	});
	$(document).on("change", "input[id^='Viagem']", function(ev) {
		var sum = 0;
		$(".viagens").each(function(){
			sum += +$(this).val();
		});
		$("#qtdViagens").val(sum);
	});
	$(document).on("change", "input[id^='Diaria']", function(ev) {
		var sum = 0;
		$(".diarias").each(function(){
			sum += +$(this).val();
		});
		$("#qtdDiarias").val(sum);
	});
	
	$(document).ready(function() {
		var max_fields      = 50; //Máximo de disciplinas
		var wrapper         = $(".input_fields_wrap"); //Fields wrapper
		var add_button      = $(".add_field_button"); //Add button ID
		var nrSemestre = 1;
		var x = 1; // initlal text box count
		
		$(add_button).click(function(e){ //on add input button click
			e.preventDefault();
			if(x < max_fields){ //max input box allowed
				x++; //text box increment
				var campos = '<span>';
				campos += '	<div class="col-xs-1">';
				campos += '	<input name="disciplina['+x+'][plano_nr_semestre]" required type="number" class="form-control" >';
				campos += '	</div>';
				campos += '	<div class="col-xs-5">';
				campos += '	<input name="disciplina['+x+'][plano_disciplina_titulo]" required type="text" class="form-control" >';
				campos += '	</div>';
				campos += '	<div class="col-xs-1">';
				campos += '	<input id="HoraTeorica-'+x+'" name="disciplina['+x+'][plano_disciplina_horas_teoricas]" type="number" class="form-control" value="1" >';
				campos += '	</div>';
				campos += '	<div class="col-xs-1">';
				campos += '	<input id="HoraPratica-'+x+'" name="disciplina['+x+'][plano_disciplina_horas_praticas]" type="number" class="form-control" value="1" >';
				campos += '	</div>';
				campos += '	<div class="col-xs-1">';
				campos += '	<input name="disciplina['+x+'][encontros]" id="Encontro-'+x+'" readonly type="number" class="form-control encontros" value="1" >';
				campos += '	</div>';
				campos += '	<div class="col-xs-1">';
				campos += '	<input name="disciplina['+x+'][viagens]" id="Viagem-'+x+'" readonly type="number" class="form-control viagens" value="1" >';
				campos += '	</div>';
				campos += '	<div class="col-xs-1">';
				campos += '	<input name="disciplina['+x+'][diarias]" id="Diaria-'+x+'" readonly type="number" class="form-control diarias" value="1" >';
				campos += '	</div>';
				
				campos += '	<div class="col-xs-1" style="text-align:center;"><a href="#"><button class="add_field_button btn btn-danger remove_field">-</button></a></div></span>';
				$(wrapper).append(campos); //add input box
			}
		});
		
		// Carrega os semestres já cadastrados
		<?php
			if ($disciplinasPlano) {
				foreach ($disciplinasPlano as $disciplina) {
				?>
				if (x < max_fields){ //max input box allowed
					x++; //text box increment
					var campos = '<span>';
					campos += '	<div class="col-xs-1">';
					campos += '	<input value="<?php echo $disciplina['plano_nr_semestre'];?>" name="disciplina['+x+'][plano_nr_semestre]" required type="number" class="form-control" >';
					campos += '	</div>';
					campos += '	<div class="col-xs-5">';
					campos += '	<input value="<?php echo $disciplina['plano_disciplina_titulo'];?>" name="disciplina['+x+'][plano_disciplina_titulo]" required type="text" class="form-control" >';
					campos += '	</div>';
					campos += '	<div class="col-xs-1">';
					campos += '	<input value="<?php echo $disciplina['plano_disciplina_horas_teoricas'];?>" id="HoraTeorica-'+x+'" name="disciplina['+x+'][plano_disciplina_horas_teoricas]" type="number" class="form-control" >';
					campos += '	</div>';
					campos += '	<div class="col-xs-1">';
					campos += '	<input value="<?php echo $disciplina['plano_disciplina_horas_praticas'];?>" id="HoraPratica-'+x+'" name="disciplina['+x+'][plano_disciplina_horas_praticas]" type="number" class="form-control"  >';
					campos += '	</div>';
					campos += '	<div class="col-xs-1">';
					campos += '	<input value="<?php echo $disciplina['plano_disciplina_qtd_encontros'];?>" name="disciplina['+x+'][encontros]" id="Encontro-'+x+'" readonly type="number" class="form-control encontros"  >';
					campos += '	</div>';
					campos += '	<div class="col-xs-1">';
					campos += '	<input value="<?php echo $disciplina['plano_disciplina_qtd_viagens'];?>" name="disciplina['+x+'][viagens]" id="Viagem-'+x+'" readonly type="number" class="form-control viagens"  >';
					campos += '	</div>';
					campos += '	<div class="col-xs-1">';
					campos += '	<input value="<?php echo $disciplina['plano_disciplina_qtd_diarias'];?>" name="disciplina['+x+'][diarias]" id="Diaria-'+x+'" readonly type="number" class="form-control diarias" >';
					campos += '	</div>';
					
					campos += '	<div class="col-xs-1" style="text-align:center;"><a href="#"><button class="add_field_button btn btn-danger remove_field">-</button></a></div></span>';
					$(wrapper).append(campos); //add input box
					
					// Preenche os somatórios gerais
					var sumE = 0;
					$(".encontros").each(function(){ sumE += +$(this).val(); });
					$("#qtdEncontros").val(sumE);
					var sumV = 0;
					$(".viagens").each(function(){ sumV += +$(this).val(); });
					$("#qtdViagens").val(sumV);
					var sumD = 0;
					$(".diarias").each(function(){ sumD += +$(this).val();});
					$("#qtdDiarias").val(sumD);
				}
				<?php
				}
			}
		?>
		
		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
			e.preventDefault(); 
			$(this).closest('span').remove(); x--;
		})
		
	});

</script>							