<?php
	$tableList = $this->planos_pedagogicos->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Planos Pedagogicos<small class='hidden-xs'><br>Lista de Planos Pedagogicos dos Cursos</small></h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>planos_pedagogicos'><i class='fa fa-users'></i> Planos_Pedagogicos</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>

<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th style="50px"><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Curso</th>
							<th style="120px"><i class='icon_profile'></i> Criado</th>
							<th style="120px"><i class='icon_profile'></i> Qtd. Semestres</th>
							<th style="120px"><i class='icon_profile'></i> Qtd. Encontros</th>
							<th style="120px"><i class='icon_profile'></i> Qtd. Viagens</th>
							<th style="120px"><i class='icon_profile'></i> Qtd. Diarias</th>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_plano_pedagogico']; ?></td>
									<td><?php echo $lista['curso_titulo']; ?></td>
									<td><?php echo date_format(new DateTime($lista['plano_pedagogico_data_criacao']), 'd/m/Y H:i:s'); ?></td>
									<td><?php echo $lista['plano_pedagogico_qtd_semestres']; ?></td>
									<td><?php echo $lista['plano_pedagogico_total_encontros']; ?></td>
									<td><?php echo $lista['plano_pedagogico_total_viagens']; ?></td>
									<td><?php echo $lista['plano_pedagogico_total_diarias']; ?></td>
									<td class='text-center acoes'>
										<div class='btn-group'>
											<a class='btn btn-warning btn-sm' href='<?php echo base_url();?>planos_pedagogicos/disciplinas/<?php echo $lista['pmk_plano_pedagogico'];?>' ><i class='fa fa-edit'></i> <span class='hidden-xs'>Disciplinas</span></a>
										</div>
									</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Curso</th>
							<th><i class='icon_profile'></i> Criado</th>
							<th><i class='icon_profile'></i> Qtd. Semestres</th>
							<th><i class='icon_profile'></i> Qtd. Encontros</th>
							<th><i class='icon_profile'></i> Qtd. Viagens</th>
							<th><i class='icon_profile'></i> Qtd. Diarias</th>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

