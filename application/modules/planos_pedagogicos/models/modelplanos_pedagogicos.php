<?php
/* Model that represents Planos_pedagogicos at database */

class ModelPlanos_pedagogicos extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('planos_pedagogicos tabela');
		$this->db->where('tabela.pmk_plano_pedagogico', $idTable);
		$this->db->join('cursos curso', 'curso.pmk_curso = tabela.fok_curso');
		$this->db->where('tabela.plano_pedagogico_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar($tableParam = ''){

		$where = array('tabela.plano_pedagogico_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_plano_pedagogico'])){ $where += array('tabela.pmk_plano_pedagogico' => $tableParam['pmk_plano_pedagogico']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('planos_pedagogicos tabela');
		$this->db->join('cursos curso', 'curso.pmk_curso = tabela.fok_curso');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('planos_pedagogicos',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_plano_pedagogico'];
			
			$this->db->where('pmk_plano_pedagogico', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('planos_pedagogicos');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['plano_pedagogico_is_ativo'] = 'Nao';
		$tableParam['pmk_plano_pedagogico'] = $idTable;
		
		$this->db->where('pmk_plano_pedagogico', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('planos_pedagogicos');
	}
	
	public function deletar_por_curso ($idTable) {
		$tableParam['plano_pedagogico_is_ativo'] = 'Nao';
		$tableParam['fok_curso'] = $idTable;
		
		$this->db->where('fok_curso', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('planos_pedagogicos');
	}
	
	
}
