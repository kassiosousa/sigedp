<?php
	$tableList = $this->configuracoes->listar();
	$configs = array();
	foreach ($tableList as $row) $configs[$row['pmk_config']] = $row;
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Configuracoes<small class='hidden-xs'><br>Configuracoes do sistema</small>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>configuracoes'><i class='fa fa-gear'></i> Configuracoes</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>

<?php if ($this->session->flashdata('form_sucesso')): ?>
<div class="alert alert-success alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<p><strong><?php echo $this->session->flashdata('form_sucesso'); ?></strong></p>
</div>
<?php endif; ?>

<?php if (!empty($erros)): ?>
<?php print_r($erros); ?>
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<p><strong>Por favor, corrija os erros indicados abaixo.</strong></p>
</div>
<?php endif; ?>

<!-- Modal de Editar Configuração -->
<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>

				<form class='form-horizontal' method="post">

					<fieldset>
						<legend>
							<small class="text-muted">Geral</small>
						</legend>

						<div class="row">
							<div class='form-group col-md-6'>
								<label for='config_porc_hora_teorica' class='col-sm-7 control-label' style="text-align:left;padding-top:0;">
									Porcentagem de hora teórica que deve ser convertido em encontros
								</label>
								<div class='col-sm-5 input-group'>
									<input type='number' min="0" max="100" class='form-control' id='config_porc_hora_teorica' name='1'  value='<?php echo $configs[1]['config_valor']; ?>' placeholder="Hora Teórica" required>
									<span class="input-group-addon">%</span>
								</div>
							</div>
							<div class='form-group col-md-6'>
								<label for='config_porc_hora_pratica' class='col-sm-7 control-label' style="text-align:left;padding-top:0;">
									Porcentagem de hora prática que deve ser convertido em encontros
								</label>
								<div class='col-sm-5 input-group'>
									<input type='number' min="0" max="100" class='form-control' id='config_porc_hora_pratica' name='2' value='<?php echo $configs[2]['config_valor']; ?>' placeholder="Hora Prática" required>
									<span class="input-group-addon">%</span>
								</div>
							</div>
						</div>
						
						<!--
						<div class="row">
							<div class='form-group col-md-6'>
								<label for='config_qtd_viagens_polo' class='col-sm-7 control-label' style="text-align:left;padding-top:0;">
									Quantidade padrão de viagens por polo
								</label>
								<div class='col-sm-4 input-group'>
									<input type='number' min="0" class='form-control' id='config_qtd_viagens_polo' name='3' value='<?php echo $configs[3]['config_valor']; ?>' placeholder='Viagens por Polo' required>
								</div>
							</div>
							<div class='form-group col-md-6'>
								<label for='config_qtd_diarias_polo' class='col-sm-7 control-label' style="text-align:left;padding-top:0;">
									Quantidade padrão de diárias por polo
								</label>
								<div class='col-sm-4 input-group'>
									<input type='number' min="0" class='form-control' id='config_qtd_diarias_polo' name='4' value='<?php echo $configs[4]['config_valor']; ?>' placeholder='Diárias por Polo' required>
								</div>
							</div>
						</div>
						-->
					</fieldset>

						<fieldset>
							<legend>
								<small class="text-muted">Notificações</small>
							</legend>

							<div class="row">
								<div class='form-group col-md-6'>
									<label for='config_ver_relatorio_viagem_pendente' class='col-sm-7 control-label' style="text-align:left;">
										Relatório de viagem pendente
									</label>
									<div class='col-sm-5'>
										<select class='form-control' name="3" id="config_ver_relatorio_viagem_pendente">
											<option <?php if($configs[3]['config_valor']=="0"){echo"selected";} ?> value="0">Não</option>
											<option <?php if($configs[3]['config_valor']=="1"){echo"selected";} ?> value="1">Sim</option>
										</select>
									</div>
								</div>
								<div class='form-group col-md-6'>
									<label for='config_ver_data_viagem_finalizada' class='col-sm-7 control-label' style="text-align:left;">
										Data de viagem finalizada
									</label>
									<div class='col-sm-5'>
										<select class='form-control'  name="4" id="config_ver_data_viagem_finalizada">
											<option <?php if($configs[4]['config_valor']=="0"){echo"selected";} ?> value="0">Não</option>
											<option <?php if($configs[4]['config_valor']=="1"){echo"selected";} ?> value="1">Sim</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class='form-group col-md-6'>
									<label for='config_ver_nova_diaria' class='col-sm-7 control-label' style="text-align:left;">
										Novas solicitações de diária
									</label>
									<div class='col-sm-5'>
										<select class='form-control' name="5" id="config_ver_nova_diaria">
											<option <?php if($configs[5]['config_valor']=="0"){echo"selected";} ?> value="0">Não</option>
											<option <?php if($configs[5]['config_valor']=="1"){echo"selected";} ?> value="1">Sim</option>
										</select>
									</div>
								</div>
								<div class='form-group col-md-6'>
									<label for='config_ver_prox_data_limite_envio_relatorio' class='col-sm-7 control-label' style="text-align:left;">
										Proximidade da data de envio de relatório
									</label>
									<div class='col-sm-5'>
										<select class='form-control' name="6" id="config_ver_prox_data_limite_envio_relatorio">
											<option <?php if($configs[6]['config_valor']=="0"){echo"selected";} ?> value="0">Não</option>
											<option <?php if($configs[6]['config_valor']=="1"){echo"selected";} ?> value="1">Sim</option>
										</select>
									</div>
								</div>
							</div>

						</fieldset>

						<div class='form-group'>
							<div class='col-sm-12'>
								<button data-bb-handler="enviar" type="submit" class="btn btn-primary">Atualizar</button>
							</div>
						</div>
					</form>

			</div>
		</div>
	</div>
</div>

<?php if (!empty($erros)): ?>
<!-- adaptação da validação crud.js -->
<script>
	gambizinha = setInterval(function(){
		if (typeof($) == 'function')
		{
			clearInterval(gambizinha);
			var erros = <?php echo json_encode($erros); ?>;
			for (var name in erros)
			{
			    var input = $('form [name="'+ name +'"]');
			    input.val('');
			    input.parents('.form-group').addClass('has-error');
			    if (input.siblings('.help-block').length == 0) input.after('<small class="help-block">'+ erros[name] +'</small>');
			    else input.siblings('.help-block').text(erros[name]);
			}
		}
	},1000);
</script>
<?php endif; ?>
