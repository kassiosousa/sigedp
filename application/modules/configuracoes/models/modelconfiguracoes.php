<?php
/* Model that represents Configuracoes at database */

class ModelConfiguracoes extends CI_Model {

	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('configuracoes tabela');
		$this->db->where('tabela.pmk_config', $idTable);
		$this->db->where('tabela.config_is_ativo', 'Sim');

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function listar($tableParam = ''){

		$where = array('tabela.config_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_config'])){ $where += array('tabela.pmk_config' => $tableParam['pmk_config']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('configuracoes tabela');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function criar($tableParam) {
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('configuracoes',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();
			return $insert_id;
		} else {
			return false;
		}
	}

	public function editar ($post)
	{
		foreach ($post as $key => $value)
		{
			$this->db->where('pmk_config', $key);
			$this->db->set(array('config_valor' => $value));
			$this->db->update('configuracoes');
		}
	}

	public function deletar ($idTable) {
		$tableParam['config_is_ativo'] = 'Nao';
		$tableParam['pmk_config'] = $idTable;

		$this->db->where('pmk_config', $idTable);
		$this->db->set($tableParam);

		return $this->db->update('configuracoes');
	}

	public function cidades_listar($tableParam = ''){

		if(isset($tableParam['id_estado'])){ $where += array('cidade.fok_estado' => $tableParam['fok_estado']); }

		$this->db->select('*');
		$this->db->from('cidade');
		$this->db->join('estado', 'estado.pmk_estado = cidade.fok_estado');
		if (isset($where)) {
			$this->db->where($where);
		}


		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

}
