<?php
/* CRUD Controller for Configuracoes */

class Configuracoes extends CI_Controller {

	function _remap($method){
		//Carrega os modelos a serem utilizados
		$this->load->model('Configuracoes/modelconfiguracoes', 'configuracoes');

		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }

		// chama a função de acordo com o método (method)
		switch( $method ) {

			default:
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 1) {
					redirect('home', 'refresh');
				} else {

					// pega todo o post já limpo de html/css/js (anti trolls)
					$post = $this->input->post(NULL, TRUE);

					// se foi um POST
					if ($post) {

						// usaremos o codeigniter para validar nosso input
						$this->load->library('form_validation');
						$this->lang->load('form_validation', 'portuguese'); // << traduções

						// regras
						$this->form_validation->set_rules('1', 'Porcentagem de Hora Teórica', 'required|integer|greater_than[-1]|less_than[101]');
						$this->form_validation->set_rules('2', 'Porcentagem de Hora Prática', 'required|integer|greater_than[-1]|less_than[101]');
						$this->form_validation->set_rules('3', 'Relatório de Viagem Pendente', 'required|is_natural');
						$this->form_validation->set_rules('4', 'Data de Viagem Finalizada', 'required|is_natural');
						$this->form_validation->set_rules('5', 'Novas Solicitações de Diária', 'required|is_natural');
						$this->form_validation->set_rules('6', 'Proximidade de envio de relatório', 'required|is_natural');

						// testa o formulário com a validação
						$sucesso = $this->form_validation->run();

						if ($sucesso) {
							$this->configuracoes->editar($post);
							$this->session->set_flashdata('form_sucesso', 'Configurações atualizadas com sucesso!');
							redirect('configuracoes', 'refresh'); // << nenhum erro ocorreu (cadastro com sucesso)
						} else
						{
							$data = array();
							$data['erros'] = $this->form_validation->get_all_errors_array();
							$this->template->load('templates/template_painel', 'index', $data);
						}
					}
					// se foi um GET
					else {
						$this->template->load('templates/template_painel', 'index');
					}
				}
			break;


		}
	}
}

