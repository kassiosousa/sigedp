<?php
/* Model that represents Pedidos at database */

class ModelPedidos extends CI_Model {

	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('pedidos tabela');
		$this->db->where('tabela.pmk_pedido', $idTable);
		$this->db->where('tabela.pedi_is_ativo', 'Sim');

		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function listar($tableParam = '')
    {
        // (1) Diretor -> TUDO
        // (2) Coord Admin -> TUDO menos oq tiver fora do planejamento (para facilitar ser� listado, por�m n�o conseguir� aprovar)
        // (3) Secret Adm -> Pedidos "Aprovado"
        // (4) Coord Curso -> Pedidos que ele criou + pedidos "Pendente"
        // (5) Secret Curso -> Pedidos que ele criou

        // Qualquer usuario ve os pedidos criados PARA ele

        // obs.: o 3 n�o ve os pedidos que ele criou pq pela regra ele n�o pode criar

		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

        // REGRAS ACIMA
        $where = "(";
        if ($this->session->userdata['usuatipo_nivel_acesso'] <= 2)
        {
            $where .= "1=1";
        }
        elseif ($this->session->userdata['usuatipo_nivel_acesso'] == 3)
        {
            $where .= "pedi_status = 'Aprovado'";
        }
        elseif ($this->session->userdata['usuatipo_nivel_acesso'] == 4)
        {
            $where .= "fok_usuario_criador = ".$this->session->userdata['pmk_usuario']." OR pedi_status = 'Pendente'";
        }
        elseif ($this->session->userdata['usuatipo_nivel_acesso'] == 5)
        {
            $where .= "fok_usuario_criador = ".$this->session->userdata['pmk_usuario'];
        }
        else
        {
            $where .= "0=1";
        }
        $where .= " OR fok_usuario = ".$this->session->userdata['pmk_usuario'].") AND pedi_is_ativo = 'Sim'";

        // FILTROS
        if ($this->input->get('status'))
        {
            $where .= " AND pedi_status = '".$this->db->escape_str($this->input->get('status'))."'";
        }

		$this->db->select('*');
		$this->db->from('pedidos');
        $this->db->where($where);
        $this->db->order_by("pmk_pedido", "desc");

		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	


	public function listar_por_curso($tableParam = ''){

		$where = array('tabela.pedi_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_pedido'])){ $where += array('tabela.pmk_pedido' => $tableParam['pmk_pedido']); }
		if(isset($tableParam['fok_curso'])){ $where += array('plano.fok_curso' => $tableParam['fok_curso']); }
		if(isset($tableParam['fok_plano_disicplina'])){ $where += array('tabela.fok_plano_disicplina' => $tableParam['fok_plano_disicplina']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['pedi_tipo'])){ $where += array('tabela.pedi_tipo' => $tableParam['pedi_tipo']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('pedidos tabela');
		$this->db->join('planos_pedagogicos_disciplinas disci', 'disci.pmk_plano_disciplina = tabela.fok_plano_disicplina');
		$this->db->join('planos_pedagogicos plano', 'plano.pmk_plano_pedagogico = disci.fok_plano_pedagogico');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

	public function criar($tableParam) {
		if (isset($tableParam)){

            $tableParam['fok_usuario_criador'] = $this->session->userdata['pmk_usuario'];

			$this->db->trans_start();
			$this->db->insert('pedidos',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();
			return $insert_id;
		} else {
			return false;
		}
	}

	public function editar ($tableParam) {

		if (isset($tableParam)){
			$idTable = $tableParam['pmk_pedido'];

			$this->db->where('pmk_pedido', $idTable);
			$this->db->set($tableParam);

			return $this->db->update('pedidos');
		} else {
			return false;
		}
	}

	public function deletar ($idTable) {
		$tableParam['pedi_is_ativo'] = 'Nao';
		$tableParam['pmk_pedido'] = $idTable;

		$this->db->where('pmk_pedido', $idTable);
		$this->db->set($tableParam);

		return $this->db->update('pedidos');
	}

    public function buscar_fok_plano_disciplina($pmk_curso)
    {
        $this->db->select('pmk_plano_disciplina, plano_disciplina_titulo, plano_nr_semestre');
        $this->db->from('planos_pedagogicos_disciplinas');
        $this->db->join('planos_pedagogicos', 'fok_plano_pedagogico = pmk_plano_pedagogico');
        $this->db->where('fok_curso', $pmk_curso);
        $this->db->distinct();

        $query = $this->db->get();
        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function buscar_pedido_curso_str($fok_plano_disicplina)
    {
        $this->db->select('pmk_curso, curso_titulo, plano_disciplina_titulo, plano_nr_semestre');
        $this->db->from('planos_pedagogicos_disciplinas');
        $this->db->join('planos_pedagogicos', 'fok_plano_pedagogico = pmk_plano_pedagogico');
        $this->db->join('cursos', 'fok_curso = pmk_curso');
        $this->db->where('pmk_plano_disciplina', $fok_plano_disicplina);
        $this->db->distinct();

        $query = $this->db->get();
        if($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function relatorios_viagens($pmk_pedido)
    {
        $this->db->select('*');
        $this->db->from('pedidos_relatorios_viagens');
        $this->db->where('fok_pedido', $pmk_pedido);
        $this->db->where('pedido_rel_is_ativo', 'Sim');

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function saldo_diarias_polo($pmk_polo)
    {
        // pega a qtde de di�rias do polo que est�o sendo/foram usadas
        $this->db->select_sum('pedi_diaria_quant');
        $this->db->from('pedidos');
        $this->db->where('pedi_is_ativo', 'Sim');
        //$this->db->where('pedi_tipo', 'Polo'); n�o filtramos por Polo porque tipo 'Curso' tbm entra no montante
        $this->db->where('pedi_status !=', 'Rejeitado');
        $this->db->where('pedi_status !=', 'Cancelado');
        $this->db->where('fok_polo', $pmk_polo);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $diarias_usadas = $query->row()->pedi_diaria_quant;
        } else {
            $diarias_usadas = 0;
        }

        // agora pegamos o somat�rio dos limites de di�rias dos cursos desse polo
        //$this->db->select_sum('curso_conta_qtd_diarias');
        $this->db->select_sum('curso_conta_qtd_diarias_inicial');
        $this->db->from('cursos_contas');
        $this->db->join('cursos_polos', 'cursos_polos.fok_curso = cursos_contas.fok_curso');
        $this->db->where('curso_conta_is_ativo', 'Sim');
        $this->db->where('cursos_polos.fok_polo', $pmk_polo);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            //$limite_diarias = $query->row()->curso_conta_qtd_diarias;
            $limite_diarias = $query->row()->curso_conta_qtd_diarias_inicial;
        } else {
            $limite_diarias = 0;
        }

        return $limite_diarias - $diarias_usadas;
    }

    public function saldo_diarias_curso($pmk_curso, $pmk_polo)
    {
        // pega a qtde de di�rias do curso que est�o sendo/foram usadas
        $this->db->select_sum('pedi_diaria_quant');
        $this->db->from('pedidos');
        $this->db->join('planos_pedagogicos_disciplinas', 'pmk_plano_disciplina = fok_plano_disicplina');
        $this->db->join('planos_pedagogicos', 'pmk_plano_pedagogico = fok_plano_pedagogico');
        $this->db->where('pedi_is_ativo', 'Sim');
        $this->db->where('pedi_tipo', 'Curso');
        $this->db->where('pedi_status !=', 'Rejeitado');
        $this->db->where('pedi_status !=', 'Cancelado');
        $this->db->where('fok_polo', $pmk_polo);
        $this->db->where('fok_curso', $pmk_curso);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $diarias_usadas = $query->row()->pedi_diaria_quant;
        } else {
            $diarias_usadas = 0;
        }

        // agora pegamos o limite de di�rias do curso
        //$this->db->select('curso_conta_qtd_diarias');
        $this->db->select('curso_conta_qtd_diarias_inicial');
        $this->db->from('cursos_contas');
        $this->db->where('curso_conta_is_ativo', 'Sim');
        $this->db->where('fok_curso', $pmk_curso);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            //$limite_diarias = $query->row()->curso_conta_qtd_diarias;
            $limite_diarias = $query->row()->curso_conta_qtd_diarias_inicial;
        } else {
            $limite_diarias = 0;
        }

        return $limite_diarias - $diarias_usadas;
    }

    public function saldo_diarias_disciplina($pmk_plano_disciplina, $pmk_polo)
    {
        // pega a qtde de di�rias da disciplina que est�o sendo/foram usadas
        $this->db->select_sum('pedi_diaria_quant');
        $this->db->from('pedidos');
        $this->db->where('pedi_is_ativo', 'Sim');
        $this->db->where('pedi_tipo', 'Curso');
        $this->db->where('pedi_status !=', 'Rejeitado');
        $this->db->where('pedi_status !=', 'Cancelado');
        $this->db->where('fok_polo', $pmk_polo);
        $this->db->where('fok_plano_disicplina', $pmk_plano_disciplina);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $diarias_usadas = $query->row()->pedi_diaria_quant;
        } else {
            $diarias_usadas = 0;
        }

        // agora pegamos o limite de di�rias da disciplina
        $this->db->select('plano_disciplina_qtd_diarias');
        $this->db->from('planos_pedagogicos_disciplinas');
        $this->db->where('pmk_plano_disciplina', $pmk_plano_disciplina);

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $limite_diarias = $query->row()->plano_disciplina_qtd_diarias;
        } else {
            $limite_diarias = 0;
        }

        return $limite_diarias - $diarias_usadas;
    }

    public function saldo_diarias_nead()
    {
        // pega a qtde de di�rias DE TUDO que est�o sendo/foram usadas
        $this->db->select_sum('pedi_diaria_quant');
        $this->db->from('pedidos');
        $this->db->where('pedi_is_ativo', 'Sim');
        $this->db->where('pedi_status !=', 'Rejeitado');
        $this->db->where('pedi_status !=', 'Cancelado');
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $diarias_usadas = $query->row()->pedi_diaria_quant;
        } else {
            $diarias_usadas = 0;
        }

        // agora pegamos o limite de di�rias DE TUDO
        //$this->db->select_sum('curso_conta_qtd_diarias');
        $this->db->select_sum('curso_conta_qtd_diarias_inicial');
        $this->db->from('cursos_contas');
        $this->db->where('curso_conta_is_ativo', 'Sim');

        $query = $this->db->get();

        if($query->num_rows() > 0) {
            //$limite_diarias = $query->row()->curso_conta_qtd_diarias;
            $limite_diarias = $query->row()->curso_conta_qtd_diarias_inicial;
        } else {
            $limite_diarias = 0;
        }

        return $limite_diarias - $diarias_usadas;
    }
}
