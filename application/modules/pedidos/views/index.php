<?php

    $tableList = $this->pedidos->listar();
    $usuarios = $this->usuarios->todos();
    $polos = $this->polos->todos();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Pedidos<small class='hidden-xs'><br>Lista de Pedidos cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>pedidos/criar' data-header='Criar Pedido' <?php echo ($this->session->userdata['usuatipo_nivel_acesso'] > 5 || $this->session->userdata['usuatipo_nivel_acesso'] == 3)? 'disabled':''; ?>>
				<i class='fa fa-plus'></i> &nbsp;Criar Pedidos
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>pedidos'><i class='fa fa-suitcase'></i> Pedidos</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
                            <th>Para</th>
                            <th>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0;">
                                        Status <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="pedidos?status=">
                                                <?php if($this->input->get('status') == ''): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Todos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Aguardando">
                                                <?php if($this->input->get('status') == 'Aguardando'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Aguardando
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Rejeitado">
                                                <?php if($this->input->get('status') == 'Rejeitado'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Rejeitado
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Aprovado">
                                                <?php if($this->input->get('status') == 'Aprovado'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Aprovado
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Liberado">
                                                <?php if($this->input->get('status') == 'Liberado'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Liberado
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Cancelado">
                                                <?php if($this->input->get('status') == 'Cancelado'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Cancelado
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Pendente">
                                                <?php if($this->input->get('status') == 'Pendente'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Pendente
                                            </a>
                                        </li>
                                        <li>
                                            <a href="pedidos?status=Concluído">
                                                <?php if($this->input->get('status') == 'Concluído'): ?>
                                                <span class="fa fa-check" style="color:green;"></span>
                                                <?php endif; ?>
                                                Concluído
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </th>
                            <th>Tipo</th>
                            <th>Viajem</th>
                            <th>Diárias</th>
                            <th>Valor da Viagem</th>
                            <th>Requerente</th>
                            <td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
                                    <td><?php echo $usuarios[$lista['fok_usuario']]; ?></td>
                                    <td><?php echo $lista['pedi_status']; ?></td>
                                    <td><?php
                                        echo $lista['pedi_tipo'];
                                        if ($lista['pedi_tipo'] == 'Polo')
                                        {
                                            echo ' ('.$polos[$lista['fok_polo']].')';
                                        }
                                        elseif($lista['pedi_tipo'] == 'Curso')
                                        {
                                            $curso_pedido = $this->pedidos->buscar_pedido_curso_str($lista['fok_plano_disicplina']);
                                            if (!empty($curso_pedido))
                                            {
                                                echo ' ('.$curso_pedido['curso_titulo'].')<br><small class="text-muted">'.
                                                $curso_pedido['plano_disciplina_titulo'].' - sem. '.$curso_pedido['plano_nr_semestre'].'</small>';
                                            }
                                            else
                                            {
                                                echo ' (<span class="text-danger">NÃO ENCONTRADO!</span>)';
                                            }
                                        }
                                    ?></td>
                                    <td><?php
                                        echo 'ida: ' . date('d/m/Y',strtotime($lista['pedi_viagem_ida_dia'])) .
                                             '<br>volta: ' . date('d/m/Y',strtotime($lista['pedi_viagem_volta_dia']))
                                    ?></td>
                                    <td><?php echo $lista['pedi_diaria_quant']; ?></td>
                                    <td><?php
                                        echo 'ida: R$' . number_format($lista['pedi_viagem_ida_valor_previsto'], 2,',','') .
                                             '<br>volta: R$' . number_format($lista['pedi_viagem_volta_valor_previsto'], 2,',','')
                                    ?></td>
                                    <td><?php echo $usuarios[$lista['fok_usuario_criador']]; ?></td>
                                    <td class='text-center acoes' style="width: 165px;">
                                        <div class='btn-group'>
                                            <button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>pedidos/editar/?id=<?php echo $lista['pmk_pedido'];?>' data-header='Avaliar Pedido'><i class='fa fa-edit'></i> <span class='hidden-xs'>Avaliar</span></button>
                                            <button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>pedidos/deletar/?id=<?php echo $lista['pmk_pedido'];?>' data-header='Confirmar Exclusão' <?php echo ($this->session->userdata['pmk_usuario'] != $lista['fok_usuario_criador'] || ($lista['pedi_status'] != 'Aguardando' && $lista['pedi_status'] != 'Pendente')) ? 'disabled':'' ?>><i class='fa fa-trash'></i> <span class='hidden-xs'>Excluir</span></button>
                                       </div>
                                    </td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
                            <th>Para</th>
                            <th>
                                <?php if($this->input->get('status')): ?>
                                <?php
                                switch ($this->input->get('status'))
                                {
                                    case 'Aguardando': case 'Pendente': $c = 'info'; break;
                                    case 'Aprovado': $c = 'primary'; break;
                                    case 'Rejeitado': $c = 'warning'; break;
                                    case 'Liberado': case 'Concluído': $c = 'success'; break;
                                    case 'Cancelado': $c = 'danger'; break;
                                    default: $c = 'default'; break;
                                }
                                ?>
                                Status <span class="label label-<?php echo $c; ?>"><?php echo $this->input->get('status'); ?></span>
                                <?php else: ?>
                                Status
                                <?php endif; ?>
                            </th>
                            <th>Tipo</th>
                            <th>Viajem</th>
                            <th>Diárias</th>
                            <th>Valor da Viagem</th>
                            <th>Requerente</th>
                            <td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

