<div style="height:10px;"></div>
<div id="div_justificativa" class="form-group hide">
    <label for="justificativa" class="control-label col-xs-12" style="text-align:left;">Informe uma justificativa para o cancelamento</label>
    <div class="col-xs-12">
        <textarea name="justificativa" id="justificativa" class="form-control"></textarea>
    </div>
</div>
<div class="form-group clearfix;">
    <label for="pedi_status" class="pull-right" style="margin-right:15px;display:inline-block;">Ação a tomar:</label>
    <div style="float:right;height:1px;width:100%;"></div>
    <div class="pull-right" style="margin-right: 15px;">
        <select name="pedi_status" id="pedi_status" class="form-control" onchange="pedi_status_change();">
            <option value>- escolha uma ação -</option>
            <?php if( $this->session->userdata('usuatipo_nivel_acesso') == 4 ): ?>
            <option value="Aguardando">Aceitar</option>
            <option value="Cancelado">Cancelar</option>
            <?php else: ?>
            <option value disabled>somente o coordenador pode avaliar</option>
            <?php endif; ?>
        </select>
    </div>
</div>

<script type="text/javascript">
    function pedi_status_change ()
    {
       if ($('#pedi_status').val() == 'Rejeitado') $('#div_justificativa').removeClass('hide');
       else $('#div_justificativa').addClass('hide');
    }
</script>
