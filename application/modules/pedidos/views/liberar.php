<?php if( $this->session->userdata('usuatipo_nivel_acesso') == 3 ): ?>
<p>Informe os números de ofício abaixo para liberar o pedido:</p>
<div class="form-horizontal">
    <div class="form-group">
        <label for="oficio_diarias" class="col-sm-4 control-label">N° ofício diárias:</label>
        <div class="col-sm-6">
            <input type="text" id="oficio_diarias" name="oficio_diarias" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="oficio_passagens" class="col-sm-4 control-label">N° ofício passagens:</label>
        <div class="col-sm-6">
            <input type="text" id="oficio_passagens" name="oficio_passagens" class="form-control">
        </div>
    </div>
</div>
<?php else: ?>
<p class="text-muted">
    Aguardando liberação do Secretário Administrativo
</p>
<?php endif; ?>
