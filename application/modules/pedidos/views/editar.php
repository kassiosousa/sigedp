<?php
    // REGRAS:
    // (1) Diretor -> pega um "aguardando" e passa para "aprovado" ou "rejeitado"
    // (2) Coord Admin -> pega um "aguardando" e passa para "aprovado" ou "rejeitado" (exceto se tiver fora do planejamento)
    // (3) Secret Adm -> "libera" ou "cancela" os pedidos "aprovados"
    // (4) Coord Curso -> pega um "pedente" e passa para "aguardando" ou "cancelado"

    if (!empty($fok_usuario)) $para = $this->usuarios->carregar($fok_usuario);
    if (!empty($fok_usuario_criador)) $de = $this->usuarios->carregar($fok_usuario_criador);
    if (!empty($fok_polo)) $polo = $this->polos->carregar($fok_polo);
    if (!empty($fok_plano_disicplina)) $curso = $this->pedidos->buscar_pedido_curso_str($fok_plano_disicplina);
?>

<form class='form-horizontal'>
	<input type='hidden' name='pmk_pedido' value='<?php echo $pmk_pedido; ?>'>

    <!-- DETALHAMENTO DO PEDIDO -->
    <h4>
        Para: <?php echo $para[0]['usua_nome']; ?> <br>
        <small>
            Tipo: <?php echo $pedi_tipo; ?>
            <?php if($pedi_tipo == 'Polo'): ?>
            (<?php echo $polo[0]['polo_titulo']; ?>)
            <?php elseif($pedi_tipo == 'Curso'): ?>
            <?php echo '('.$curso['curso_titulo'].') p/ '.$curso['plano_disciplina_titulo'].' '.$curso['plano_nr_semestre'].'º semestre'; ?>
            <?php endif; ?>
        </small>
    </h4>
    <p>
        Requerido por: <?php echo $de[0]['usua_nome']; ?>
    </p>
    <p>
        <?php
            // cores de cada status
            switch ($pedi_status)
            {
                case 'Aguardando': case 'Pendente': $c = 'info'; break;
                case 'Aprovado': $c = 'primary'; break;
                case 'Rejeitado': $c = 'warning'; break;
                case 'Liberado': case 'Concluído': $c = 'success'; break;
                case 'Cancelado': $c = 'danger'; break;
                default: $c = 'default'; break;
            }
        ?>
        Status: <span class="label label-<?php echo $c; ?>"><?php echo $pedi_status; ?></span>
    </p>
    <p>
        <u>Quantidade de Diárias:</u>
        <?php echo "$pedi_diaria_quant, com início em ".date('d/m/Y',strtotime($pedi_diaria_dia_inicial)) ?>
    </p>
    <p>
        <u>Viagem de Ida:</u> em
        <?php echo date('d/m/Y',strtotime($pedi_viagem_ida_dia)).', transporte '.$pedi_viagem_ida_transporte.', valor R$ '.number_format($pedi_viagem_ida_valor_previsto,2,',','') ?>
    </p>
    <p>
        <u>Viagem de Volta:</u> em
        <?php echo date('d/m/Y',strtotime($pedi_viagem_volta_dia)).', transporte '.$pedi_viagem_volta_transporte.', valor R$ '.number_format($pedi_viagem_volta_valor_previsto,2,',','') ?>
    </p>
    <p>
        <u>Informações:</u> <br>
        <pre><?php echo $pedi_justificativa; ?></pre>
    </p>
    <?php $saldo_diarias_nead = $this->pedidos->saldo_diarias_nead(); ?>

    <hr>

    <!-- AÇÔES -->
    <?php if($pedi_status == 'Aguardando'): ?>
    <?php include FCPATH. 'application/modules/pedidos/views/aprovar.php'; ?>
    <?php elseif($pedi_status == 'Pendente'): ?>
    <?php include FCPATH. 'application/modules/pedidos/views/aguardar.php'; ?>
    <?php elseif($pedi_status == 'Aprovado'): ?>
    <?php include FCPATH. 'application/modules/pedidos/views/liberar.php'; ?>
    <?php elseif($pedi_status == 'Liberado'): ?>
    <?php include FCPATH. 'application/modules/pedidos/views/relatorio.php'; ?>
    <?php endif; ?>

</form>

