

<!-- Modal de Criar -->

<form class='form-horizontal'>

    <div class='form-group'>
        <label for='pedi_tipo' class='col-sm-4 control-label'>Tipo:</label>
        <div class='col-sm-8'>
            <select name="pedi_tipo" id="pedi_tipo" class="form-control" autofocus>
                <?php if( $this->session->userdata('usuatipo_nivel_acesso') < 2 ): ?>
                <option value="Nead">Nead</option>
                <?php endif; ?>
                <option value="Curso">Curso</option>
                <option value="Polo">Polo</option>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='fok_polo' class='col-sm-4 control-label'>Polo:</label>
        <div class='col-sm-8'>
            <select name="fok_polo" id="fok_polo" class="form-control">
                <option value>- Selecione o Polo -</option>
                <?php foreach($polos as $polo): ?>
                <option value="<?php echo $polo['pmk_polo'] ?>"><?php echo $polo['polo_titulo'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class='form-group'>
        <label for='uf' class='col-sm-4 control-label'>Curso:</label>
        <div class='col-sm-8'>
            <select id="uf" name="curso" class="form-control select-cidade" data-ref="#fok_polo" data-param="polo" style="width:100%;" data-url="<?php echo base_url();?>pedidos/cursos">
                <option value>- Selecione o Polo -</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="fok_plano_disicplina" class="col-sm-4 control-label">Disciplina:</label>
        <div class="col-sm-8">
            <select id="fok_plano_disicplina" name="fok_plano_disicplina" class="form-control select-cidade" style="width:100%;" data-url="<?php echo base_url();?>pedidos/disciplinas">
                <option value>- Selecione Curso -</option>
            </select>
        </div>
    </div>
	<div class='form-group'>
		<label for='fok_usuario' class='col-sm-4 control-label'>Usuário:</label>
		<div class='col-sm-8'>
            <select name="fok_usuario" id="fok_usuario" class="form-control select-search" style="width:100%;">
            <?php foreach($usuarios as $usuario): ?>
            <option value="<?php echo $usuario['pmk_usuario'] ?>"><?php echo $usuario['usua_nome'] ?></option>
            <?php endforeach; ?>
            </select>
		</div>
	</div>

    <fieldset>
        <legend>
            <small class="text-muted">Diárias</small>
        </legend>

        <div class='form-group'>
            <label for='pedi_diaria_dia_inicial' class='col-sm-4 control-label'>Início das Diárias:</label>
            <div class='col-sm-8'>
                <input type='date' class='form-control input-date' id='pedi_diaria_dia_inicial' name='pedi_diaria_dia_inicial' required>
            </div>
        </div>
        <div class='form-group'>
            <label for='pedi_diaria_quant' class='col-sm-4 control-label'>Número de Diárias:</label>
            <div class='col-sm-8'>
                <input type='number' min="0" class='form-control' id='pedi_diaria_quant' name='pedi_diaria_quant' required>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>
            <small class="text-muted">Viagem de Ida</small>
        </legend>
        <div class='form-group'>
            <label for='pedi_viagem_ida_dia' class='col-sm-4 control-label'>Data da Viagem (ida):</label>
            <div class='col-sm-8'>
                <input type='date' class='form-control input-date' id='pedi_viagem_ida_dia' name='pedi_viagem_ida_dia' required>
            </div>
        </div>
        <div class='form-group'>
            <label for='pedi_viagem_ida_valor_previsto' class='col-sm-4 control-label'>Valor da Viagem (ida):</label>
            <div class='col-sm-8'>
                <input type='number' min="0" class='form-control' id='pedi_viagem_ida_valor_previsto' name='pedi_viagem_ida_valor_previsto' required>
            </div>
        </div>
        <div class='form-group'>
            <label for='pedi_viagem_ida_transporte' class='col-sm-4 control-label'>Transporte (ida):</label>
            <div class='col-sm-8'>
                <select name="pedi_viagem_ida_transporte" id="pedi_viagem_ida_transporte" class="form-control">
                    <option value="Terrestre">Terrestre</option>
                    <option value="Carro Inst.">Carro da Instituição</option>
                    <option value="Carro Particular">Carro Particular</option>
                </select>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>
            <small class="text-muted">Viagem de Volta</small>
        </legend>
        <div class='form-group'>
            <label for='pedi_viagem_volta_dia' class='col-sm-4 control-label'>Data da Viagem (volta):</label>
            <div class='col-sm-8'>
                <input type='date' class='form-control input-date' id='pedi_viagem_volta_dia' name='pedi_viagem_volta_dia' required>
            </div>
        </div>
        <div class='form-group'>
            <label for='pedi_viagem_volta_valor_previsto' class='col-sm-4 control-label'>Valor da Viagem (volta):</label>
            <div class='col-sm-8'>
                <input type='number' min="0" class='form-control' id='pedi_viagem_volta_valor_previsto' name='pedi_viagem_volta_valor_previsto' required>
            </div>
        </div>
        <div class='form-group'>
            <label for='pedi_viagem_volta_transporte' class='col-sm-4 control-label'>Transporte (volta):</label>
            <div class='col-sm-8'>
                <select name="pedi_viagem_volta_transporte" id="pedi_viagem_volta_transporte" class="form-control">
                    <option value="Terrestre">Terrestre</option>
                    <option value="Carro Inst.">Carro da Instituição</option>
                    <option value="Carro Particular">Carro Particular</option>
                </select>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>
            <small class="text-muted">Justificativa</small>
        </legend>
        <div class="form-group">
            <div class="col-xs-12">
                <textarea name="pedi_justificativa" id="pedi_justificativa" class="form-control"></textarea>
            </div>
        </div>
    </fieldset>
</form>

<script type="text/javascript">
    $(function(){
        $('#pedi_tipo').change(function(){
            if ($(this).val() != 'Nead')
            {
                $('#fok_polo').removeAttr('disabled');
                $('label[for="fok_polo"]').removeClass('text-muted');
            }
            else
            {
                $('#fok_polo').attr('disabled','disabled');
                $('label[for="fok_polo"]').addClass('text-muted');
            }

            if ($(this).val() == 'Curso')
            {
                $('#uf').removeAttr('disabled');
                $('#fok_plano_disicplina').removeAttr('disabled');
                $('label[for="uf"]').removeClass('text-muted');
                $('label[for="fok_plano_disicplina"]').removeClass('text-muted');
            }
            else
            {
                $('#uf').attr('disabled','disabled');
                $('#fok_plano_disicplina').attr('disabled','disabled');
                $('label[for="uf"]').addClass('text-muted');
                $('label[for="fok_plano_disicplina"]').addClass('text-muted');
            }
        });

        $('#pedi_tipo').change();

        $('#pedi_diaria_quant, #fok_polo, #fok_plano_disicplina').change(function(){
            $.ajax({
                type: 'GET',
                url: '<?php echo base_url();?>pedidos/justificativa',
                data: {
                    polo : $('#fok_polo:not([disabled])').val() || 0,
                    curso : $('#uf:not([disabled])').val() || 0,
                    disciplina : $('#fok_plano_disicplina:not([disabled])').val() || 0,
                    diarias : $('#pedi_diaria_quant').val() || 0
                },
                dataType: 'json'
            })
            .done(function(msg){ // adaptado do crud.js
                if (msg)
                {
                    var input = $('.modal-dialog form [name="pedi_justificativa"]');
                    if (input.siblings('.help-block').length == 0) input.after('<span class="help-block">'+ msg.pedi_justificativa +'</span>');
                    else input.siblings('.help-block').text(msg.pedi_justificativa);
                }
            });
        });
    });
</script>
