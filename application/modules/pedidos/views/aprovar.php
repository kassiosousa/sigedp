<p>
    <?php
        $saldo_diarias_nead = $this->pedidos->saldo_diarias_nead();
        if ($saldo_diarias_nead < 0)
        {
            $excede = true;
            echo '<span class="text-danger">Esse pedido está <strong>excedendo</strong> o limite de diárias do NEAD</span>';
        }
        elseif (!empty($fok_polo))
        {
            $saldo_diarias_polo = $this->pedidos->saldo_diarias_polo($fok_polo);
            if ($saldo_diarias_polo < 0)
            {
                $excede = true;
                echo '<span class="text-danger">Esse pedido está <strong>excedendo</strong> o limite de diárias do POLO</span>';
            }
            else
            {
                if (!empty($curso['pmk_curso']))
                {
                    $saldo_diarias_curso = $this->pedidos->saldo_diarias_curso($curso['pmk_curso'], $fok_polo);
                    if ($saldo_diarias_curso < 0)
                    {
                        $excede = true;
                        echo '<span class="text-danger">Esse pedido está <strong>excedendo</strong> o limite de diárias do CURSO</span>';
                    }
                }
                if (empty($excede) && !empty($fok_plano_disicplina))
                {
                    $saldo_diarias_disciplina = $this->pedidos->saldo_diarias_disciplina($fok_plano_disicplina, $fok_polo);
                    if ($saldo_diarias_curso < 0)
                    {
                        $excede = true;
                        echo '<span class="text-danger">Esse pedido está <strong>excedendo</strong> o limite de diárias da DISCIPLINA</span>';
                    }
                }
            }
        }
        // após passar por toda a verificação
        if (empty($excede))
        {
            echo '<span class="glyphicon glyphicon-ok" style="color:green;"></span> A aprovação desse pedido não excede o limite de diárias ou o planejamento';
        }
    ?>
</p>
<div style="height:10px;"></div>
<div id="div_justificativa" class="form-group hide">
    <label for="justificativa" class="control-label col-xs-12" style="text-align:left;">Informe uma justificativa para rejeição</label>
    <div class="col-xs-12">
        <textarea name="justificativa" id="justificativa" class="form-control"></textarea>
    </div>
</div>
<div class="form-group clearfix;">
    <label for="pedi_status" class="pull-right" style="margin-right:15px;display:inline-block;">Ação a tomar:</label>
    <div style="float:right;height:1px;width:100%;"></div>
    <div class="pull-right" style="margin-right: 15px;">
        <select name="pedi_status" id="pedi_status" class="form-control" onchange="pedi_status_change();">
            <option value>- escolha uma ação -</option>
            <?php if(
                $this->session->userdata('usuatipo_nivel_acesso') < 2 ||
                ($this->session->userdata('usuatipo_nivel_acesso') == 2 && empty($excede)) ): ?>
            <option value="Aprovado">Aprovar</option>
            <?php else: ?>
            <option value disabled>somente o diretor pode aprovar</option>
            <?php endif; ?>
            <option value="Rejeitado">Rejeitar</option>
        </select>
    </div>
</div>

<script type="text/javascript">
    function pedi_status_change ()
    {
       if ($('#pedi_status').val() == 'Rejeitado') $('#div_justificativa').removeClass('hide');
       else $('#div_justificativa').addClass('hide');
    }
</script>
