<?php
    $relatorios_viagens = $this->pedidos->relatorios_viagens($pmk_pedido);
?>

<?php if(!empty($relatorios_viagens)): ?>
<p>Relatório de viagem:</p>
<?php endif; ?>
<?php foreach ($relatorios_viagens as $rel): ?>
<dl class="dl-horizontal">
    <dt>
        <a href="<?php echo base_url(); ?>assets/relatorios/<?php echo $rel['pedido_rel_anexo'] ?>" download>
            <?php echo $rel['pedido_rel_anexo']; ?>
        </a>
    </dt>
    <dd>
        Enviado em: <?php echo date('d/m/Y',strtotime($rel['pedido_rel_data'])); ?>
        <?php echo $rel['pedido_rel_texto']; ?>
    </dd>
</dl>
<?php endforeach; ?>

<?php if( $this->session->userdata('pmk_usuario') == $fok_usuario ): ?>

    <?php if (strtotime($pedi_viagem_volta_dia) >= strtotime("today")): ?>
    <div class="form-group" style="margin-left:0;margin-right:0;">
        <label for="relatorio_viagem">Enviar relatório de viagem: (PDF, Microsoft Word)</label>
        <input type="file" id="relatorio_viagem" name="relatorio_viagem">
    </div>
    <div class="form-group" style="margin-left:0;margin-right:0;">
        <label for="descricao_relatorio">Descrição do relatório de viagem:</label>
        <textarea name="descricao_relatorio" id="descricao_relatorio" class="form-control"></textarea>
    </div>
    <?php else: ?>
    <p>
        Você poderá enviar seu relatório de viagem a partir do dia
        <?php echo date('d/m/Y',strtotime($pedi_viagem_volta_dia)); ?>
    </p>
    <?php endif; ?>

<?php endif; ?>
