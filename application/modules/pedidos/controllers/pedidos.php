<?php
/* CRUD Controller for Pedidos */

class Pedidos extends CI_Controller {

    function __construct() {
        parent::__construct();
        //Carrega os modelos a serem utilizados
        $this->load->model('Pedidos/modelpedidos', 'pedidos');
		$this->load->model('Pedidos_relatorios_viagens/modelpedidos_relatorios_viagens', 'pedidos_relatorios_viagens');

        $this->load->helper('data');
    }

	function _remap($method){

		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }

		// chama a função de acordo com o método (method)
		switch( $method ) {

			default:
                // models extra necessários para montagem da view
                $this->load->model('usuarios/usuariosmodel', 'usuarios');
                $this->load->model('Polos/modelpolos', 'polos');

				// css e js exclusivos para essa página
                $templateExtras = array(
                    'styles' => array('select2.min.css', 'crud.css'),
                    'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
                );

				$this->template->load('templates/template_painel', 'index', $templateExtras);
			break;

			case 'criar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 5 || $this->session->userdata['usuatipo_nivel_acesso'] == 3)
                    {
                        // obs.: todos menos usuário (6) e secret adm (3)
						echo json_encode('Nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post) {

                            // ajusteszinhos input number p/ dinheiro $ -> R$ (tem que ser antes de dar load na validação)
                            $post['pedi_viagem_ida_valor_previsto'] = $_POST['pedi_viagem_ida_valor_previsto'] = str_replace(',', '.', $post['pedi_viagem_ida_valor_previsto']);
                            $post['pedi_viagem_ida_valor_previsto'] = $_POST['pedi_viagem_volta_valor_previsto'] = str_replace(',', '.', $post['pedi_viagem_volta_valor_previsto']);

							// usaremos o codeigniter para validar nosso input
							$this->load->library('form_validation');
							$this->lang->load('form_validation', 'portuguese'); // << traduções
                            $this->form_validation->set_message('data_valida', 'O campo %s não é uma data válida.');
                            $this->form_validation->set_message('tipo_valido', 'O campo %s foi preenchido com um valor inválido.');
                            $this->form_validation->set_message('justificativa_valida','A justificativa é requerida pois esse pedido irá EXCEDER o limite de diárias');

                            $this->form_validation->set_rules('pedi_tipo', 'Tipo', 'required|callback_tipo_valido');

                            if ($post['pedi_tipo'] != 'Nead')
                            {
                                $this->form_validation->set_rules('fok_polo', 'Polo', 'required|is_natural_no_zero');
                            }
                            if ($post['pedi_tipo'] == 'Nead')
                            {
                                unset($post['fok_polo']);
                                unset($post['fok_plano_disicplina']);
                            }
                            if ($post['pedi_tipo'] == 'Polo')
                            {
                                unset($post['fok_plano_disicplina']);
                            }
                            if ($post['pedi_tipo'] == 'Curso')
                            {
                                $this->form_validation->set_rules('fok_plano_disicplina', 'Disciplina', 'required|is_natural_no_zero');
                            }
                            $this->form_validation->set_rules('fok_usuario', 'Usuário', 'required|is_natural_no_zero');
                            $this->form_validation->set_rules('pedi_diaria_dia_inicial', 'Diária Inicial', 'callback_data_valida[pedi_diaria_dia_inicial]');
                            $this->form_validation->set_rules('pedi_diaria_quant', 'Diárias', 'required|is_natural');
                            $this->form_validation->set_rules('pedi_viagem_ida_dia', 'Viagem Ida', 'callback_data_valida[pedi_viagem_ida_dia]');
                            $this->form_validation->set_rules('pedi_viagem_ida_valor_previsto', 'Valor Ida', 'required|numeric');
                            $this->form_validation->set_rules('pedi_viagem_ida_transporte', 'Transporte Ida', 'required');
                            $this->form_validation->set_rules('pedi_viagem_volta_dia', 'Viagem Volta', 'callback_data_valida[pedi_viagem_volta_dia]');
                            $this->form_validation->set_rules('pedi_viagem_volta_valor_previsto', 'Valor Volta', 'required|numeric');
                            $this->form_validation->set_rules('pedi_viagem_volta_transporte', 'Transporte Volta', 'required');
                            $this->form_validation->set_rules('pedi_justificativa', 'Justificativa', 'callback_justificativa_valida');

							// testa o formulário com a validação informada
							$sucesso = $this->form_validation->run();

							if ($sucesso) {
                                // o input já foi validado, mas não alterado no caso de IE, Firefox, as linhas abaixo resolvem isso
                                $post['pedi_diaria_dia_inicial'] = valid_date_str($post['pedi_diaria_dia_inicial']);
                                $post['pedi_viagem_ida_dia'] = valid_date_str($post['pedi_viagem_ida_dia']);
                                $post['pedi_viagem_volta_dia'] = valid_date_str($post['pedi_viagem_volta_dia']);

                                // REGRA DE NEGÓCIO
                                if ($this->session->userdata('usuatipo_nivel_acesso') < 5)
                                {
                                    $post['pedi_status'] = 'Aguardando';
                                }
                                else // Secretário de Curso
                                {
                                    $post['pedi_status'] = 'Pendente';
                                }

                                unset($post['curso']);

                                $post['pedi_justificativa'] = trim($post['pedi_justificativa']);
                                if (!empty($post['pedi_justificativa']))
                                {
                                    $post['pedi_justificativa'] =
                                        date('d/m/Y H:i') . ' ' .
                                        $this->session->userdata('usua_nome') .
                                        " criou o pedido com a seguinte justificativa:\n" .
                                        $post['pedi_justificativa'];
                                }
                                else
                                {
                                    $post['pedi_justificativa'] =
                                        date('d/m/Y H:i') . ' ' .
                                        $this->session->userdata('usua_nome') .
                                        " criou o pedido";
                                }

								$this->pedidos->criar($post);
								echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
							}
							else
							{
								echo json_encode($this->form_validation->get_all_errors_array());
							}
						}
						// se foi um GET
						else
						{
							// renderiza o modal
                            $data = array();
                            $data['usuarios'] = $this->db->get_where('usuarios', array('usua_is_ativo' => 'Sim'))->result_array();
							$data['polos'] = $this->db->get_where('polos', array('polo_is_ativo' => 'Sim'))->result_array();
                            $data['cursos'] = $this->db->get_where('cursos', array('curso_is_ativo' => 'Sim'))->result_array();
                            $this->load->view('criar', $data);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'editar':
				// ajax
				if ($this->input->is_ajax_request())
				{
					// tipo de retorno: JSON
					header('Content-Type: application/json');

					// SEGURANÇA - Nível requerido
					if($this->session->userdata['usuatipo_nivel_acesso'] > 5) {
						echo json_encode('nivel de acesso insuficiente');
					} else {
						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post && $post['pmk_pedido'])
						{
                            $pedido = $this->pedidos->carregar($post['pmk_pedido']);
							/**
                             * AQUI COMEÇA O TRATAMENTO DAS DIVERSAS VIEWS QUE COMPOE O "AVALIAR PEDIDO"
                             * Primeiro é o seguinte:
                             *     Se o pedido está 'Aguardando'
                             *         Então se o cara for Coord Admin ou maior na hierarquia ele pode:
                             *         'Aprovar' ou 'Rejeitar' o pedido.
                             *         obs.: o Coord Admin só pode aprovar se o pedido estiver dentro do
                             *         planejamento, mas podemos não se preocupar com isso aqui no backend
                             *         porque essa segurança já é feita na view.
                            */
                            if ($pedido[0]['pedi_status'] == 'Aguardando')
                            {
                                if($this->session->userdata['usuatipo_nivel_acesso'] < 3)
                                {
                                    if ($post['pedi_status']) // << é permitido vir vazio
                                    {
                                        if ($post['pedi_status'] == 'Aprovado')
                                        {
                                            $post['justificativa'] =
                                                $pedido[0]['pedi_justificativa'] . "\n" .
                                                date('d/m/Y H:i') . ' ' .
                                                $this->session->userdata('usua_nome') .
                                                " aprovou o pedido";

                                            $this->pedidos->editar(array(
                                                'pmk_pedido' => $post['pmk_pedido'],
                                                'pedi_status' => $post['pedi_status'],
                                                'pedi_justificativa' => $post['justificativa']
                                            ));
                                            echo json_encode(false); // sucesso
                                        }
                                        elseif ($post['pedi_status'] == 'Rejeitado')
                                        {
                                            // rejeitado tem q ter justificativa
                                            $post['justificativa'] = trim($post['justificativa']);
                                            if (!empty($post['justificativa']))
                                            {
                                                $post['justificativa'] =
                                                    $pedido[0]['pedi_justificativa'] . "\n" .
                                                    date('d/m/Y H:i') . ' ' .
                                                    $this->session->userdata('usua_nome') .
                                                    " rejeitou o pedido com a seguinte justificativa:\n" .
                                                    $post['justificativa'];

                                                $this->pedidos->editar(array(
                                                    'pmk_pedido' => $post['pmk_pedido'],
                                                    'pedi_status' => $post['pedi_status'],
                                                    'pedi_justificativa' => $post['justificativa']
                                                ));
                                                echo json_encode(false); // sucesso
                                            }
                                            else
                                            {
                                                echo json_encode(array('justificativa' => 'Justificativa necessária'));
                                            }
                                        }
                                        else
                                        {
                                            echo json_encode(array('pedi_status' => 'Opção inválida'));
                                        }
                                    }
                                    else
                                    {
                                        echo json_encode(false); // se veio vazio apenas fecha o modal
                                    }
                                }
                                else
                                {
                                    echo json_encode(array('pedi_status' => 'Nível de acesso insuficiente'));
                                }
                            }
                            /*
                             * Se o pedido está 'Pendente'
                             *     então SOMENTE o Coord Curso pode colocar ele como 'aguardando' ou 'cancela-lo'
                            */
                            elseif ($pedido[0]['pedi_status'] == 'Pendente')
                            {
                                if($this->session->userdata['usuatipo_nivel_acesso'] == 4)
                                {
                                    if ($post['pedi_status']) // << é permitido vir vazio
                                    {
                                        if ($post['pedi_status'] == 'Aguardando')
                                        {
                                            $post['justificativa'] =
                                                $pedido[0]['pedi_justificativa'] . "\n" .
                                                date('d/m/Y H:i') . ' ' .
                                                $this->session->userdata('usua_nome') .
                                                " aceitou o pedido";

                                            $this->pedidos->editar(array(
                                                'pmk_pedido' => $post['pmk_pedido'],
                                                'pedi_status' => $post['pedi_status'],
                                                'pedi_justificativa' => $post['justificativa']
                                            ));
                                            echo json_encode(false); // sucesso
                                        }
                                        elseif ($post['pedi_status'] == 'Cancelado')
                                        {
                                            // rejeitado tem q ter justificativa
                                            $post['justificativa'] = trim($post['justificativa']);
                                            if (!empty($post['justificativa']))
                                            {
                                                $post['justificativa'] =
                                                    $pedido[0]['pedi_justificativa'] . "\n" .
                                                    date('d/m/Y H:i') . ' ' .
                                                    $this->session->userdata('usua_nome') .
                                                    " cancelou o pedido com a seguinte justificativa:\n" .
                                                    $post['justificativa'];

                                                $this->pedidos->editar(array(
                                                    'pmk_pedido' => $post['pmk_pedido'],
                                                    'pedi_status' => $post['pedi_status'],
                                                    'pedi_justificativa' => $post['justificativa']
                                                ));
                                                echo json_encode(false); // sucesso
                                            }
                                            else
                                            {
                                                echo json_encode(array('justificativa' => 'Justificativa necessária'));
                                            }
                                        }
                                        else
                                        {
                                            echo json_encode(array('pedi_status' => 'Opção inválida'));
                                        }
                                    }
                                    else
                                    {
                                        echo json_encode(false); // se veio vazio apenas fecha o modal
                                    }
                                }
                                else
                                {
                                    echo json_encode(array('pedi_status' => 'Nível de acesso insuficiente'));
                                }
                            }
                            /*
                             * Se o pedido já estiver 'Aprovado'
                             *     então a única ação possível é do Secr Adm que pode 'liberá-lo' informando os números de ofício
                            */
                            elseif ($pedido[0]['pedi_status'] == 'Aprovado')
                            {
                                if($this->session->userdata['usuatipo_nivel_acesso'] == 3)
                                {
                                    $post['oficio_diarias'] = trim($post['oficio_diarias']);
                                    $post['oficio_passagens'] = trim($post['oficio_diarias']);
                                    if (empty($post['oficio_diarias']))
                                    {
                                        echo json_encode(array('oficio_diarias' => 'Ofício diárias necessário'));
                                    }
                                    elseif (empty($post['oficio_passagens']))
                                    {
                                        echo json_encode(array('oficio_passagens' => 'Ofício passagens necessário'));
                                    }
                                    else
                                    {
                                        $post['justificativa'] =
                                            $pedido[0]['pedi_justificativa'] . "\n" .
                                            date('d/m/Y H:i') . ' ' .
                                            $this->session->userdata('usua_nome') .
                                            " liberou o pedido sob os seguintes números de ofício:\n" .
                                            "Ofício de diárias " . $post['oficio_diarias'] . "\n" .
                                            "Ofício de passagens " . $post['oficio_passagens'];

                                        $this->pedidos->editar(array(
                                            'pmk_pedido' => $post['pmk_pedido'],
                                            'pedi_status' => 'Liberado',
                                            'pedi_justificativa' => $post['justificativa']
                                        ));
                                        echo json_encode(false); // sucesso
                                    }
                                }
                                else
                                {
                                    echo json_encode(array('pedi_status' => 'Nível de acesso insuficiente'));
                                }
                            }
                            /*
                             * Se o pedido já estiver 'Liberado'
                             *     então quem viajou pode enviar um relatório
                            */
                            elseif ($pedido[0]['pedi_status'] == 'Liberado')
                            {
                                if ($pedido[0]['fok_usuario'] == $this->session->userdata('pmk_usuario'))
                                {
                                    if (strtotime($pedido[0]['pedi_viagem_volta_dia']) >= strtotime("today"))
                                    {
                                        $this->load->library('upload');
                                        $this->upload->initialize(array(
                                            'upload_path' => FCPATH.'assets/relatorios',
                                            'allowed_types' => 'pdf|doc|docx'
                                        ));
                                        if ( $this->upload->do_upload('relatorio_viagem') )
                                        {
                                            $this->load->model('pedidos_relatorios_viagens/modelPedidos_relatorios_viagens', 'relatorios_viagens');

                                            $this->relatorios_viagens->criar(array(
                                                'fok_pedido' => $post['pmk_pedido'],
                                                'pedido_rel_data' => date('Y-m-d'),
                                                'pedido_rel_texto' => $post['descricao_relatorio'],
                                                'pedido_rel_anexo' => $this->upload->file_name
                                            ));

                                            echo json_encode(false); // fecha modal
                                        }
                                        else
                                        {
                                            echo json_encode(array('relatorio_viagem' => $this->upload->error_msg[0]));
                                        }
                                    }
                                    else
                                    {
                                        echo json_encode(array('relatorio_viagem' => 'Você só poderá enviar seu relatório após a viagem'));
                                    }
                                }
                                else
                                {
                                    echo json_encode(array('relatorio_viagem' => 'Só o usuário da viagem pode enviar o relatório'));
                                }
                            }
                            else
                            {
                                echo json_encode(false); // fecha modal
                            }
						}
						// se foi um GET
						else {
                            // models extra necessários para montagem da view
                            $this->load->model('usuarios/usuariosmodel', 'usuarios');
                            $this->load->model('Polos/modelpolos', 'polos');

							$table = $this->pedidos->carregar($this->input->get('id'));
							// renderiza o modal
							$this->load->view('editar', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;

			case 'deletar':
				// ajax
				if ($this->input->is_ajax_request()) {
					// tipo de retorno: JSON
					header('Content-Type: application/json');

                    $table = $this->pedidos->carregar($this->input->get('id'));

					// SEGURANÇA - só quem criou && aguardano ou pendente
					if($table[0]['fok_usuario_criador'] != $this->session->userdata['pmk_usuario'] ||
                        ($table[0]['pedi_status'] != 'Aguardando' && $table[0]['pedi_status'] != 'Pendente')){
						echo json_encode('nivel de acesso insuficiente');
					} else {

						// pega todo o post já limpo de html/css/js (anti trolls)
						$post = $this->input->post(NULL, TRUE);

						// se foi um POST
						if ($post) {
							$this->pedidos->deletar($post['pmk_pedido']);
							echo json_encode(false); // << nenhum erro ocorreu (cadastro com sucesso)
						}
						// se foi um GET
						else
						{
							// renderiza o modal
							$this->load->view('deletar', $table[0]);
						}
					}
				} else {
					echo 'essa requisição precisa vir por ajax';
				}
			break;

            case 'disciplinas':
                // serve para o ajax do select2 - adaptado do select-cidade
                $_result = $this->pedidos->buscar_fok_plano_disciplina($this->input->get('estado'));
                $result = array();
                foreach ($_result as $disciplina)
                {
                    $d = new stdClass;
                    $d->id = $disciplina["pmk_plano_disciplina"];
                    $d->text = $disciplina["plano_disciplina_titulo"] . ' (semestre ' . $disciplina["plano_nr_semestre"] . ')';
                    $result[] = $d;
                }
                echo json_encode($result);
            break;

            case 'cursos':
                // serve para o ajax do select2 - adaptado do select-cidade
                $this->load->model('cursos/modelcursos', 'cursos');
                $_result = $this->cursos->buscar_por_polo($this->input->get('polo'));
                $result = array();
                foreach ($_result as $curso)
                {
                    $d = new stdClass;
                    $d->id = $curso["pmk_curso"];
                    $d->text = $curso["curso_titulo"];
                    $result[] = $d;
                }
                echo json_encode($result);
            break;

            case 'justificativa':
                // server para o ajax que checa obrigatoriedade da justificativa
                $polo = $this->input->get('polo');
                $curso = $this->input->get('curso');
                $disciplina = $this->input->get('disciplina');
                $diarias = $this->input->get('diarias');

                if (!empty($diarias))
                {
                    $saldo_diarias_nead = $this->pedidos->saldo_diarias_nead();
                    if ($saldo_diarias_nead - $diarias < 0)
                    {
                        echo json_encode(array('pedi_justificativa' =>
                            'A justificativa é requerida pois esse pedido irá EXCEDER o limite de diárias do NEAD'));
                        return;
                    }

                    if (!empty($polo))
                    {
                        $saldo_diarias_polo = $this->pedidos->saldo_diarias_polo($polo);
                        if ($saldo_diarias_polo - $diarias < 0)
                        {
                            echo json_encode(array('pedi_justificativa' =>
                                'A justificativa é requerida pois esse pedido irá EXCEDER o limite de diárias do POLO'));
                            return;
                        }

                        if (!empty($curso))
                        {
                            $saldo_diarias_curso = $this->pedidos->saldo_diarias_curso($curso, $polo);
                            if ($saldo_diarias_curso - $diarias < 0)
                            {
                                echo json_encode(array('pedi_justificativa' =>
                                    'A justificativa é requerida pois esse pedido irá EXCEDER o limite de diárias do CURSO'));
                                return;
                            }
                        }

                        if (!empty($disciplina))
                        {
                            $saldo_diarias_disciplina = $this->pedidos->saldo_diarias_disciplina($disciplina, $polo);
                            if ($saldo_diarias_curso - $diarias < 0)
                            {
                                echo json_encode(array('pedi_justificativa' =>
                                    'A justificativa é requerida pois esse pedido irá EXCEDER o limite de diárias da DISCIPLINA'));
                                return;
                            }
                        }
                    }

                    echo json_encode(array('pedi_justificativa' => 'A justificativa é opcional'));
                    return;
                }
                else
                {
                    echo json_encode(array('pedi_justificativa' => ''));
                    return;
                }
            break;
		}
	}

    function tipo_valido($str)
    {
        if ($str)
        {
            $validos = array('Curso','Polo');
            if ($this->session->userdata('usuatipo_nivel_acesso') < 2) $validos[] = 'Nead';

            if (in_array($str, $validos)) return true;
            else return false;
        }
        return true; // << pq já testamos 'required' e não queremos 2 erros
    }

    function data_valida($str, $name)
    {
        if ($str = valid_date_str($str)) return true;
        else return false;
    }

    function justificativa_valida($str)
    {
        $polo = $this->input->post('fok_polo');
        $curso = $this->input->post('curso');
        $disciplina = $this->input->post('fok_plano_disicplina');
        $diarias = $this->input->post('pedi_diaria_quant');
        $str = trim($str);

        if (!empty($diarias))
        {
            $saldo_diarias_nead = $this->pedidos->saldo_diarias_nead();
            if ($saldo_diarias_nead - $diarias < 0)
            {
                if (strlen($str) < 6) return false;
            }

            if (!empty($polo))
            {
                $saldo_diarias_polo = $this->pedidos->saldo_diarias_polo($polo);
                if ($saldo_diarias_polo - $diarias < 0)
                {
                    if (strlen($str) < 6) return false;
                }

                if (!empty($curso))
                {
                    $saldo_diarias_curso = $this->pedidos->saldo_diarias_curso($curso, $polo);
                    if ($saldo_diarias_curso - $diarias < 0)
                    {
                        if (strlen($str) < 6) return false;
                    }
                }

                if (!empty($disciplina))
                {
                    $saldo_diarias_disciplina = $this->pedidos->saldo_diarias_disciplina($disciplina, $polo);
                    if ($saldo_diarias_curso - $diarias < 0)
                    {
                        if (strlen($str) < 6) return false;
                    }
                }
            }

            return true;
        }
        return false;
    }
}

