<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="SIGEDP - Sistema gestor de diárias e passagens">
		<meta name="author" content="Kássio Sousa, Júlio Filho">
		<meta name="keyword" content="Passagens, Diarias, Nead, PHP">
		<!-- <link rel="shortcut icon" href="TODO"> -->

		<title>SIGEDP - ACESSO</title>

		<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

		<style type="text/css">
			body{
				background-color: #f2f2f2;
				margin-top: 100px;
			}
		</style>
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<?php echo $contents; ?>
		
		<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/perfil.js"></script>

	</body>
</html>

