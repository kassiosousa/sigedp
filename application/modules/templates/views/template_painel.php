<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="SIGEDP - Sistema gestor de diárias e passagens">
		<meta name="author" content="Kássio Sousa, Júlio Filho">
		<meta name="keyword" content="Passagens, Diarias, Nead, PHP">
		<!-- <link rel="shortcut icon" href="TODO"> -->

		<title>SIGEDP</title>

		<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/sb-admin.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/plugins/morris.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
			.nav>li.disabled>a:focus, .nav>li.disabled>a:hover
			{
				background-color: transparent !important;
			}
        </style>
        <?php if(isset($styles) && count($styles) > 0): ?>
        <?php foreach ($styles as $style): ?>
        <link href="<?php echo base_url(); ?>assets/css/<?php echo $style; ?>" rel="stylesheet">
    	<?php endforeach; ?>
    	<?php endif;?>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Barra de Navegação</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo base_url(); ?>">SIGEDP</a>
				</div>
				<ul class="nav navbar-right top-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
						<ul class="dropdown-menu message-dropdown">
							<li class="message-preview">
								<a href="#">
									<div class="media">
										<span class="pull-left">
											<img class="media-object" src="http://pp.vk.me/c625529/v625529697/5440/4OSeZmzlTns.jpg" alt="">
										</span>
										<div class="media-body">
											<h5 class="media-heading"><strong>Notificação com Figura</strong>
											</h5>
											<p class="small text-muted"><i class="fa fa-clock-o"></i> Ontem às 16:32</p>
											<p>Ahh que delícia cara!</p>
										</div>
									</div>
								</a>
							</li>
							<li class="message-preview">
								<a href="#">
									<div class="media">
										<div class="media-body">
											<h5 class="media-heading"><strong>Notificação sem Figura</strong>
											</h5>
											<p class="small text-muted"><i class="fa fa-clock-o"></i> 20 de agosto às 08:03</p>
											<p>Sem figura cabe mais texto e dispensa "ícones padrão" para os tipos de notificações.</p>
										</div>
									</div>
								</a>
							</li>
							<li class="message-footer">
								<a href="#">Ver todas as notificações</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata('usua_nome'); ?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo base_url(); ?>home/perfil"><i class="fa fa-fw fa-user"></i> Minha Conta</a>
							</li>
							<li>
								<a href="#"><i class="fa fa-fw fa-bell"></i> Notificações</a>
							</li>
							<li>
								<a href="#"><i class="fa fa-fw fa-gear"></i> Configurações</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo base_url(); ?>login/logout"><i class="fa fa-fw fa-power-off"></i> Sair</a>
							</li>
						</ul>
					</li>
				</ul>

				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav side-nav">
						<?php
							// Controle front-end de acesso
							$menus = array(
								'home' =>
									array(
											'nome' => 'Painel Principal',
											'icone' => 'dashboard',
											'nivel_acesso' => 6
										),
								'usuarios' =>
									array(
											'nome' => 'Usuários',
											'icone' => 'users',
											'nivel_acesso' => 2
										),
								'cursos' =>
									array(
											'nome' => 'Cursos',
											'icone' => 'book',
											'nivel_acesso' => 2
										),
								'cursos_contas' =>
									array(
											'nome' => 'Contas de Cursos',
											'icone' => 'usd',
											'nivel_acesso' => 2
										),
								'planos_pedagogicos' =>
									array(
											'nome' => 'Plano Pedagógico',
											'icone' => 'building',
											'nivel_acesso' => 2
										),
								'polos' =>
									array(
											'nome' => 'Polos',
											'icone' => 'building',
											'nivel_acesso' => 2
										),
                                'pedidos' =>
                                    array(
                                            'nome' => 'Pedidos',
                                            'icone' => 'suitcase',
                                            'nivel_acesso' => 6
                                        ),
								'relatorios' =>
                                    array(
                                            'nome' => 'Relatórios',
                                            'icone' => 'pie-chart',
                                            'nivel_acesso' => 2
                                        ),
								'configuracoes' =>
									array(
											'nome' => 'Configurações',
											'icone' => 'gear',
											'nivel_acesso' => 2
										),
							);

							foreach ($menus as $url => $values)
							{
								$menus[$url]['classes'] = '';
								if ($this->uri->segment(1) == $url) $menus[$url]['classes'] .= 'active ';
								if ($this->session->userdata('usuatipo_nivel_acesso') > $values['nivel_acesso']) $menus[$url]['classes'] .= 'disabled ';
							}
						?>
						<?php foreach ($menus as $url => $values): ?>
						<li class="<?php echo $values['classes']; ?>">
							<a href="<?php echo base_url($url); ?>"><i class="fa fa-fw fa-<?php echo $values['icone']; ?>"></i> <?php echo $values['nome']; ?></a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</nav>

			<div id="page-wrapper">
				<div class="container-fluid">
                    <?php echo $contents; ?>
				</div>
			</div>
		</div>

		<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            // fix height by julio
            if ( $(window).width() > 768 )
            {
                var h = $(window).height() - $('nav.navbar-fixed-top').height();
                $('#page-wrapper').css('min-height', h + "px" );
            }
            // fix disabled nav itens by julio
            $('.nav>li.disabled>a').click(function(event){ event.preventDefault(); });
        </script>
		<?php if(isset($scripts) && count($scripts) > 0): ?>
		<?php foreach ($scripts as $script): ?>
		<script src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
		<?php endforeach; ?>
		<?php endif;?>
	</body>

</html>

