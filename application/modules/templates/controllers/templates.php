<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates extends CI_Controller {
	
	function _remap($method){ 
		
		switch( $method ) {
		
			case 'templates_login':
				$this->load->view('templates_login');
			break;
			
			case 'templates_painel':
				$this->load->view('templates_painel');
			break;
			
			case '404':
				$this->load->view('404');
			break;
		}
	}
}