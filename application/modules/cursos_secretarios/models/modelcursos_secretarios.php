<?php
/* Model that represents Cursos_secretarios at database */

class ModelCursos_secretarios extends CI_Model {
	
	public function carregar ($idTable){
		$this->db->select('*');
		$this->db->from('cursos_secretarios tabela');
		$this->db->where('tabela.pmk_curso_secretario', $idTable);
		$this->db->where('tabela.curso_secretario_is_ativo', 'Sim');
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar($tableParam = ''){

		$where = array('tabela.curso_secretario_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_curso_secretario'])){ $where += array('tabela.pmk_curso_secretario' => $tableParam['pmk_curso_secretario']); }
		if(isset($tableParam['fok_curso'])){ $where += array('tabela.fok_curso' => $tableParam['fok_curso']); }
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }
		
		$this->db->select('*');
		$this->db->from('cursos_secretarios tabela');
		$this->db->join('cursos', 'cursos.pmk_curso = tabela.fok_curso');
		$this->db->join('usuarios', 'usuarios.pmk_usuario = tabela.fok_usua_secretario');
		$this->db->where($where); 
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar($tableParam) { 
		if (isset($tableParam)){
			$this->db->trans_start();
			$this->db->insert('cursos_secretarios',	$tableParam);
			$insert_id = $this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			$this->db->trans_complete();	
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function editar ($tableParam) {
	
		if (isset($tableParam)){
			$idTable = $tableParam['pmk_curso_secretario'];
			
			$this->db->where('pmk_curso_secretario', $idTable);
			$this->db->set($tableParam);
			
			return $this->db->update('cursos_secretarios');
		} else {
			return false;
		}
	}
	
	public function deletar ($idTable) {
		$tableParam['curso_secretario_is_ativo'] = 'Nao';
		$tableParam['pmk_curso_secretario'] = $idTable;
		
		$this->db->where('pmk_curso_secretario', $idTable);
		$this->db->set($tableParam);
		
		return $this->db->update('cursos_secretarios');
	}
	
	public function deletar_multiplos ($fok_curso) {
		
		// Para deletar muitos dados em escala
		$this->db->where('fok_curso', $fok_curso);
		$this->db->delete('cursos_secretarios');
		if ($this->db->affected_rows()){
			return true;
		} else { return false; }
	}
}
