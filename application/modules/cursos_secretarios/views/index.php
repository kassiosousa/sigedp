
<?php
	$tableList = $this->cursos_secretarios->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Cursos_secretarios<small class='hidden-xs'><br>Lista de Cursos_secretarios cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>cursos_secretarios/criar' data-header='Cadastrar Cursos_secretarios'>
				<i class='fa fa-plus'></i> &nbsp;Criar Cursos_secretarios
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>cursos_secretarios'><i class='fa fa-users'></i> Cursos_secretarios</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th><i class='icon_profile'></i> Pmk_curso_secretario</th>
<th><i class='icon_profile'></i> Fok_curso</th>
<th><i class='icon_profile'></i> Fok_usua_secretario</th>
<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_curso_secretario']; ?></td>
<td><?php echo $lista['fok_curso']; ?></td>
<td><?php echo $lista['fok_usua_secretario']; ?></td>
<td class='text-center acoes'>
					<div class='btn-group'>
						<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>cursos_secretarios/editar/?id=<?php echo $lista['pmk_curso_secretario'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
						<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>cursos_secretarios/deletar/?id=<?php echo $lista['pmk_curso_secretario'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
					</div>
				</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Pmk_curso_secretario</th>
<th><i class='icon_profile'></i> Fok_curso</th>
<th><i class='icon_profile'></i> Fok_usua_secretario</th>
<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

