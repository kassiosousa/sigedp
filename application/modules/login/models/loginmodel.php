<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loginmodel extends CI_Model  {

	// ================ LOGIN ===================
	public function verifica_login($usua_cpf, $dsPassword){
		//Field validation succeeded.  Validate against database
		$result = $this->logar($usua_cpf, $dsPassword);
		
		if($result) {
			$dadossessao  = array();
			foreach($result as $row) {
				$dadossessao  = array(
				'pmk_usuario' => $row->pmk_usuario,
				'fok_usuariotipo' => $row->fok_usuariotipo,
				'usua_email' => $row->usua_email,
				'usua_nome' => $row->usua_nome,
				'usuatipo_nivel_acesso' => $row->usuatipo_nivel_acesso,
				'base_url' => base_url()
				);
				$this->session->set_userdata($dadossessao);
			}
			return true;
		} else {
			return false;
		}
	}
	
    # LOGIN USUÁRIO
	public function logar($usua_cpf, $dsPassword) {
	
		$this->db->select('*');
		$this->db->from('usuarios user');
		$this->db->join('usuarios_tipos usuatipo', 'user.fok_usuariotipo = usuatipo.pmk_usuariotipo');
		$this->db->where('usua_cpf', $usua_cpf);
		$this->db->where('usua_senha', MD5($dsPassword));
		$this->db->where('usua_is_ativo', 'Sim');
		$this->db->limit(1);

		$query = $this->db->get();
				
		if($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
	
	public function session_criar() { 
		if ($this->session->all_userdata() ){
			$fieldsSessao = $this->session->all_userdata();
			
			$fields['pmk_usuario'] = $this->session->userdata('pmk_usuario');
			$fields['fok_usuariotipo'] = $fieldsSessao['fok_usuariotipo'];
			$fields['session_id'] = $fieldsSessao['session_id'];
			$fields['ip_address'] = $fieldsSessao['ip_address'];
			$fields['user_agent'] = $fieldsSessao['user_agent'];
			$fields['usua_email'] = $fieldsSessao['usua_email'];
			$fields['usua_nome'] = $fieldsSessao['usua_nome'];
			$fields['usuatipo_nivel_acesso'] = $fieldsSessao['usuatipo_nivel_acesso'];
			$fields['base_url'] = $fieldsSessao['base_url'];
			
			$this->db->insert('sys_sessions',$fields);
			$insert_id = $this->db->insert_id();
			
			return $insert_id;
		} else {
			return false;
		}
	}
	
	public function session_carregar($pmk_usuario) {
	
		$this->db->select('*');
		$this->db->from('sys_sessions');
		$this->db->where('pmk_usuario', $pmk_usuario);
		$this->db->limit(1,1);
		$this->db->order_by("pmk_sys_sessions", "DESC"); 

		$query = $this->db->get();
				
		if($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
	
	public function logout(){
		session_start();
        $this->session->sess_destroy();
		$_SESSION = array();
		session_destroy();
        redirect('login', 'refresh');
    }
	
	// CONTADORES
	public function contador_usuarios (){
		$this->db->select('count(pmk_usuario) as qtdUsuarios');
		$this->db->from('usuarios usua');
		$this->db->where('usua.usua_is_ativo', "Sim");
		
		$this->db->limit(1);
		$query = $this->db->get();

		if($query->num_rows() == 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}

//============================================================================================================================================

	
}