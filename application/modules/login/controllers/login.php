<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function _remap($method){ 
		//Carrega os modelos a serem utilizados
		$this->load->model('login/loginmodel', 'login');
		$this->load->model('usuarios/usuariosmodel', 'usuarios');
		
		switch( $method ) {
		
			case 'logout':
				$this->login->logout();
			break;
		
			default:
				// SEGURANÇA - Verifica se já tem login ativo e manda ao painel
				if ($this->session->userdata) { 
					if ( 
						(array_key_exists("usuatipo_nivel_acesso", $this->session->userdata))&&
						($this->session->userdata('base_url') == base_url()) 
					) { 
						redirect('home/', 'refresh'); 
					}
				} 
				
				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('select2.min.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js')
				);
				// Senão, carrega página de login
				$this->template->load('templates/template_login', 'index', $templateExtras);
			break;
			
			case 'logar':
				
				// Recebe os valores vindos do formulário (São obrigatórios)
				if ($_POST) {
					$dsSenha = $_POST['fields']['dsSenha'];
					$usua_cpf = $_POST['fields']['usua_cpf'];
					
					// usaremos o codeigniter para validar nosso input
					$this->load->library('form_validation');
					$this->lang->load('form_validation', 'portuguese'); // << traduções
					// regras
					$this->form_validation->set_rules('usua_senha', 'Senha', 'required|min_length[3]');
					$this->form_validation->set_rules('usua_cpf', 'CPF', 'exact_length[14]');
					// a validação de data é mais coisada
					$this->load->helper('data');
							
					// Testa se existe ou não este usuário
					if ($this->login->verifica_login($usua_cpf, $dsSenha)) {
						// Se fez o login corretamente, faz a inserção da sessão
						$this->login->session_criar();
						//Go to private area
						redirect('login', 'refresh');
					} else {
						//Field validation failed.  User redirected to login page
						// Joga na sessão a mensagem de erro
						$this->session->set_flashdata('form_erro', 'Usuário ou Senha inválido');
						redirect('login', 'refresh');
					}				
				} else {
					//Devolve ao início
					redirect('login', 'refresh');
				}
			break;
			
			case 'recover':
			
				// Recebe os valores vindos do formulário (São obrigatórios)
				if (isset($_POST['fields']['dsEmail'])){
					$mailUsuario = $_POST['fields']['dsEmail'];
					$usuarioPorEmail = $this->usuarios->carregar_por_email($mailUsuario);
						
					//	Se tiver, manda um email com link para que ele possa alterar a senha
					if ($usuarioPorEmail) {
						$emailsender = "contato@sigedp.com.br";
						$assunto = $usuarioPorEmail[0]['usua_nome']." -- ".$mailUsuario;
						$mensagemHTML = "Voce deseja alterar sua senha no SIGEDP? Basta acessar o link abaixo que a nova senha será gerada.\n";
						$mensagemHTML .= "Você receberá um email com a nova senha logo em seguida.\n";
						// A chave é o código do usuário em md5
						$mensagemHTML .= base_url()."/login/alterar_senha?k=".MD5($usuarioPorEmail[0]['pmk_usuario']); 
						$maildestiny = $mailUsuario;
							$headers = "MIME-Version: 1.1\r\n";
							$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
							$headers .= "From: ".$emailsender."\r\n"; // remetente
							$headers .= "Return-Path: ".$maildestiny."\r\n"; // return-path
							$headers .= $assunto."\r\n"; // Assunto
		
						if (mail($maildestiny, "Recuperação de senha", $mensagemHTML, $headers)) { 
							// Sucesso 
							$this->session->set_flashdata('form_sucesso','Email para recuperar senha foi enviado com sucesso');
							redirect('login', 'refresh');
						} else {	
							$this->session->set_flashdata('form_erro','Erro ao enviar email');
							redirect('login', 'refresh');
						}
					} else {
						// 	Se não tiver, avisa que o email é inválido
						$this->session->set_flashdata('form_erro','Email inválido, tente novamente!');
						redirect('login/recover', 'refresh');
					}
				}
				$this->template->load('templates/template_login', 'recover');
			break;
			
			case 'alterar_senha':
			
				if (isset($_GET['k'])) {
					$key = $_GET['k'];
					// Procura usuário que tenha este código registrado
					$usuariosSistema = $this->usuarios->listar();
					$usuarioCarregado = null;
					if ($usuariosSistema) {
						foreach ($usuariosSistema as $usuario) {
							$codMD5 = MD5($usuario['pmk_usuario']);
							if ($codMD5 == $key) {
								$usuarioCarregado = $usuario;
							}
						}
					}
					
					//	Se tiver, gera nova senha, coloca na conta dele e envia email
					if ($usuarioCarregado) {
						$novaSenha = rand(1,1000)."".rand(1,1000)."".rand(1,1000)."".rand(1,1000)."".rand(1,1000);
						// Altera senha do usuário
						$usuaParam = $usuarioCarregado;
						$usuaParam['usua_senha'] = MD5($novaSenha);
						$idUsuario = $this->usuarios->editar($usuaParam);
						
						if ($idUsuario) { // Se editou a senha, manda email
							$mailUsuario = $usuarioCarregado['usua_email'];
							$emailsender = "contato@sigedp.com.br";
							$assunto = $usuarioCarregado['usua_nome']." -- ".$mailUsuario;
							$mensagemHTML = "A seguir, está sua nova senha, gerada de modo automático, ";
							$mensagemHTML .= "utilize para acessar o sistema SIGEDP e modifique para uma de sua preferência.\n";
							$mensagemHTML .= "Nova senha: ".$novaSenha;
							$maildestiny = $mailUsuario;
								$headers = "MIME-Version: 1.1\r\n";
								$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
								$headers .= "From: ".$emailsender."\r\n"; // remetente
								$headers .= "Return-Path: ".$maildestiny."\r\n"; // return-path
								$headers .= $assunto."\r\n"; // Assunto
			
							if (mail($maildestiny, "Recuperação de senha", $mensagemHTML, $headers)) { 
								// Sucesso 
								$this->session->set_flashdata('form_sucesso','Foi enviado um email com sua nova senha!');
								redirect('login', 'refresh');
							} else {	
								$this->session->set_flashdata('form_erro','Erro ao enviar email de recuperação, tente novamente.');
								redirect('login', 'refresh');
							}
						}
						// 	Se não editou
						$this->session->set_flashdata('form_erro','Senha não alterada!');
						redirect('login', 'refresh');
					} else {
						// 	Se não tiver, avisa que o email é inválido
						$this->session->set_flashdata('form_erro','Email inválido, tente novamente!');
						redirect('login/recover', 'refresh');
					}
				}
			break;
			
			case 'contato':
				if ($_POST) {
					$emailsender = $_POST['email'];
					$assunto = $_POST['name']." -- ".$_POST['email'];
					$mensagemHTML = $_POST['message'];
					$maildestiny = "walberpontes@gmail.com";
						$headers = "MIME-Version: 1.1\r\n";
						$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
						$headers .= "From: ".$emailsender."\r\n"; // remetente
						$headers .= "Return-Path: ".$maildestiny."\r\n"; // return-path
						$headers .= $assunto."\r\n"; // Assunto
	
					if (mail($maildestiny, "Contato via formulário - REQUALI", $mensagemHTML, $headers)) { 
						// Sucesso 
						redirect('site/?dsResult=Sucesso', 'refresh');
					} else {	
						redirect('site/?dsResult=Erro', 'refresh');
					}
				}
				$this->index();
			break;
			
		}
	}

}
