<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<?php if ($this->session->flashdata('form_erro')){ ?>
					<div class="alert alert-block alert-danger fade in">
						<?php echo $this->session->flashdata('form_erro'); ?>
					</div>
				<?php } ?>
				<div class="panel-heading">
					<h3 class="panel-title">Login</h3>
				</div>
				<div class="panel-body">
					<form class="login-form" method="post" action="<?php echo base_url(); ?>login/logar">
						<fieldset>
							<div class="form-group">
								<input class="form-control" name="fields[usua_cpf]" name="usua_cpf" type="text" placeholder="000.000.000-00" data-mask="999.999.999-99">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Senha" name="fields[dsSenha]" name="password" type="password" value="">
							</div>
							<div class="checkbox">
								<label>
									<!-- <input name="remember" type="checkbox" value="Remember Me">Remember Me -->
									<a href="<?php echo base_url(); ?>login/recover">Esqueci a senha</a>
								</label>
							</div>
							<!-- Change this to a button or input when using this as a form -->
							<button type="submit" class="btn btn-lg btn-success btn-block">Login</a>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
