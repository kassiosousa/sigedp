<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<?php if ($this->session->flashdata('form_erro')){ ?>
					<div class="alert alert-block alert-danger fade in">
						<?php echo $this->session->flashdata('form_erro'); ?>
					</div>
				<?php } ?>
				<div class="panel-heading">
					<h3 class="panel-title">Recuperar Senha</h3>
				</div>
				<div class="panel-body">
					<form class="login-form" method="post" action="<?php echo base_url(); ?>login/recover">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Email" name="fields[dsEmail]" name="email" type="email" autofocus="">
							</div>
							<div class="checkbox">
								<a href="<?php echo base_url(); ?>login">Voltar</a>
							</div>
							<!-- Change this to a button or input when using this as a form -->
							<button type="submit" class="btn btn-lg btn-success btn-block">Recuperar</a>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

