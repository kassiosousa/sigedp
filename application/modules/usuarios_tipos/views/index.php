
<?php
	$tableList = $this->usuarios_tipos->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Usuarios_tipos<small class='hidden-xs'><br>Lista de Usuarios_tipos cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>usuarios_tipos/criar' data-header='Cadastrar Usuarios_tipos'>
				<i class='fa fa-plus'></i> &nbsp;Criar Usuarios_tipos
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>usuarios_tipos'><i class='fa fa-users'></i> Usuarios_tipos</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							<th><i class='icon_profile'></i> Pmk_usuariotipo</th>
<th><i class='icon_profile'></i> Usuatipo_nivel_acesso</th>
<th><i class='icon_profile'></i> Usuatipo_nome</th>
<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
							if ($tableList) {
								foreach ($tableList as $lista) { ?>
								<tr>
									<td><?php echo $lista['pmk_usuariotipo']; ?></td>
<td><?php echo $lista['usuatipo_nivel_acesso']; ?></td>
<td><?php echo $lista['usuatipo_nome']; ?></td>
<td class='text-center acoes'>
					<div class='btn-group'>
						<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>usuarios_tipos/editar/?id=<?php echo $lista['pmk_usuariotipo'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
						<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>usuarios_tipos/deletar/?id=<?php echo $lista['pmk_usuariotipo'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
					</div>
				</td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th><i class='icon_profile'></i> Pmk_usuariotipo</th>
<th><i class='icon_profile'></i> Usuatipo_nivel_acesso</th>
<th><i class='icon_profile'></i> Usuatipo_nome</th>
<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

