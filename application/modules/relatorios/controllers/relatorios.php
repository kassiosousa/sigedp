<?php
/* CRUD Controller for Relatorios */

class Relatorios extends CI_Controller {

	function _remap($method){
		//Carrega os modelos a serem utilizados
		$this->load->model('Relatorios/modelrelatorios', 'relatorios');
		$this->load->model('usuarios/usuariosmodel', 'usuarios');
		$this->load->model('Configuracoes/modelconfiguracoes', 'configuracoes');
		$this->load->model('Pedidos/modelpedidos', 'pedidos');
		$this->load->model('Polos/modelpolos', 'polos');
		$this->load->model('Cursos/modelcursos', 'cursos');

		// Teste de sessão ativa para acessar este módulo
		if (isset($this->session->userdata)) {
			if (!(array_key_exists('usuatipo_nivel_acesso', $this->session->userdata))) {
				redirect('login', 'refresh');
			}
		} else { redirect('login', 'refresh'); }

		// chama a função de acordo com o método (method)
		switch( $method ) {

			default:
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('select2.min.css', 'crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'RelListaPedidos', $templateExtras);
			break;
			
			case "RelListaUsuarios":
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('select2.min.css', 'crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'RelListaUsuarios', $templateExtras);
			break;
			
			case "RelFinanceiro":
				// SEGURANÇA - Level de acesso
				if($this->session->userdata['usuatipo_nivel_acesso'] > 2) { redirect('home', 'refresh'); }

				// css e js exclusivos para essa página
				$templateExtras = array(
					'styles' => array('select2.min.css', 'crud.css'),
					'scripts' => array('bootbox.min.js', 'jquery.maskedinput.js', 'select2.min.js', 'crud.js')
				);

				$this->template->load('templates/template_painel', 'RelFinanceiro', $templateExtras);
			break;


		}
	}
}

