<?php
	$polosArr = $this->polos->listar();
	$cursosArr = $this->cursos->listar();
	$usuariosArr = $this->usuarios->listar();
	
	// Valores padrão
	$fields=[];
	if (isset($_POST['fields']['pedi_tipo']) && $_POST['fields']['pedi_tipo'] != "0"){ 
		$fields['pedi_tipo'] = $_POST['fields']['pedi_tipo']; 
	}
	if ( (isset($_POST['fields']['fok_usuario'])) && $_POST['fields']['fok_usuario'] != 0){ 
		$fields['fok_usuario'] = $_POST['fields']['fok_usuario']; 
	}
	if ( (isset($_POST['fields']['fok_polo'])) && $_POST['fields']['fok_polo'] != 0){ 
		$fields['fok_polo'] = $_POST['fields']['fok_polo']; 
	}
	if ( (isset($_POST['fields']['pedi_status'])) && $_POST['fields']['pedi_status'] != "0"){ 
		$fields['pedi_status'] = $_POST['fields']['pedi_status']; 
	}
	if (isset($_POST['fields']['data_inicio']) && $_POST['fields']['data_inicio'] != ""){ $fields['data_inicio'] = $_POST['fields']['data_inicio']; }
	if (isset($_POST['fields']['data_fim']) && $_POST['fields']['data_fim'] != ""){ $fields['data_fim'] = $_POST['fields']['data_fim']; }
	
	$pedidosRealizados = $this->relatorios->listar_financeiro($fields);

?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>Relatórios<small class='hidden-xs'><br>Financeiro</small></h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>relatorios'><i class='fa fa-users'></i> Lista de Pedidos</a></li>
			<li><a href='<?php echo base_url();?>relatorios/RelFinanceiro'><i class='fa fa-users'></i> Financeiro</a></li>
			<li><a href='<?php echo base_url();?>relatorios/RelListaUsuarios'><i class='fa fa-users'></i> Lista de Usuários</a></li>
			<li class='active'>Listar</li>
		</ol>
	</div>
</div>

<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Relatório Financeiro</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
			
				<div class="row">
				
					<div class="col-sm-12">
						<div class="dataTables_length" id="dataTables-example_length">
							<form method="post" action="" >
								<label>Início <input <?php if (!empty($fields['data_inicio'])){?>value="<?php echo $fields['data_inicio']; ?>"<?php } ?> type="date" class="form-control input-sm" name="fields[data_inicio]" type=""></label>
								<label>Fim <input <?php if (!empty($fields['data_fim'])){?>value="<?php echo $fields['data_fim']; ?>"<?php } ?>  type="date" class="form-control input-sm" name="fields[data_fim]" type=""></label>
								<label>Usuário 
									<select type="date" class="form-control input-sm" name="fields[fok_usuario]">
										<option <?php if (isset($fields['fok_usuario']) && $fields['fok_usuario'] == 0) { echo "selected"; } ?> value="0">Todos</option>
										<?php 
										if($usuariosArr) {
											foreach ($usuariosArr as $usuario) {
										?>
										<option <?php if ((isset($fields['fok_usuario'])) && $fields['fok_usuario']==$usuario['pmk_usuario']) { echo "selected"; } ?>  value="<?php echo $usuario['pmk_usuario'];?>"><?php echo $usuario['usua_nome'];?></option>
										<?php
											}
										}
										?>
									</select>
								</label>
								<label>Polo 
									<select type="date" class="form-control input-sm" name="fields[fok_polo]">
										<option <?php if (isset($fields['fok_polo']) && $fields['fok_polo'] == 0) { echo "selected"; } ?> value="0">Todos</option>
										<?php 
										if($polosArr) {
											foreach ($polosArr as $polo) {
										?>
										<option <?php if ((isset($fields['fok_polo'])) && $fields['fok_polo']==$polo['pmk_polo']) { echo "selected"; } ?>  value="<?php echo $polo['pmk_polo'];?>"><?php echo $polo['polo_titulo'];?></option>
										<?php
											}
										}
										?>
									</select>
								</label>
								<label><button type="subtmi" value="Filtrar">Filtrar</button></label>
							</form>
						</div>
					</div>
					
				</div>
				
				<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th  style="text-align:center;" colspan="10">Pedido</th>
							<th  style="text-align:center;" colspan="3">Valores</th>
						</tr>
						<tr>
						
							<th style="width: 50px"><i class='icon_profile'></i> Cód.</th>
							<th><i class='icon_profile'></i> Tipo</th>
							<th><i class='icon_profile'></i> Polo</th>
							<th><i class='icon_profile'></i> Qtd. Diárias</th>
							<th><i class='icon_profile'></i> Status</th>
							
							<th><i class='icon_profile'></i> Diária Inicial</th>
							<th><i class='icon_profile'></i> Viagem Ida</th>
							<th><i class='icon_profile'></i> Transporte Ida</th>
							<th><i class='icon_profile'></i> Viagem Volta</th>
							<th><i class='icon_profile'></i> Transporte Volta</th>
							
							<th><i class='icon_profile'></i> Gasto Diárias</th>
							<th><i class='icon_profile'></i> Viagem Ida</th>
							<th><i class='icon_profile'></i> Viagem Volta</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$totalViagemIda = 0;
							$totalViagemVolta = 0;
							$totalDiarias = 0;
							$totalGastoDiarias = 0;
							if ($pedidosRealizados) {
								foreach ($pedidosRealizados as $pedido) { 
									$poloField = $this->polos->carregar($pedido['fok_polo']);
									$totalViagemIda += $pedido['pedi_viagem_ida_valor_previsto'];
									$totalViagemVolta += $pedido['pedi_viagem_volta_valor_previsto'];
									$totalDiarias += $pedido['pedi_diaria_quant'];
									$totalGastoDiarias += $pedido['pedi_diaria_quant'] * $pedido['curso_conta_vr_diaria'];
								?>
								<tr>
								
									<td><?php echo $pedido['pmk_pedido']; ?></td>
									<td><?php echo $pedido['pedi_tipo']; ?></td>
									<td><?php echo $poloField[0]['polo_titulo']; ?></td>
									<td><?php echo $pedido['pedi_diaria_quant']; ?></td>
									<td><?php echo $pedido['pedi_status']; ?></td>
									
									<td><?php $date = date_create($pedido['pedi_diaria_dia_inicial']); echo date_format($date, 'd/m/Y'); ?></td>
									<td><?php $date = date_create($pedido['pedi_viagem_ida_dia']); echo date_format($date, 'd/m/Y H:i:s'); ?></td>
									<td><?php echo $pedido['pedi_viagem_ida_transporte']; ?></td>
									<td><?php $date = date_create($pedido['pedi_viagem_volta_dia']); echo date_format($date, 'd/m/Y H:i:s'); ?></td>
									<td><?php echo $pedido['pedi_viagem_volta_transporte']; ?></td>
									
									<td style="text-align:right;">R$ <?php echo $pedido['pedi_diaria_quant'] * $pedido['curso_conta_vr_diaria']; ?></td>
									<td style="text-align:right;">R$ <?php echo $pedido['pedi_viagem_ida_valor_previsto']; ?></td>
									<td style="text-align:right;">R$ <?php echo $pedido['pedi_viagem_volta_valor_previsto']; ?></td>
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="3"></th>
							<th>Total: <?php echo $totalDiarias; ?></th>
							<th colspan="6"></th>
							<th style="text-align:right;">Total: R$ <?php echo $totalGastoDiarias; ?></th>
							<th style="text-align:right;">Total: R$ <?php echo $totalViagemIda; ?></th>
							<th style="text-align:right;">Total: R$ <?php echo $totalViagemVolta; ?></th>
						</tr>
					</tfoot>
				</table>
				
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->