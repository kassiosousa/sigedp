<?php
/* Model that represents Relatorios at database */

class ModelRelatorios extends CI_Model {

	public function listar_pedidos($tableParam = ''){

		$where = array('tabela.pedi_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_pedido'])){ $where += array('tabela.pmk_pedido' => $tableParam['pmk_pedido']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['fok_plano_disicplina'])){ $where += array('tabela.fok_plano_disicplina' => $tableParam['fok_plano_disicplina']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['pedi_tipo'])){ $where += array('tabela.pedi_tipo' => $tableParam['pedi_tipo']); }
		if(isset($tableParam['pedi_status'])){ $where += array('tabela.pedi_status' => $tableParam['pedi_status']); }
		
		if(isset($tableParam['data_inicio'])){ $where += array('tabela.pedi_viagem_ida_dia <=' => $tableParam['data_inicio']); }
		if(isset($tableParam['data_fim'])){ $where += array('tabela.pedi_viagem_volta_dia >=' => $tableParam['data_fim']); }
		
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('pedidos tabela');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar_usuarios($tableParam = ''){

		$where = array('tabela.pedi_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_pedido'])){ $where += array('tabela.pmk_pedido' => $tableParam['pmk_pedido']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['fok_plano_disicplina'])){ $where += array('tabela.fok_plano_disicplina' => $tableParam['fok_plano_disicplina']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['pedi_tipo'])){ $where += array('tabela.pedi_tipo' => $tableParam['pedi_tipo']); }
		if(isset($tableParam['pedi_status'])){ $where += array('tabela.pedi_status' => $tableParam['pedi_status']); }
		if(isset($tableParam['fok_usuario'])){ $where += array('tabela.fok_usuario' => $tableParam['fok_usuario']); }
		
		if(isset($tableParam['data_inicio'])){ $where += array('tabela.pedi_viagem_ida_dia <=' => $tableParam['data_inicio']); }
		if(isset($tableParam['data_fim'])){ $where += array('tabela.pedi_viagem_volta_dia >=' => $tableParam['data_fim']); }
		
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('pedidos tabela');
		$this->db->join('usuarios usua', 'usua.pmk_usuario = tabela.fok_usuario');
		$this->db->join('usuarios_tipos usuatipo', 'usua.fok_usuariotipo = usuatipo.pmk_usuariotipo');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar_financeiro($tableParam = ''){

		$where = array('tabela.pedi_is_ativo' => 'Sim');
		if(isset($tableParam['pmk_pedido'])){ $where += array('tabela.pmk_pedido' => $tableParam['pmk_pedido']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['fok_plano_disicplina'])){ $where += array('tabela.fok_plano_disicplina' => $tableParam['fok_plano_disicplina']); }
		if(isset($tableParam['fok_polo'])){ $where += array('tabela.fok_polo' => $tableParam['fok_polo']); }
		if(isset($tableParam['pedi_tipo'])){ $where += array('tabela.pedi_tipo' => $tableParam['pedi_tipo']); }
		if(isset($tableParam['fok_usuario'])){ $where += array('tabela.fok_usuario' => $tableParam['fok_usuario']); }
		
		if(isset($tableParam['data_inicio'])){ $where += array('tabela.pedi_viagem_ida_dia <=' => $tableParam['data_inicio']); }
		if(isset($tableParam['data_fim'])){ $where += array('tabela.pedi_viagem_volta_dia >=' => $tableParam['data_fim']); }
		
		if(isset($tableParam['newLimit'])){ $this->db->limit($tableParam['newLimit'],0); } else { $this->db->limit(100,0); }

		$this->db->select('*');
		$this->db->from('pedidos tabela');
		$this->db->join('usuarios usua', 'usua.pmk_usuario = tabela.fok_usuario');
		$this->db->join('usuarios_tipos usuatipo', 'usua.fok_usuariotipo = usuatipo.pmk_usuariotipo');
		
		$this->db->join('planos_pedagogicos_disciplinas disciplina', 'disciplina.pmk_plano_disciplina = tabela.fok_plano_disicplina');
		$this->db->join('planos_pedagogicos plano', 'disciplina.fok_plano_pedagogico = plano.pmk_plano_pedagogico');
		$this->db->join('cursos_contas conta', 'conta.fok_curso = plano.fok_curso');
		
		$this->db->where('tabela.pedi_status', 'Concluido');
		$this->db->where($where);
		if (isset($like)) {
			$this->db->like($like);
		}
		$query = $this->db->get();

		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return false;
		}
	}

    public function todos() {
        // retorna todos num array associativo
        $query = $this->db->get('polos');
        $_results = $query->result();
        $results = array();
        foreach ($_results as $result) {
            $results[ $result->pmk_polo ] = $result->polo_titulo;
        }
        return $results;
    }

    public function por_curso($pmk_curso) {
        // retorna todos num array associativo
        $query = $this->db->get_where('cursos_polos',array('fok_curso' => $pmk_curso));
        $_results = $query->result();
        $results = array();
        foreach ($_results as $result) {
            $results[] = $result->fok_polo;
        }
        return $results;
    }
}
