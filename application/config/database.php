<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = ENVIRONMENT;
$active_record = TRUE;

// configs do banco no pc do kassio
$db['development_kassio']['hostname'] = 'localhost';
$db['development_kassio']['username'] = 'root';
$db['development_kassio']['password'] = '';
$db['development_kassio']['database'] = 'sigedp';
$db['development_kassio']['dbdriver'] = 'mysql';
$db['development_kassio']['dbprefix'] = '';
$db['development_kassio']['pconnect'] = TRUE;
$db['development_kassio']['db_debug'] = TRUE;
$db['development_kassio']['cache_on'] = FALSE;
$db['development_kassio']['cachedir'] = '';
$db['development_kassio']['char_set'] = 'utf8';
$db['development_kassio']['dbcollat'] = 'utf8_general_ci';
$db['development_kassio']['swap_pre'] = '';
$db['development_kassio']['autoinit'] = TRUE;
$db['development_kassio']['stricton'] = FALSE;

// configs do banco no pc do julio
$db['development_julio']['hostname'] = 'localhost';
$db['development_julio']['username'] = 'root';
$db['development_julio']['password'] = 'root';
$db['development_julio']['database'] = 'sigedp';
$db['development_julio']['dbdriver'] = 'mysql';
$db['development_julio']['dbprefix'] = '';
$db['development_julio']['pconnect'] = TRUE;
$db['development_julio']['db_debug'] = TRUE;
$db['development_julio']['cache_on'] = FALSE;
$db['development_julio']['cachedir'] = '';
$db['development_julio']['char_set'] = 'utf8';
$db['development_julio']['dbcollat'] = 'utf8_general_ci';
$db['development_julio']['swap_pre'] = '';
$db['development_julio']['autoinit'] = TRUE;
$db['development_julio']['stricton'] = FALSE;

// configs do banco em produção
$db['production']['hostname'] = 'sigedp.mysql.uhserver.com'; // TODO
$db['production']['username'] = 's1g3dp'; // TODO
$db['production']['password'] = 'w@lb3r@p0nt3s'; // TODO
$db['production']['database'] = 'sigedp'; // TODO
$db['production']['dbdriver'] = 'mysql';
$db['production']['dbprefix'] = '';
$db['production']['pconnect'] = TRUE;
$db['production']['db_debug'] = TRUE;
$db['production']['cache_on'] = FALSE;
$db['production']['cachedir'] = '';
$db['production']['char_set'] = 'utf8';
$db['production']['dbcollat'] = 'utf8_general_ci';
$db['production']['swap_pre'] = '';
$db['production']['autoinit'] = TRUE;
$db['production']['stricton'] = FALSE;



/* End of file database.php */
/* Location: ./application/config/database.php */
