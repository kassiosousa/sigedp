<?php

$html_fields = "";
$table_fields = $vars["table_fields"];

$qtdColunas = count($table_fields);
$countColunas = 0;

foreach($table_fields as $table_field){
	$countColunas++;
    if($table_field != $vars['pmktable'] && $qtdColunas > $countColunas){
        $field_name = $table_field;
        $label = ucwords($field_name);
        $html_fields .= "
		<div class='form-group'>
			<label for='$label' class='col-sm-4 control-label'>$label</label>
			<div class='col-sm-8'>
				<input type='text' class='form-control' id='$field_name' name='$field_name' value='<?php echo $$field_name; ?>' placeholder='$field_name' required>
			</div>
		</div>";
	
    }
}

$editar_template = "
<!-- Modal de Editar Usu�rio -->

<form class='form-horizontal'>
	<input type='hidden' name='${vars['pmktable']}' value='<?php echo $${vars['pmktable']}; ?>'>
		$html_fields
</form>

";
