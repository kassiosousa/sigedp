<?php
$keyTable = $vars['pmktable'];
$list_fields = '';
$qtdColunas = count($vars['table_fields']);

$countColunas = 0;
foreach($vars['table_fields'] as $field){
	$countColunas++;
    if($qtdColunas > $countColunas){
        $field_name = $field;
		$list_fields .= "<td><?php echo \$lista['$field_name']; ?></td>\n";
    }
}

// Após fazer a lista básica de rows, adiciona a coluna de opções
$list_fields .= "<td class='text-center acoes'>
					<div class='btn-group'>
						<button class='btn btn-primary btn-sm modal-form' data-url='<?php echo base_url();?>${vars['table']}/editar/?id=<?php echo \$lista['$keyTable'];?>' data-header='Editar Cadastro'><i class='fa fa-edit'></i> <span class='hidden-xs'>Editar</span></button>
						<button class='btn btn-danger btn-sm modal-form' data-url='<?php echo base_url();?>${vars['table']}/deletar/?id=<?php echo \$lista['$keyTable'];?>' data-header='Confirmar Exclusão'><i class='icon_close_alt2'></i><span class='hidden-xs'>Excluir</span></button>
					</div>
				</td>";

$head_fields = '';

$countColunas = 0;
foreach($vars['table_fields'] as $field){
	$countColunas++;
    if($qtdColunas > $countColunas){
	    $field_name = ucwords($field);
    	$head_fields .= "<th><i class='icon_profile'></i> $field_name</th>\n";
    }
}
// Após fazer a lista básica de rows, adiciona a coluna de opções
$head_fields .= "<td></td>";


$index_template = "
<?php
	\$tableList = \$this->${vars['table']}->listar();
?>

<div class='row'>
    <div class='col-xs-12'>
        <h1>${vars['model']}<small class='hidden-xs'><br>Lista de ${vars['model']} cadastrados</small>
			<button class='btn btn-success pull-right modal-form' data-url='<?php echo base_url();?>${vars['table']}/criar' data-header='Cadastrar ${vars['model']}'>
				<i class='fa fa-plus'></i> &nbsp;Criar ${vars['model']}
			</button>
		</h1>
        <ol class='breadcrumb'>
			<li><a href='<?php echo base_url();?>home'><i class='fa fa-dashboard'></i> Painel</a></li>
			<li><a href='<?php echo base_url();?>${vars['table']}'><i class='fa fa-users'></i> ${vars['model']}</a></li>
			<li class='active'>Listar</li>
		</ol>
    </div>
</div>


<div class='row'>
	<div class='col-xs-12'>
		<div class='box'>
			<div class='box-body'>
				<table id='listTable' class='table table-bordered table-hover'>
					<thead>
						<tr>
							$head_fields
						</tr>
					</thead>
					<tbody>
						<?php
							if (\$tableList) {
								foreach (\$tableList as \$lista) { ?>
								<tr>
									$list_fields
								</tr>
								<?php
								}
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							$head_fields
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

";
