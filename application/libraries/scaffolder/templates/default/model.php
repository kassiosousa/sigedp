<?php

$fields = '';
$isAtivo = '';
foreach($vars['table_fields'] as $field){
    $field_name = $field;
	$fields .= "    var \$$field_name;\n";
}
$isAtivo = $field; // Pega a �ltima posi��o do array que � o isAtivo

$fill = '';
foreach($vars['table_fields'] as $field){
    $field_name = $field;
	$fill .= "        \$this->$field_name = \$this->input->post('$field_name');\n";
}
$validate = '';
foreach($vars['table_fields'] as $field){
    if($field != $vars['pmktable']){
        $field_name = $field;
	    $validate .= "        if(\$this->$field_name == '') return false;\n";
    }
}

$model_template = "<?php
/* Model that represents ${vars['model']} at database */

class ${vars['model_name']} extends CI_Model {
	
	public function carregar (\$idTable){
		\$this->db->select('*');
		\$this->db->from('${vars['table']} tabela');
		\$this->db->where('tabela.${vars['pmktable']}', \$idTable);
		\$this->db->where('tabela.$isAtivo', 'Sim');
		
		\$this->db->limit(1);
		\$query = \$this->db->get();

		if(\$query->num_rows() == 1) {
			return \$query->result_array();
		} else {
			return false;
		}
	}
	
	public function listar(\$tableParam = ''){

		\$where = array('tabela.$isAtivo' => 'Sim');
		if(isset(\$tableParam['${vars['pmktable']}'])){ \$where += array('tabela.${vars['pmktable']}' => \$tableParam['${vars['pmktable']}']); }
		if(isset(\$tableParam['newLimit'])){ \$this->db->limit(\$tableParam['newLimit'],0); } else { \$this->db->limit(100,0); }
		
		\$this->db->select('*');
		\$this->db->from('${vars['table']} tabela');
		\$this->db->where(\$where); 
		if (isset(\$like)) {
			\$this->db->like(\$like);
		}
		\$query = \$this->db->get();

		if(\$query->num_rows() > 0) {
			return \$query->result_array();
		} else {
			return false;
		}
	}
	
	public function criar(\$tableParam) { 
		if (isset(\$tableParam)){
			\$this->db->trans_start();
			\$this->db->insert('${vars['table']}',	\$tableParam);
			\$insert_id = \$this->db->insert_id(); // << � necess�rio transa��o p/ usar isto corretamente
			\$this->db->trans_complete();	
			return \$insert_id;
		} else {
			return false;
		}
	}
	
	public function editar (\$tableParam) {
	
		if (isset(\$tableParam)){
			\$idTable = \$tableParam['${vars['pmktable']}'];
			
			\$this->db->where('${vars['pmktable']}', \$idTable);
			\$this->db->set(\$tableParam);
			
			return \$this->db->update('${vars['table']}');
		} else {
			return false;
		}
	}
	
	public function deletar (\$idTable) {
		\$tableParam['$isAtivo'] = 'Nao';
		\$tableParam['${vars['pmktable']}'] = \$idTable;
		
		\$this->db->where('${vars['pmktable']}', \$idTable);
		\$this->db->set(\$tableParam);
		
		return \$this->db->update('${vars['table']}');
	}
}
";
