<?php

require("config.php");
require("file_handler.php");
require("message.php");
require("text.php");
	
class Scaffolder {
    
    var $test = false;
    var $logger;
    var $sep = "/";
    /**
    * Default constructor that set if will be runned a test
    **/    
    public function __construct($testing = false){
        $this->test = $testing;
        $this->logger = new Message($testing);
    }
    /**
    * Include template file and return variable $($name)_template value
    * Eg.: if $name = controller
    * returns variable $controller_template value which is in file of TEMPLATES[controller]
    */
    private function getTemplate($name, $vars = array()){
        $var_name = "TEMPLATE_" . strtoupper($name);
        $template_name = constant($var_name);
        require($template_name);
        $template = $name . "_template";
        return $$template;
    }
    /**
    * Create a file at $path which content is the template
    * If it is running a test file will not be created
    **/
    private function createFile($path, $template){
        if(!$this->test){
            create_file($path, $template);
        }
    }
    /**
    * Joins $path and $filename adding extension if $filename is set
    **/
    private function getPath($path, $filename){
        return APPPATH . $path . $this->sep . ($filename != "" ? $filename . ".php" : "");
    }
    /**
    * Returns the path where the view will be created
    **/
    private function getViewPath($model, $view){
        $view_path = VIEW_PATH . $this->sep . Text::pluralize($model);
        if(!$this->test){
            create_folder($this->getPath($view_path, ""));
        }
        return $this->getPath($view_path, $view);
    }
    /**
    * Verify if user who is running this application can write at $path
    **/
    private function canWrite($path){
        clearstatcache();
        return is_writable($path);
    }
    /**
    * Create template at file to $what you want, at $path using $filename
    * $vars are to fill into template variables
    **/
    private function createTemplate($what, $path, $filename, $vars){
        $this->logger->log($filename);
        $template = $this->getTemplate($what, $vars);
        $pathname = ($what == 'controller' || $what == 'model') ? $this->getPath($path, $filename) : $this->getPath($path /*$vars['table']*/, $filename); //Mesma coisa pra controoller/model/view
        $this->createFile($pathname, $template);
        $this->logger->done();
        return $template;
    }
    /**
    * Verify if user can write at default paths:
    *       Controller, model and view paths
    * Displays message if user has no permissions and it is not a test
    **/
    public function verifyPaths(){
        $response = $this->canWrite($this->getPath(CONTROLLER_PATH, ""));
        if($response)
            $response = $this->canWrite($this->getPath(MODEL_PATH, ""));
        if($response)
            $response = $this->canWrite($this->getPath(VIEW_PATH, ""));
        if(!$response && !$this->test){
            $this->logger->cannotWrite();
        }
        return $response;
    }
    /**
    * Create a controller based on template set
    **/
    public function createController($controller, $pmktable){
        //$controller_plural = Text::pluralize($controller);
		$controller_plural = $controller;
        return $this->createTemplate("controller", "modules/".$controller."/".CONTROLLER_PATH, $controller_plural, 
            array(
				"table" => $controller,
                "controller" => $controller_plural,
                "controller_name" => ucwords($controller_plural),
                "model" => "model" . $controller,
                "model_name" => ucwords($controller),
				"pmktable" => $pmktable
            )
        );
    }
    /**
    * Create a model based on template set
    **/
    public function createModel($model, $pmktable, $table_fields){
        return $this->createTemplate("model", "modules/".$model."/".MODEL_PATH, "model" . $model, 
            array(
                "table" => $model,
                "model_name" => "Model" . ucwords($model),
                "model" => ucwords($model),
                "table_fields" => $table_fields,
				"pmktable" => $pmktable
            )
        );
    }
	/**
    * Create index view based on template set
    **/
    public function createIndexView($model, $pmktable, $fields){
        return $this->createTemplate("index", "modules/".$model."/".VIEW_PATH, "index", 
            array(
                "table" => $model,
                "model_name" => "Model" . ucwords($model),
                "model" => ucwords($model),
                "table_fields" => $fields,
				"pmktable" => $pmktable
            )
        );
    }
    /**
    * Create criar view based on template set
    **/
    public function createSaveView($model, $pmktable, $fields){
	
        return $this->createTemplate("criar", "modules/".$model."/".VIEW_PATH, "criar", array(
                "table" => $model,
				"pmktable" => $pmktable,
                "table_fields" => $fields,
				"model" => ucwords($model),
            )
        );
    }
    /**
    * Create form view based on template set
    **/
    public function createFormView($model, $pmktable, $fields){
        return $this->createTemplate("form", "modules/".$model."/".VIEW_PATH, "form", array(
                "table" => $model,
                "table_fields" => $fields,
				"pmktable" => $pmktable
            )
        );
    }
	/**
    * Cria a view de editar um item baseado no template setado
    **/
    public function createEditView($model, $pmktable, $fields){
        $first_field = $fields[1];
        return $this->createTemplate("editar", "modules/".$model."/".VIEW_PATH, "editar", 
            array(
                "table" => $model,
                "first_field" => $first_field,
				"pmktable" => $pmktable,
                "table_fields" => $fields
            )
        );
    }
    /**
    * Cria a view de deletar um item baseado no template setado
    **/
    public function createDeleteView($model, $pmktable, $fields){
        $first_field = $fields[1];
        return $this->createTemplate("deletar", "modules/".$model."/".VIEW_PATH, "deletar", 
            array(
                "table" => $model,
                "first_field" => $first_field,
				"pmktable" => $pmktable
            )
        );
    }

    /**
    * Create all CRUD features
    **/
    public function generate($table, $pmktable, $fields){
        if($this->verifyPaths()){
            $this->createController($table, $pmktable);
            $this->createModel($table, $pmktable, $fields);
            $this->createIndexView($table, $pmktable, $fields);
            $this->createDeleteView($table, $pmktable, $fields);
            //$this->createFormView($table, $pmktable, $fields);
			$this->createEditView($table, $pmktable, $fields);
			$this->createSaveView($table, $pmktable, $fields);
            $this->logger->success($table);
        }
    }
}
