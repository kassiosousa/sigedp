<?php

$TEMPLATE_NAME = "default";

define("TEMPLATE", "templates/$TEMPLATE_NAME/");
define("TEMPLATE_CONTROLLER", TEMPLATE . "controller.php");
define("TEMPLATE_MODEL", TEMPLATE . "model.php");
define("TEMPLATE_INDEX", TEMPLATE . "index.php");
define("TEMPLATE_CRIAR", TEMPLATE . "criar.php");
define("TEMPLATE_EDITAR", TEMPLATE . "editar.php");
define("TEMPLATE_DELETAR", TEMPLATE . "deletar.php");
define("TEMPLATE_FORM", TEMPLATE . "form.php");

define("CONTROLLER_PATH", "controllers");
define("MODEL_PATH", "models");
define("VIEW_PATH", "views");
