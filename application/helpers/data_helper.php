<?php

function valid_date_str($date='')
{
    $date = trim($date);
    if( empty($date) ) return false;
    $len = strlen($date);

    // somente data
    if($len == 10)
    {
        // 31/01/2015
        if($date{2} == '/' && $date{5} == '/')
        {
            if(validateDate($date,'d/m/Y'))
            {
                list($d,$m,$y) = explode('/', $date);
                return "$y-$m-$d";
            }
            else
            {
                return false;
            }
        }
        // 2015-01-31
        else
        {
            return validateDate($date,'Y-m-d')? $date : false;
        }
    }
    // data e hora
    elseif($len == 19)
    {
        // 31/01/2015 12:00:00
        if($date{2} == '/' && $date{5} == '/')
        {

            if(validateDate($date,'d/m/Y H:i:s'))
            {
                list($dmy, $time) = explode(' ', $date);
                list($d,$m,$y) = explode('/', $dmy);
                return "$y-$m-$d $time";
            }
            else
            {
                return false;
            }
        }
        // 2015-01-31 12:00:00
        else
        {
            return validateDate($date,'Y-m-d H:i:s')? $date : false;
        }
    }
    else
    {
        return false;
    }
}

// http://php.net/manual/pt_BR/function.checkdate.php#113205
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
