$(function(){

    // coloca máscara nos campos que tem "data-mask" definido
    $('input[data-mask]').each(function(){
        $(this).mask($(this).data('mask'));
    });

    // trata input type date para browsers que ainda não implementaram (IE, Firefox)
    $('input.input-date').each(function(){
        if($(this)[0].type != 'date')
        {
            var ptrn = /(\d{4})\-(\d{2})\-(\d{2})/;
            if($(this)[0].value != '' && $(this)[0].value.match(ptrn))
            {
                $(this)[0].value = $(this)[0].value.replace(ptrn, '$3/$2/$1');
            }
            $(this).mask('99/99/9999');
        }
    });

    // evento de click para mostrar os campos de alteração de senha
    $('#btn-alterar-senha').click(function()
    {
        $(this).parents('.form-group').slideUp({
            duration: 'fast',
            //easing: 'linear',
            complete: function()
            {
                $('#div-alterar-senha').slideDown({duration: 'fast'});
            }
        });
    });

    $('#uf').change(function()
    {
        $("#fok_cidade").empty();
        $.getJSON($("#fok_cidade").data('url') + "?estado=" + $('#uf').val())
        .done(function(data)
        {
            $("#fok_cidade").select2({ data : data });
        });
    });

    // submit do form
    $('#btn-submit').click(function()
    {
        $('.form-group').removeClass('has-error');
        $('.help-block').text('');
        $(this).attr('disabled','disabled').text('Processando...');

        // envia o POST com os campos do formulário
        $.ajax({
            type: 'POST',
            url: 'perfil',
            data: $('form').serialize()
        })
        .done(function(erros){

            $('#btn-submit').removeAttr('disabled').text('Atualizar Dados');

            // exibe os erros de validação se houver
            if (erros)
            {
                for (var name in erros)
                {
                    var input = $('form [name="'+ name +'"]');
                    input.parents('.form-group').addClass('has-error');
                    if (input.siblings('.help-block').length == 0) input.after('<span class="help-block">'+ erros[name] +'</span>');
                    else input.siblings('.help-block').text(erros[name]);
                }

                bootbox.alert({
                    size: "small",
                    title: "Falha ao Atualizar",
                    message: "Corrija as informações com erro no formulário e tente novamente."
                });
            }
            // se submeteu com sucesso :-)
            else
            {
                $('input[name="usua_senha_atual"], input[name="usua_senha"], input[name="usua_senha_confirma"]').val('');

                bootbox.alert({
                    size: "small",
                    title: "Atualizado com Sucesso",
                    message: "Suas informações cadastrais foram atualizadas!"
                });
            }
        });
    });
});
