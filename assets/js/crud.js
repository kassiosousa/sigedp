$(function(){

    // define o evento de clique dos botões de formulário que disparam modais
    $('button.modal-form').click(function(){
        var btn = $(this);
        $.ajax({
            url: btn.data('url'),
            dataType: 'html'
        })
        .done(function(html){
            var modal = bootbox.dialog({
                title: btn.data('header'),
                message: html,
                buttons: {
                    enviar : {
                        label: 'Enviar',
                        className: 'btn-primary',
                        callback: function()
                        {
                            $(this).find('.form-group').removeClass('has-error');
                            $(this).find('.help-block').text('');
                            $(this).find('button[data-bb-handler="enviar"]').attr('disabled','disabled')
                                .text('Processando...'); // << impede dois submits
                            submit_form_ajax(btn.data('url')); // << envia submit (ajax)
                            return false; // << não fecha o modal
                        }
                    }
                }
            });

            // coloca máscara nos campos que tem "data-mask" definido
            $(modal).find('input[data-mask]').each(function(){
                $(this).mask($(this).data('mask'));
            });

            // trata input type date para browsers que ainda não implementaram (IE, Firefox)
            $(modal).find('input.input-date').each(function(){
                if($(this)[0].type != 'date')
                {
                    var ptrn = /(\d{4})\-(\d{2})\-(\d{2})/;
                    if($(this)[0].value != '' && $(this)[0].value.match(ptrn))
                    {
                        $(this)[0].value = $(this)[0].value.replace(ptrn, '$3/$2/$1');
                    }
                    $(this).mask('99/99/9999');
                }
            });

            // coloca caixa de busca para os selects que foram grandes
            $(modal).find('select.select-search').each(function(){
                $(this).select2();
            });

            // tratamento para select de cidade, se houver
            $(modal).find('select.select-cidade').each(function(){
                var uf = $(this).data('ref') || '#uf';
                var param = $(this).data('param') || 'estado';
                var that = $(this);

                $(modal).find(uf).change(function(){
                    if (!that.is('[disabled]'))
                    {
                        that.empty();
                        $.getJSON(that.data('url') + "?" + param + "=" + $(uf).val())
                        .done(function(data){
                            that.select2({ data : data });
                            that.change();
                        });
                    }
                });
            });

            // dá um delayzinho e devolve o focus ao autofocus (pq o bootbox poe o focus no botão final)
            var autofocus = $(modal).find('[autofocus]');
            if (autofocus.length == 1)
            {
                setTimeout(function(){
                    autofocus.focus();
                },900);
            }

        });
    });

    // função que submete o formulário que está dentro do modal
    function submit_form_ajax(url)
    {
        var contentType = 'application/x-www-form-urlencoded';
        var data = $('.modal-dialog form').serialize();
        var processData = true;
        if ( $('.modal-dialog form input[type="file"]').length > 0 )
        {
            contentType = false;
            data = new FormData($('.modal-dialog form').get(0));
            processData = false;
        }

        // envia o POST com os campos do formulário
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            contentType: contentType,
            processData: processData
        })
        .done(function(erros){
            // exibe os erros de validação se houver
            if (erros)
            {
                for (var name in erros)
                {
                    var input = $('.modal-dialog form [name="'+ name +'"]');
                    input.parents('.form-group').addClass('has-error');
                    if (input.siblings('.help-block').length == 0) input.after('<span class="help-block">'+ erros[name] +'</span>');
                    else input.siblings('.help-block').text(erros[name]);
                }
                $('.modal-dialog button[data-bb-handler="enviar"]').text('Enviar').removeAttr('disabled');
            }
            // se submeteu com sucesso :-)
            else
            {
                bootbox.hideAll();
                setTimeout(function(){ location.reload(true); },500);
            }
        });
    }

});
